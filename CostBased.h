////
//// Created by sergeyn on 02/04/16.
////
//
//#ifndef TCACHES_COSTBASED_H
//#define TCACHES_COSTBASED_H
//
//#include <unordered_map>
//#include <vector>
//#include <list>
//
//namespace  Caching {
//    typedef unsigned term_t;
//
//    class Term;
//
//    class CacheInterface {
//    public:
//        virtual ~CacheInterface() { }
//
//        //length is the up-to-date length of the term on disk (could be smaller if deletes)
//        virtual bool visit(term_t term, size_t length) = 0;
//    };
//
//    class BaseCache : public CacheInterface {
//    public:
//        size_t cacheHits;
//        size_t cachePostingsServed;
//        size_t cachePostingsMissed;
//        size_t maxPostings;
//
//        BaseCache();
//        virtual ~BaseCache();
//        virtual void setMaxPostings(size_t maxP);
//        virtual size_t getTotalP() const = 0;
//        bool visit(unsigned term, size_t length);
//        size_t size() const; //return the count of cached terms (size of lookup)
//    protected:
//        virtual void miss(term_t term, size_t length) = 0;
//        virtual void hit(Term* tptr, size_t length) = 0;
//
//        Term* visit(term_t term) const;
//        Term* placeNew(term_t term, size_t length);
//        void evictMany(const std::vector<term_t>& victims);
//        void evict(term_t t);
//
//        class BaseCacheIMPL;
//        BaseCacheIMPL *baseimpl;
//    };
//
//    //probably all broken...
//    class MoveToFront : public BaseCache {
//        std::list<Term*> termQ;
//        typedef std::list<Term*>::iterator LI;
//        std::unordered_map<term_t, LI> localLookup;
//        size_t totalPostings;
//    public:
//        explicit MoveToFront(size_t maxPstings=0) : totalPostings(0){
//        	maxPostings=(maxPstings);
//        }
//        size_t getTotalP() const { return totalPostings; }
//    protected:
//        virtual void miss(term_t term, size_t length);
//        virtual void hit(Term* tptr, size_t length);
//     };
//
//    class CostBased : public  BaseCache{
//    public:
//        explicit CostBased(size_t maxPstings=0);
//        ~CostBased();
//        virtual void setMaxPostings(size_t maxP);
//        size_t getTotalP() const;
//        void report(std::ostream& out) const;
//    protected:
//        virtual void miss(term_t term, size_t length);
//        virtual void hit(Term* tptr, size_t length);
//
//        class CostBasedIMPL;
//        CostBasedIMPL *pimpl;
//    };
//
//}
//#endif //TCACHES_COSTBASED_H
