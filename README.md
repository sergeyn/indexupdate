# README #

Thesis related code

------------------------------------------------------------------------------
The two most important events are:
**Eviction** (from UB) and **Consolidation** (of segmented data).

Recently we moved to **consolidation**-at-eviction mode (for monoliths AND castes)
When we evict a subset of terms from UB (all for monoliths) we have to:

1. if new data as a sep. segment  
    * accumulate all those and write together to disk
2.  merge-in mem-to-disk:  
    * read/write data (with k-way merge proccedure)  
    * conditional set appr. flag in global-lex

In both cases: 
* add the new lengths to global lex 
* clear this term from UB-lexicon
* orFlag -- set the new bit for terms in global-lex (only those that had postings in this batch!)

Notable sizes:
* sizeBefore &mdash; the size of the stack before eviction
* sizeDuring &mdash; the size of the stack when doing virtual push of the new seg
* sizeAfterMerge &mdash; the size of the stack after merging

Note:  *sizeDuring = 1 + sizeBefore* and *sizeAfterMerge == sizeDuring* iff this is a new segment    
orFlag is formed from a single set bit in position sizeDuring  
conditionalFlags are 3 masks that used to set the bit at pos. sizeAfterMerge conditionally

####Chrono

Before evicting/consolidating we remove all segments that were completely outdated.  
For the last (left-most) remaining we reimburse *only* for the writing of that segment - deleted.   
We still must read it completely.
During queries we only consider the actual part of the list.

We need to adapt the prognosticator to work correctly with Chrono.  
For this we need to be able to estimate the lifetime of a chasm we are trying to merge.    
We also need to establish the cost of consolidation across it.    
When merging L-segment(s) with R-segment(s) if L+R > CHURN for this caste, we start reducing the left part...

When considering lifetime, it is not just the right *part / whole experiment*,     
but at most CHURN, since if the tail is long enough the chasm is going away.


####Random
We delete (and reimburse for writing) only from segments that participate in a merge!




##More Tests!
If not well tested we want to test the k-way consolidator

 
 
