#ifndef IO_H_
#define IO_H_

#include "Numeric.h"
#include "Asserts.h"
#include "Types.h"

class PostingSize {
public:
	static unsigned szOfPostingBytes;
	static size_t postingsInBytes(size_t bytes) { return bytes/szOfPostingBytes; }
	static size_t bytesOfPostings(size_t postings) { return postings*szOfPostingBytes; }
	//static size_t bytesOfPostings(size_t postings, float) { return postings*szOfPostingBytes; }
};

struct IOStats {
	IOStats(uint64_t bytes, uint64_t sks, std::string l = "") : RW(bytes),seek(sks),label(l){}
	IOStats(uint64_t postings, uint64_t sks, int, std::string l = "") : RW(PostingSize::bytesOfPostings(postings)),seek(sks),label(l){}
	uint64_t RW;
	uint64_t seek;
	std::string label;

	inline double ioTimeInMinutes() const {
		const uint64_t mbstoms =  (uint64_t(NUMERIC::ioMBS)<<20)/(uint64_t(1000));
		const uint64_t ms = NUMERIC::ioSeek*seek + //seek time
				RW/mbstoms; //RW time
		return double(ms)/60000.0; //convert to minutes
	}
};

std::ostream& operator <<(std::ostream& stream, const IOStats& ws);

inline double queryCost(size_t queries, size_t sumSegmentSizes, size_t segmentsCount) {
	TASSERT(segmentsCount>0,"major underflow!")
	return IOStats(queries*sumSegmentSizes,queries*(segmentsCount-1),-1).ioTimeInMinutes();
}

void auxRebuildCPrice(std::vector<double> &consolidationPriceVector, const std::vector<size_t>& sizeStack, size_t lastOnDisk /*0 or 1*/, size_t withpb /*0 or 1*/);

inline void rebuildCPricePB(std::vector<double> &consolidationPriceVector, const std::vector<size_t>& sizeStack) {
	auxRebuildCPrice(consolidationPriceVector,sizeStack,0,1); }
inline void rebuildCPrice(std::vector<double> &consolidationPriceVector, const std::vector<size_t>& sizeStack, size_t lastOnDisk /*0 or 1*/) {
	auxRebuildCPrice(consolidationPriceVector,sizeStack,lastOnDisk,0); }

ConsolidationStats auxConsolidateTail(std::vector<size_t>& sizeStack/*, std::vector<double>& consolidationPriceVector*/, offset_t offset,size_t _isPB /* 0 or 1*/, size_t lastInMem /*0 or 1*/);

// The number of consolidation schedules https://oeis.org/A002627
ConsolidationStats kWayConsolidateAux(std::vector<size_t> &segments, size_t lastInMem /*0 or 1*/, size_t memBufferPostings);


//Note: the memBuffer sz is in postings!
//this one doesn't work with <2 segments!
template<typename IT>
ConsolidationStats kWayConsolidate(IT begin, IT end, size_t lastInMem /*0 or 1*/, size_t memBufferPostings = NUMERIC::mergingMemBuffer ) {
	std::vector<size_t> segments(begin,end);
	return kWayConsolidateAux(segments,lastInMem,memBufferPostings);
}

struct ConsTime {
	static  double consolidationDataToMinutes(const ConsolidationStats& cd);
	//{return IOStats(cd.data(), cd.seeks(),-1).ioTimeInMinutes();}

};
#endif /* IO_H_ */
