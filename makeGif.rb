lines =  (ARGV[1] || 34).to_i
frames = (ARGV[2] || 64).to_i

require 'rubygems'
require 'rmagick'

ar = File.new(ARGV[0]).readlines
(0...frames).each { |f| 
	width = ar[f*lines].chomp.split('').size
	img = Magick::Image.new(width,lines)

	(0...lines).each_with_index { |l,r|
		ar[f*lines + r].chomp.split('').each_with_index { |v,x|
			img.pixel_color(x, l, "blue") if v!=' '
		}
	}
	name = "frame_%05d.gif" % f
	img.write name
}
puts %x[convert -delay 60 -loop 0 frame_*.gif animation.gif]
