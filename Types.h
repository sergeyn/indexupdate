#ifndef TYPES_H_
#define TYPES_H_

//===================================================
//	system specific types
//===================================================
typedef unsigned tid_t;
typedef unsigned did_t;
typedef unsigned scr_t;
typedef unsigned offset_t;
typedef unsigned short int caste_t;

typedef  std::pair<uint16_t,uint16_t> consolidation; //consolidation for the prognosticator

inline bool getBit(void* ptr, const unsigned bitNumber) {return (reinterpret_cast<unsigned char*>(ptr))[bitNumber>>3] & (1<<(bitNumber&7));}
inline void setBit(void* ptr, const unsigned bitNumber) {(reinterpret_cast<unsigned char*>(ptr))[bitNumber>>3] |= (1<<(bitNumber&7));}
inline void unsetBit(void* ptr, const unsigned bitNumber) {(reinterpret_cast<unsigned char*>(ptr))[bitNumber>>3] &= ~((1<<(bitNumber&7)));}


namespace Env {
	const std::string envirnoment_names[4] = {"EVERGROW","OOPS","CHRONO","RNDDEL"};
	const unsigned EVERGROW(0);
	const unsigned CHRONO(2);
	const unsigned RNDDEL(3);
}


struct listAttributes {
	tid_t list; did_t length; did_t freq;
	explicit listAttributes(tid_t lst=0, did_t len=0, did_t frq=0) : list(lst),length(len),freq(frq) {}

	inline bool operator<(const listAttributes& rhs) const { return
								(length==rhs.length) ? freq>rhs.freq : length > rhs.length; } //NOTE: reversed
};

struct listAttWithCasteId : public listAttributes{
	listAttWithCasteId() : casteId(0){}
	explicit listAttWithCasteId(const listAttributes& meta) : listAttributes(meta), casteId(0){}
	caste_t casteId;
};

inline unsigned operator+(unsigned f, const listAttWithCasteId& rhs) { return f+rhs.freq; }
inline unsigned accLen(unsigned l, const listAttWithCasteId& rhs) { return l+rhs.length; }

struct ConsolidationStats{
	size_t rseeks; size_t rdata; size_t wdata; size_t wseeks;
	ConsolidationStats(size_t rs=0, size_t rd=0, size_t wd=0, size_t ws=0) : rseeks(rs),rdata(rd),wdata(wd),wseeks(ws){}
	ConsolidationStats& operator+=(const ConsolidationStats& rhs) { rseeks+=rhs.rseeks; rdata+=rhs.rdata; wseeks+=rhs.wseeks; wdata+=rhs.wdata; return *this;}
	size_t seeks() const { return rseeks+wseeks; }
	size_t data() const { return rdata+wdata; }
};
inline ConsolidationStats operator+(const ConsolidationStats& a, const ConsolidationStats& b) {
	return ConsolidationStats { a.rseeks+b.rseeks, a.rdata+b.rdata, a.wdata+b.wdata, a.wseeks+b.wseeks};
}
inline bool operator==(const ConsolidationStats& a, const ConsolidationStats& b) {
	return a.rseeks==b.rseeks && a.rdata==b.rdata && a.wdata==b.wdata && a.wseeks==b.wseeks;
}

std::ostream& operator<<(std::ostream&,const ConsolidationStats&);
std::ostream& operator<<(std::ostream&,const consolidation&);

struct ConsolidationPacket {
	offset_t position;
	offset_t leftMost;
	offset_t rightMost; //the position of segment's right border e.g.: ... -1 sri 0 srj 0 srk -1
	double cGain;
	size_t leftSum;
	size_t rightSum;
	ConsolidationPacket(offset_t where = 0, offset_t left = 0, offset_t right = 0, double gain = -1.0)
		: position(where), leftMost(left), rightMost(right), cGain(gain), leftSum(0), rightSum(0) {}
	bool isValid() const { return position  & leftMost & rightMost; }
	bool isInMem() const { return position +2  == rightMost; }
};

std::ostream& operator<<(std::ostream& out, const ConsolidationPacket &p);
inline bool operator<(const ConsolidationPacket &l, const ConsolidationPacket &r) {
	return l.rightMost < r.rightMost ? true :
			(l.rightMost == r.rightMost ? l.position < r.position : false) ;
}

inline bool operator==(const ConsolidationPacket &l, const ConsolidationPacket &r) {
        return  l.rightMost == r.rightMost &&
		l.leftMost == r.leftMost &&
                l.position == r.position;
}
#endif /* TYPES_H_ */

