#include "FlagsData.h"
#include "Asserts.h"

flgTuple MasksForLexFixing::s_masks[MAX_SEGMENTS];

void MasksForLexFixing::initMasks() {
	for(unsigned i=0; i<MAX_SEGMENTS; ++i)
		s_masks[i] = MasksForLexFixing::getFlagsForConditionalSet(i);
}

flgTuple MasksForLexFixing::get(unsigned bitOffset) { return s_masks[bitOffset]; }

/*

msb         <------ lsb
    ...  4   3  [2]  1  0       //bit offsets
         e   d  [c]  b  a       //bit values
         0   0  [1]  0  0       //or Mask
         1   1  [1]  0  0       //presence discovery mask
         0   0  [0]  1  1       //clear upper bits mask
         0   0 c|d|e b  a       // res
*/

std::tuple<flagsData,flagsData,flagsData> MasksForLexFixing::getFlagsForConditionalSet(unsigned bitOffset) {
	auto orMask = getZeroFlag();
	auto maskAwayLow =  getOneFlag();
	auto removeHigh = getZeroFlag();

	for(unsigned b=0; b<bitOffset; ++b) {
		b_setBit(removeHigh,b); //clearing upper bits mask
		b_unsetBit(maskAwayLow,b); //masking away lower bits mask
	}
	b_setBit(orMask,bitOffset); //bit setting
	return std::make_tuple(maskAwayLow,removeHigh,orMask);
}

//def test flags,tail,sz
//	allOnes = 0xffff; shiftBy  = sz-tail
//	clearMask = allOnes>>(16-shiftBy); orMask = 1<<shiftBy
//	masks = [0, orMask]; index = (0!=(flags>>shiftBy))?1:0
//	return (flags&clearMask)|masks[index]
//end

//there are 2.5 cases:
//* adding a single segment at the end of the stack
//* going from 8,4,1 via 8,4,1,1 to 8,4,2
//* merging the new guy with old ones
//see readme
flgTuple MasksForLexFixing::safeMasksForLexFixing(const unsigned /*oldStackSz*/, const unsigned newStackSize) {
	CHEAP_ASSERT(newStackSize > 0 && newStackSize <= MAX_SEGMENTS,"wrong stack data")
	return get(newStackSize-1);//maskAwayLow,removeHigh,orMask
}


