/*
 * URange.h
 *
 *  Created on: Dec 22, 2014
 *      Author: sergeyn
 */

#ifndef URANGE_H_
#define URANGE_H_


#include "Globals.h"
#include "Types.h"

class listAttributesUpdater;
class listAttributesProvider;

class URange {
	tid_t firstTerm;
	tid_t lastTerm;
	did_t totalDiskPostings; //real size of this caste
	std::vector<tid_t> memPostings;

	friend class URangeManager;
	inline void setDiskPostings(size_t pst) { totalDiskPostings = did_t(pst); }
	//inline void zeroMem() { memPostings = 0; }
	inline void evictMem() { totalDiskPostings += memPostings.size(); }

public:
	URange();
	URange(tid_t first, tid_t last, size_t diskPostings=0);
 	inline void addPosting(tid_t id) { memPostings.push_back(id); }
	std::pair<tid_t,tid_t> getBounds() const { return std::make_pair(firstTerm,lastTerm); }
	inline tid_t getFirst() const { return firstTerm;}
 	inline tid_t getLast() const { return lastTerm;}
	inline size_t sizeAll() const { return sizeMem()+totalDiskPostings; }
	inline size_t sizeMem() const { return memPostings.size(); }

};

struct TermBlock { //hold info about long lists
	did_t postingsOnDiskInRangeBlock;
	did_t currentTermBlockSizePostings;
	TermBlock();
	std::pair<size_t,size_t> writePostings(size_t newTotalPostings); //newTotal is from lex, the appendix in postingsInRangeBlock
};

class WriteBackBuffer;
class DiskReader;
class URangeManager {
	std::vector<tid_t> ubPostings;
	std::map<tid_t,URange> ranges;
	std::unordered_map<tid_t,TermBlock> longListsOfRange;
	size_t totalInMemory;

	WriteBackBuffer& writeBackBuffer;
	WriteBackBuffer& consolidationWriteBackBuffer;
	DiskReader& consolidationReader;

	size_t evictRange(URange& range,listAttributesUpdater& upd,  listAttributesProvider& provider);
	unsigned splitRange(URange& range, const listAttributesProvider& provider, size_t sizeAfterMergeBytes);
	void moveToTermBlock(tid_t id,did_t listLen);

public:
	static const size_t listSzThresholdBytes; //256kb
	static const size_t flushedMemoryBytes; //20mb
	static const size_t rangeBlockLimitBytes; //32mb;
	static const size_t ubLimitBytes; //1gb
	static size_t ubLimitPostings; //1gb
	static size_t listSzThresholdPostings; //256kb
	URangeManager(WriteBackBuffer& wbb, WriteBackBuffer& cwbb, DiskReader& crdr);
	inline void accumulatePosting(tid_t term) {ubPostings.push_back(term); }
	size_t process(listAttributesUpdater& upd, listAttributesProvider& provider);
	size_t totalInMem() const { return totalInMemory; }
};

#endif /* URANGE_H_ */
