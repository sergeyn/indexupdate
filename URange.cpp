/*
 * URange.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: sergeyn
 */

#include "URange.h"
#include "Asserts.h"
#include "Interfaces.h"
#include "IO.h"
#include "Numeric.h"
#include "ProxyIO.h"

const size_t URangeManager::listSzThresholdBytes(256*1024); //256kb
const size_t URangeManager::flushedMemoryBytes(20*1024*1024); //20mb
const size_t URangeManager::rangeBlockLimitBytes(32*1024*1024); //32mb;
const size_t URangeManager::ubLimitBytes(1024*1024*1024); //1gb
size_t URangeManager::listSzThresholdPostings;//(PostingSize::postingsInBytes(listSzThresholdBytes)); //256kb
size_t URangeManager::ubLimitPostings;//(PostingSize::postingsInBytes(ubLimitBytes)); //1gb

TermBlock::TermBlock() : postingsOnDiskInRangeBlock(0),currentTermBlockSizePostings(URangeManager::listSzThresholdPostings){}

std::pair<size_t,size_t> TermBlock::writePostings(size_t newTotalPostings) {
	std::pair<size_t,size_t> ret; //seekw,postingsw,seekr,postingsr
	if(currentTermBlockSizePostings<newTotalPostings) {
		//pay one seek, write appendix -> postingsInRangeBlock
		ret = {postingsOnDiskInRangeBlock,0};
	}
	else { //relocate
		currentTermBlockSizePostings=newTotalPostings*2;
		//pay one seek + newTotalPostings-postingsInRangeBlock for reading
		//pay one seek + newTotalPostings for writing
		ret = {newTotalPostings,newTotalPostings-postingsOnDiskInRangeBlock};
	}
	postingsOnDiskInRangeBlock = 0;
	return ret;
}
//======================================================================
URange::URange() :  firstTerm(0),lastTerm(0),totalDiskPostings(0)
{}

URange::URange(tid_t first, tid_t last, size_t diskPostings) :firstTerm(first),lastTerm(last),totalDiskPostings(diskPostings)
{
	CHEAP_ASSERT(first<=last,"messed up range")
	if(first == last)
		COUTL << first << " is a singleton range\n";
}
//======================================================================
URangeManager::URangeManager(WriteBackBuffer& wbb, WriteBackBuffer& cwbb, DiskReader& crdr) : totalInMemory(0),
												writeBackBuffer(wbb),consolidationWriteBackBuffer(cwbb),consolidationReader(crdr){
	const tid_t smallestValid = 0;
	URange all(smallestValid,NUMERIC::MAXT);
	ranges.insert( {smallestValid,all }); //add a single range covering all
}
//======================================================================
//we assume that all the data is already in main mem -- diskPostings and memPostings
unsigned URangeManager::splitRange(URange& range,const listAttributesProvider& provider, size_t sizeAfterMergeBytes) {
	const unsigned neededHalfFilledBlocks = ceil(float(sizeAfterMergeBytes)/float(URangeManager::rangeBlockLimitBytes/2));
	const size_t eachBlockSharePostings =  PostingSize::postingsInBytes(sizeAfterMergeBytes)/neededHalfFilledBlocks;
	CHEAP_ASSERT(neededHalfFilledBlocks>1,"why are we splitting?");

	auto origLastTerm = range.lastTerm;
	URange* ptr = &range;
	size_t accPosts = 0;
	for(tid_t tid = range.firstTerm; tid<=origLastTerm; ++tid)	{
		auto it = longListsOfRange.find(tid);
		if(it!=longListsOfRange.end() && it->second.postingsOnDiskInRangeBlock == 0) //long one that was taken care of
			continue;
		//the length in the lexicon is already updated with new postings
		accPosts +=  provider.length(tid);
		if(accPosts>eachBlockSharePostings) {
			ptr->lastTerm = tid; //record the last tid of the range
			ptr->setDiskPostings(accPosts);
			accPosts = 0;

			//add new Range
			if(tid+1<=origLastTerm) {
				URange newRange(tid+1,origLastTerm);
				ranges[tid+1] = newRange;
				ptr = & ranges[tid+1];
			}
		}
	}
	//todo: perhaps we should assert that sum of sizes of those ranges is ok
	ptr->setDiskPostings(accPosts); //don't forget the last one!
	consolidationWriteBackBuffer.push_back(accPosts);
	for(unsigned i=0; i<neededHalfFilledBlocks-1; ++i)
		consolidationWriteBackBuffer.push_back(0); //just for the seeks
	return neededHalfFilledBlocks;
}
//======================================================================
void URangeManager::moveToTermBlock(tid_t id,did_t listLen) {
	auto rw = longListsOfRange[id].writePostings(listLen);
	if(rw.second)
		consolidationReader.readBytes(PostingSize::postingsInBytes(rw.second));
	consolidationWriteBackBuffer.push_back(rw.first);
}
//======================================================================
size_t URangeManager::evictRange(URange& range, listAttributesUpdater& upd, listAttributesProvider& provider) {
	//merge postings of the range with on-disk part
	//read data from rangeblock
	//merge in memory
	auto postingsMem = range.sizeMem();
	auto postingsDisk = range.sizeAll()-postingsMem; //for merge we need to read this and add one seek
	auto lastId = range.getLast();

	//sort --------------------------------
	auto& memPostings = range.memPostings;
	std::sort(memPostings.begin(),memPostings.end());
	//COUTL << memPostings.front() << ">=" << range.getFirst() << "\n";
	CHEAP_ASSERT(memPostings.front()>=range.getFirst(),"bad range>");
	CHEAP_ASSERT(memPostings.back()<=lastId,"bad range<");

	//first stage: foreach term add the mem-postings to accumulating struct -------------------
	std::vector<std::pair<tid_t,did_t>> termsToPostings;
	auto startIt = memPostings.begin();
	size_t tid = *startIt;
	termsToPostings.reserve(postingsMem/2);
	size_t acc = 0;
	for(std::vector<tid_t>::iterator it = startIt; it!=memPostings.end(); ++it)	{
		if(tid == *it)
			++acc;
		else {
			termsToPostings.push_back({tid,acc});
			acc = 1;
			tid = *it;
		}
	}
	 termsToPostings.push_back({tid,acc}); //don't forget the last one
	if(termsToPostings.empty()) 	{
		COUTL << "memPostings: " << memPostings.size() << " for range " << range.getFirst() << " : " << range.getLast() << "\n";
	}
	else
		upd.addLength(termsToPostings); //batch update the lexicon

	//second stage: seek for long lists -----------------------------
	auto listSzPostingsLimit = PostingSize::postingsInBytes(URangeManager::listSzThresholdBytes);
	size_t accPostingsInLongLists = 0;
	for(std::pair<tid_t,did_t>& termAcc : termsToPostings) {
		const auto id = termAcc.first;
		const auto newPosts = termAcc.second;
		auto listLen = provider.length(id); //by now list length is updated in lex
		if(listLen > listSzPostingsLimit) { //some kind of overflow related to long lists
			if(0 == longListsOfRange.count(id))  //new comer
				longListsOfRange[id].postingsOnDiskInRangeBlock = 0; //register
			auto& refToSz = longListsOfRange[id];
			refToSz.postingsOnDiskInRangeBlock += newPosts;

			if(refToSz.postingsOnDiskInRangeBlock > listSzPostingsLimit) {//long list overflowing
				longListsOfRange[id].writePostings(newPosts); //moveToTermBlock
				accPostingsInLongLists += newPosts;
			} //end if overflow
		} //end if long list
	} //end for

	//handle rangeblock overflow
	const size_t postingsAfterMerge = (postingsMem-accPostingsInLongLists+postingsDisk);
	const size_t sizeAfterMergeBytes = PostingSize::bytesOfPostings(postingsAfterMerge);
	consolidationReader.readBytes(range.totalDiskPostings); //we need to pay for reading in either case

	if(sizeAfterMergeBytes > URangeManager::rangeBlockLimitBytes){
		splitRange(range,provider,sizeAfterMergeBytes); //note: when done this range reference might become dangling!
	}
	else {
		// store data in rangeblock -- read prev. data from disk and write the new size back
		writeBackBuffer.push_back(postingsAfterMerge);
		range.evictMem(); //remove postings of range from memory
	}

	memPostings.clear();
	return postingsMem;
}
//======================================================================
size_t URangeManager::process(listAttributesUpdater& upd,listAttributesProvider& provider) {
	const size_t postingLimitFlush = PostingSize::postingsInBytes(URangeManager::flushedMemoryBytes);
	CHEAP_ASSERT(!ranges.empty(),"no initial range given!")

	// update the ranges with new postings -----------------------------
	std::sort(ubPostings.begin(),ubPostings.end()); //sort by tid, should be small enough, just the delta

	auto itPosts = ubPostings.begin();
	auto itRanges = ranges.begin();

	while(itPosts!=ubPostings.end())  {
		if(*itPosts <= itRanges->second.getLast()) { //if we are in this range
			itRanges->second.addPosting(*itPosts);
			++itPosts;
		}
		else {

			auto firstlast = itRanges->second.getBounds();
			++itRanges;
			if(itRanges == ranges.end())
				break;
			if(*itPosts<itRanges->second.getFirst())
			{
				COUTL << firstlast.first << " : " << firstlast.second << " <= " << *itPosts << "<="<< itRanges->second.getFirst() << " : " << itRanges->second.getLast() <<ENDL;
				FATAL("!");
			}
		}
	} //end while
	totalInMemory+=ubPostings.size();
	ubPostings.clear(); //now we have them in ranges, remove


	//sort the ranges by max sizeMem -----------------------------------
	std::vector<URange*> ptrs(ranges.size());
	unsigned i=0;
	for(auto& mPair : ranges)
		ptrs[i++]=&mPair.second;
	std::sort(ptrs.begin(),ptrs.end(), [](const URange* a, const URange* b) {return b->sizeMem() < a->sizeMem(); }); //reverse sort


	// evict from largest to smallest -----------------------------------
	size_t accPostings = 0;
	unsigned countEvicted = 0;
	for(URange* range : ptrs) 	{
		const auto expectEvicting = range->sizeMem();
		accPostings += expectEvicting;
		const auto evicted = evictRange(*range,upd,provider);
		totalInMemory -= evicted;
		CHEAP_ASSERT(evicted == expectEvicting,"expected to evict");
		++countEvicted;
		if(accPostings >= postingLimitFlush && totalInMemory < URangeManager::ubLimitPostings) //stop when we have evicted enough
			break;
	}

	COUTL << "evicted " << countEvicted << " there are now " << ranges.size()<<" ranges\n";
	return accPostings;
}
