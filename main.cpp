#include "Configuration.h"
#include "Options.h"
#include "Prognosticator.h"
#include "BufferManageSimulator.h"
#include "TuplesRW.h"
#include "Framework.h"
#include "QP.h"
#include "Numeric.h"

#include <csignal>

Options parseCommandLineArgs(int argc, char * argv[]) {
        argc-=(argc>0); argv+=(argc>0); // skip program name argv[0]
        Options options;
        options.parseFromCmdLine(argc,argv);
        return options;
}



std::pair<BufferStats,BufferStats> runUB(const configuration cfg, const std::string& fileUpdatesFreqs, const std::string& evictor) {
	COUT2 << "UB size in postings: "<< cfg.postingBufferSize << " (~" <<
			cfg.postingBufferSize/NUMERIC::avgPostingInDoc << " docs). It is " <<
		cfg.postingBufferSize * PostingSize::szOfPostingBytes << " bytes"<< ENDL;
    //get update and query freqs from lex
    std::vector<did_t> uHist, qFreqs;
    uHist.resize(cfg.MAXT+1,1);
    qFreqs.resize(cfg.MAXT+1,1);

    /*
    //first read the clueweb data for updates histogram:
    auto clueInterpFname = "newInterpolation_85";
    std::ifstream inClueWeb(clueInterpFname); //sparseLex.lex
    if(!inClueWeb.is_open()) FATAL(clueInterpFname);
    while(inClueWeb.good()) {
		tid_t id; did_t length; unsigned freq;
		inClueWeb >> id >> length >> freq;
		TASSERT(id<=cfg.MAXT,"wrong clueId");
		uHist[id]=length;
		//qFreqs[id]=freq; //we will override it with the second file anyway
    }*/
    //now use the interpolation file for the freqs
    std::ifstream in(fileUpdatesFreqs);
    if(!in.is_open()) FATAL(fileUpdatesFreqs);
    while(in.good()) {
		tid_t id; did_t length; did_t qFreq;
		in >> id >> length >> qFreq;
		TASSERT(id<=cfg.MAXT,"wrong clueId");
		uHist[id]=length;
		qFreqs[id]=qFreq;
    }
    COUT4 << "lexicons were read" << ENDL;
    if(0)
	{
//		for(unsigned i=0; i<10; ++i) {
//			size_t maxOffset = std::max_element(qFreqs.begin(),qFreqs.end())-qFreqs.begin();
//			COUTL << maxOffset << ": " << qFreqs[maxOffset] << " <--> " << uHist[maxOffset-1] << " " << uHist[maxOffset] << " " << uHist[maxOffset+1] << ENDL;
//			qFreqs[maxOffset]=0;
//		}

	}

    DistrFromFile updatesDistr("updates");
    DistrFromFile qDistr("queries");
	COUT4 << "update and query distributions initialized" << ENDL;

	//consolidate the term data in a matrix:
	std::vector<listAttWithCasteId> termPoints;
	termPoints.reserve(uHist.size()+16);
	for(tid_t id=1; id<uHist.size(); ++id)
	//	if(uHist[id]>2)
			termPoints.push_back(listAttWithCasteId(listAttributes(id,uHist[id],qFreqs[id])));

	//free the data
    uHist = std::vector<did_t>();
    qFreqs = std::vector<did_t>();

    //scale controls the number of castes (large scale -- less castes)
    // auto scale = (NUMERIC::totalPostingsClueweb/cfg.postingBufferSize);
    auto scale = (NUMERIC::totalPostingsClueweb/256242);	//this is a bit cheating...
    CasteSplitter splitter(cfg.minWBSize*scale,cfg.castesScale);
    splitter.splitInPlace(termPoints);

    Framework framework; //
    auto freeUpTo = (cfg.postingBufferSize * cfg.freeUpTo) / 100; //freeupto is in percents!
	COUT2 << "cfg.CACHESZ: " << cfg.CACHESZ << " postings or " <<  cfg.CACHESZ*PostingSize::szOfPostingBytes << "bytes " << ENDL;
	/*(std::vector<listAttWithCasteId>& casteSorted,
			DistributionInterface* updateD, DistributionInterface* queriesD,
			const std::string& qProc, const std::string& evictor, size_t postingBufferSize, size_t freeUp2, size_t expectedTerms, size_t cacheSize,
			std::pair<unsigned, unsigned> updatesToQueriesRatio,unsigned aggressive,
			const std::string& jumpstartFile,bool loadIOs)*/
    framework.setup(termPoints,&updatesDistr,&qDistr,cfg.qprocAlg,evictor,cfg.postingBufferSize,freeUpTo,cfg.MAXT,cfg.CACHESZ,
    		std::make_pair(cfg.updatesCountInStream,cfg.queriesCountInStream),cfg.aggressive,
		cfg.JUMPSTART,cfg.LOADPROXIES);

    auto stats = framework.run(cfg.runCycles, cfg.runPostings,
    		          std::make_pair(cfg.updatesCountInStream,cfg.queriesCountInStream),
    		          cfg.printStatsFrequency,cfg.stableCapacity,
    		          cfg.IS_DEL_RAND, cfg.IS_SKI, cfg.runPostings);
   // COUT4 << framework.getCasteStats();
    return stats;
}

void setGlobals(const configuration& cfg) {
	COUT2 << "initial MAXT " << cfg.MAXT << ENDL;
	caste::minWBSize = cfg.minWBSize;
	COUT2 << "initial minWBSize " << caste::minWBSize << ENDL;
	caste::maxWBSize =  cfg.maxWBSize;
	PostingSize::szOfPostingBytes = cfg.szOfPostingBytes;
	COUT2 << "postingBytes: " << PostingSize::szOfPostingBytes << ENDL;
	ReMergePolicy::setCmpMargin(cfg.compareMargin);

#ifdef DEBUG
	std::cout << IOStats(2730682452420000,21333).ioTimeInMinutes() << std::endl;
	std::cout << IOStats(11196078291616,1852795).ioTimeInMinutes() << std::endl;
	std::cout << IOStats(0,60000000).ioTimeInMinutes() << std::endl;
	std::cout << IOStats(0,21333).ioTimeInMinutes() << std::endl;
#endif
}
void asserts() {
	static_assert(sizeof(void *) == 8, "non 64-bit code generation is discouraged (for now)");
	static_assert(sizeof(caste_t) == 2, "we count on it being small");
}

configuration configuration::globalConfig;
Options configuration::globalOptions;

int main(int argc, char * argv[]) {
	// Install a signal handler
	std::signal(SIGABRT, signal_handler);
	asserts();

	//CasteManager::splitInPlaceFromFileToCout("../cluewebTrainedWithTrec.noSinglesnoRares"); return 0;
	Options options = parseCommandLineArgs(argc, argv);
	globalVerbosity = options.VERBOSITY;
	COUT2 << "Build: " << __DATE__ << " " << __TIME__ << ENDL;
	if(globalVerbosity>=2) {
		for(auto i : make_irange(argc))
			std::cout << argv[i] << " ";
		std::cout << std::endl;
		options.print(std::cout);
	}
	#ifdef SSD
	COUT2 << "SSD run\n";
	#else
	COUT2 << "HD run\n";
	#endif

	MasksForLexFixing::initMasks();

	COUT2 << "bytesReadInTimeOfSeek: " << NUMERIC::bytesReadInTimeOfSeek << "\n";
	COUT2 << "FlagsData sizeof: " << sizeof(flagsData)*8 << " bits\n";
	COUT2 << "Lex sizeof: " << sizeof(listMetaDataForGlobalLex) << " bytes\n";
	COUT2 << "ioSeek: " << NUMERIC::ioSeek << "\n";

	if(options.IMBUE)
		std::cout.imbue(std::locale(""));

	globalRegistry.registerClass<UpdatesBuffer::CasteEvictor>();
	globalRegistry.registerClass<QpNoWriteBack>();
	globalRegistry.registerClass<QpWithCastes>();

	configuration cfg;

	cfg.runCycles = options.CYCLES;
	cfg.runPostings = options.POSTINGS;
	cfg.printStatsFrequency = options.PRINTSTATSFREQ;

	const auto totalmem = options.TOTALMEM;
	if(options.TCPERCENT == 111) //convention
		options.TCPERCENT = 100 - options.UBPERCENT;

	cfg.postingBufferSize = (totalmem*options.UBPERCENT)/100;
	cfg.CACHESZ = (totalmem*options.TCPERCENT)/100;
	cfg.castesScale = options.CASTESCALE;
	cfg.qprocAlg = options.QPROC;
	cfg.stableCapacity = options.STABLECAPACITY;
	cfg.IS_DEL_RAND = options.IS_RANDOM_DEL;
	cfg.IS_SKI = options.IS_SKI;
	cfg.JUMPSTART = options.JUMPSTART;
	cfg.LOADPROXIES = options.LOADPROXIES;
	cfg.freeUpTo = options.FREEPCT;
	cfg.aggressive = options.AGGRESSIVE;
	cfg = configuration::mergeSelect(options.MONOLITHICMERGE,cfg);
	cfg.forceMergeGeoInCastes = options.FORCEGEOMERGEINCASTES;
	if(cfg.qprocAlg == QpWithCastes().name()) { //we need this for the sake of forced log. merge
		cfg = configuration::remergeLog(cfg);
	}

	cfg = configuration::ratesUpdatesQueries(cfg,options.UPSRATE,options.QUERYRATE);
	setGlobals(cfg);
	configuration::globalConfig = cfg;
	configuration::globalOptions = options;

#ifdef URF
	URangeManager::listSzThresholdPostings = (PostingSize::postingsInBytes(URangeManager::listSzThresholdBytes)); //256kb
	URangeManager::ubLimitPostings = (PostingSize::postingsInBytes(URangeManager::ubLimitBytes)); //1gb
	cfg.postingBufferSize = URangeManager::ubLimitPostings;
	globalRegistry.registerClass<QpURF>();
	COUTL << "cfg.postingBufferSize: " << cfg.postingBufferSize << ENDL;
#endif

	//auto bna =
			runUB(cfg, options.PREDICTIONS,options.EVICTOR);
	//COUT << bna.second << ENDL;
	return 0;
}



/*
template <typename Iterator>
double meanIt(Iterator begin, Iterator end) {
	return std::accumulate(begin,end,0.0)/static_cast<double>(end-begin);
}
template <typename Iterator>
double stdDevIt(Iterator begin, Iterator end) {
	double mean = meanIt(begin,end);
	double sz = static_cast<double>(end-begin);
	double acc=0.0;
	while(begin!=end) {
		double var = *begin - mean;
		acc +=  (var*var);
		++begin;
	}
	return std::sqrt(acc/sz);
}

   //initiate an update distribution model
	//DiscreteWithUnseens updatesDistr(uHist,1<<30);//cfg.newTermFrequency);
	//updatesDistr.reseed(0);
if(0){
//	std::ofstream f("/tmp/updatesTest", std::ios::out | std::ios::binary);
//		const unsigned mega = 1000*1000;
//		unsigned buffer[mega];
//		for(size_t i=0; i<1000; ++i) {
//			for(unsigned j=0; j<mega; ++j)
//				(buffer[j]=updatesDistr.next());
//			f.write((const char*)buffer,sizeof(unsigned)*mega);
//			COUTL << i << ENDL;
//		}
//		f.flush();	f.close();
//		FATAL("!");
    }
		//initiate a query distribution
   // DiscreteWithUnseens qDistr(qFreqs,1<<30);
   // qDistr.reseed(0);




	if(0)
	{
//	std::vector<unsigned> histU(cfg.MAXT+1024,0);
//	std::vector<unsigned> histQ(cfg.MAXT+1024,0);
//		//test distribution alignment:
//		for(unsigned i=0; i<256; ++i) {
//			COUTL << i << ENDL;
//			for(unsigned j=0; j<1024*1024; ++j) 		{
//				++histU[updatesDistr.next()];
//				++histQ[qDistr.next()];
//			}
//		}
//		for(unsigned i=0; i<10; ++i) {
//			size_t maxOffset = std::max_element(histQ.begin(),histQ.end())-histQ.begin();
//			COUTL << maxOffset << ": " << histQ[maxOffset] << " <--> " << histU[maxOffset-1] << " " << histU[maxOffset] << " " << histU[maxOffset+1] << ENDL;
//			histQ[maxOffset]=0;
//		}

	}
*/
