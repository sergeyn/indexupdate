#include "CasteSystem.h"
#include "IO.h"
#include "UpdatesBuffer.h"

CasteManager::CasteManager(listAttributesProvider& attP) : attProvider(attP),aggressive(1){tidToCid.resize(86532822,0); casteVec.push_back(nullptr);}

CasteManager::~CasteManager() {
	for (caste* cst : *this) //skip first!
		delete cst;
}

void CasteManager::printHists() {
	COUTL << "HHHHHHHHHHHHHH\n";
	auto copy = getSortedCopy2();
	for(auto cst : copy) {
		CHEAP_ASSERT(cst,"why null")
		cst->printHistData();
	}
//	const auto& singletons = copy.back();
//	size_t acc = 0;
//	for(auto id : singletons->members)
//		acc += b_getSetBitInFlags(flags(id));
//	COUTL << double(acc) / double(singletons->members.size()) << ENDL;
}

void CasteManager::resestAll() {
	for(auto cst : *this)
		cst->resetAfterEviction();
	longTailTerms.clear();
}

void CasteManager::loadCastesFromAttMatrix(const std::vector<listAttWithCasteId>& data) {
	std::vector<caste> lCastes = caste::convertSplitterToCastes(data);
	caste_t mxId=0;
	for(const auto& lcst : lCastes)
		mxId = std::max(mxId,lcst.casteId);

	COUTV(6) << "id #members totalUpd totalQueries " << ENDL;
	for(const auto& lcst : lCastes) {
		if(lcst.casteId==mxId)
			continue;
		add(lcst.casteId,lcst);
		COUTV(6) << lcst.casteId << " "  << lcst.members.size() << " " << lcst.predictedUpdates << " " << lcst.predictedQueries << ENDL;
	}
	COUT3 << mxId-1 << " castes integrated"<< ENDL;
}

void CasteManager::loadCastesFromFile(const std::string& fname) {
	listAttWithCasteId lst;
	lst.casteId = 0;
	std::vector<listAttWithCasteId> data;
	std::ifstream in(fname);
	while(in.good()) {
		in >> lst.list >> lst.casteId >> lst.length >> lst.freq;
		data.push_back(lst);
	}
	data.pop_back();
	loadCastesFromAttMatrix(data);
}

void CasteManager::reset(caste_t cid) { casteVec[cid]->resetAfterEviction(); }

std::vector<caste> caste::convertSplitterToCastes(const std::vector<listAttWithCasteId>& sorted) {
	std::vector<caste> res;
	std::unordered_map<caste_t, unsigned> caste2index;

	for(const auto& lst : sorted) {
		if(0 == caste2index.count(lst.casteId)) {
			res.push_back(caste());
			caste2index[lst.casteId] = static_cast<unsigned>(res.size()-1);
			res.back().casteId = lst.casteId;
		}
		caste& cst = res[caste2index[lst.casteId]];
		cst.members.push_back(lst.list);
		cst.predictedQueries += lst.freq;
		cst.predictedUpdates += lst.length;
		TASSERT(res[caste2index[lst.casteId]].members.size(),"res vec is empty!");

	}
	//for(const auto& cst : res) 	COUTL << cst.casteId << " " << cst.members.size() << ENDL;
	return  res;
}

std::vector<size_t> CasteManager::getSortedEpochSizes() const {
	auto castes = getSortedCopy2();
	std::vector<size_t> sizes;
	//CHEAP_ASSERT(castes.back()->casteId == castes.size() && castes.back()->members.size() > 1024*1024, "bad last caste")
	//castes.pop_back();
	sizes.resize(castes.size(),0);
	for(auto cst : castes)
		sizes[cst->casteId - 1] = cst->epochUpdates;
	return sizes;
}

std::vector<size_t> CasteManager::getSortedUBSizes() const {
	auto castes = getSortedCopy2();
	std::vector<size_t> sizes;
	//CHEAP_ASSERT(castes.back()->casteId == castes.size() && castes.back()->members.size() > 1024*1024, "bad last caste")
	//castes.pop_back();
	sizes.resize(castes.size(),0);
	for(auto cst : castes)
		sizes[cst->casteId - 1] = cst->ubPostings;
	return sizes;
}

std::vector<caste*> CasteManager::getSortedCopy2() const {
	return std::vector<caste*>(this->begin(),this->end());
	//return getSortedCopyP([](caste* l, caste* r){ return l->epochQueries >= r->epochQueries; });
	//TODO: interesting, how will using above changes ski-rental w.r.t below?
	//return getSortedCopyP([](caste* l, caste* r){ return l->totalQueries+l->predictedQueries >= r->totalQueries+r->predictedQueries; });
}
std::vector<caste*> CasteManager::getSortedCopy() const {
	return getSortedCopyP(compareCastePtr());
}

////if the temp. caste has enough postings
////it will become a proud member of the caste system
////if it has too many postings we are going to split it and add the sub-castes
////in any case we expect the new caste(s) to be on top of the heap.
//size_t CasteManager::pushPromotedSingletons(bool force) {
//	if(promotedSingletons.members.empty() || promotedSingletons.ubPostings<caste::minWBSize)
//		if(!force) return 0; //not going to happen
//
//	if(promotedSingletons.ubPostings>caste::maxWBSize)
//		COUT4 << ("we need to implement splitting of the promotedSingletons...") << ENDL;
//
//	promotedListsSet.clear();
//
//	auto newId = casteVec.size();
//	add(newId,promotedSingletons);
//	COUT3 << " pushPromotedSingletons - " << newId << " members - " << promotedSingletons.members.size() << " postings - " << promotedSingletons.ubPostings << ENDL;
//	promotedSingletons = caste();
//	return newId;
//}

//@TERMIDORIENTED
void CasteManager::addPtr(size_t id, caste* cst) {
	cst->casteId = static_cast<caste_t>(id);
	cst->sortMembers();
	casteVec.push_back(cst);

	if(! cst->members.empty())
		registerMembersToCaste(cst->members.begin(),cst->members.end(), cst->casteId);
}


size_t CasteManager::pushLongTailTerms(bool force) {
	const auto sz = longTailTerms.size();
	if(sz < caste::minWBSize && !force)
		return 0;
	if(sz > caste::maxWBSize) COUT4 << ("huge singletons!");

	if(casteVec.back()->predictedQueries) { //first time we push those
		COUT2 << "First push of singletons: " << sz << ENDL;
		caste newC;
		add(casteVec.size(),newC);
	}

	std::vector<tid_t> newMembers;
	newMembers.reserve(longTailTerms.size());
	caste& casteForThose = *casteVec.back();
	for(const auto term : longTailTerms) {
		casteForThose.addMember(term.first, term.second, 0);
		newMembers.push_back(term.first);
	}
	registerMembersToCaste(newMembers.begin(), newMembers.end(),casteForThose.casteId);
	casteForThose.updateReg(sz);
	casteForThose.sortMembers();
	COUT4 << "pushing long tail terms " << casteForThose.casteId << " " << sz << ENDL;

	longTailTerms.clear();
	return casteForThose.casteId;
}


CasteSplitter::CasteSplitter(size_t minT, size_t _factor, unsigned vertDiv) :
		 minThreshold(unsigned(minT)), verticalDiv(vertDiv),factor(_factor){}

unsigned CasteSplitter::splitInPlace(std::vector<listAttWithCasteId>& input) { //note -- input is going to be altered
	std::sort(input.begin(),input.end()); //sort by length - descending

	size_t i = 0;
	size_t sumCurrent = 0;
	caste_t runningCasteId = 1; //0 is not a legal id
	while(i<input.size()) { //prefix sum lengths as you go!
		sumCurrent += (input[i].length * factor );
		input[i].casteId = runningCasteId;
		++i;
		if(sumCurrent>=minThreshold) { //we have a new caste
			++runningCasteId;
			sumCurrent = 0;
		}
	}
	return runningCasteId;
}

CasteStats::CasteStats(const CasteManager& m):csm(m){}

void CasteStats::printSegmentsCount(std::ostream& stream)const {
	if(globalVerbosity<6)
		return;
	for(auto cst : csm )
		stream << cst->casteId << " " << cst->getSegmentStack().size() <<" " <<cst->members.size() << "\n";
	stream.flush();

}

void CasteStats::printHist(std::ostream& stream)const {
	stream << "id\tseeks\tqueries\n";
	for(auto cst : csm)
			stream << cst->casteId << "\t" << cst->totalSs() << "\t" << cst->totalQs() << "\n";
}

void CasteStats::print(std::ostream& stream, bool printAll)const{
	stream << "#castes: " << csm.casteVec.size() << std::endl;
	std::vector<caste> castes;
	castes.reserve(csm.casteVec.size());
	for(auto cst : csm)
		castes.push_back(*cst);
	std::sort(castes.begin(),castes.end(),caste::predicateSorterPrint);
	unsigned cnt = 0;
	for(const auto& cst : castes){
		unsigned add = unsigned(cst.ubPostings+cst.totalPostings > 0);
		cnt += add;
	}
	stream << "Hit castes: " << cnt << ENDL;
	if(printAll)
		for(const auto& cst : castes)
			stream << cst;
}

std::ostream& operator <<(std::ostream& stream, const CasteStats& cst) { cst.print(stream); return stream; }


inline double getRatioLife(ProcessTerm* hackGetMaxPosts, bool isRandom, double accSizes, did_t totalPostings, float aggressive,  size_t epochUpdates) {
	if(hackGetMaxPosts) { //this is the evergrow env 1
		auto p = hackGetMaxPosts->getMaxAndUB();
		auto maxPosts = p.first;
		auto totalSeenPostings = reinterpret_cast<const UpdatesBuffer*>(p.second)->seenPostings();
		return (double(maxPosts-totalSeenPostings)) / (double(maxPosts));
	}

	if(isRandom) {
		float factoredSz = float(accSizes)/aggressive;
		double estimatePostingsInSegmentLife =  totalPostings * std::log(factoredSz);
		return estimatePostingsInSegmentLife / (double(epochUpdates));
	}

	double lengthTail = totalPostings-accSizes;
	return lengthTail/(double(totalPostings)); //posting updates until all those are gone
}

//size_t caste::consolidateAuxThink(bool isRandom, ProcessTerm* hackGetMaxPosts, float aggressive) {
//	const auto sssz = size();
//	TASSERT(size() > 1,"nothing to consolidate")
//
//	std::vector<double> wins;
//	wins.reserve(sssz);
//
//	double accSizes = segmentStack.back();
//	for(auto i : make_irange(size_t(2),sssz+1)) {
//		accSizes+=segmentStack[sssz-i];
//		double ratioLife = getRatioLife(hackGetMaxPosts, isRandom, accSizes, totalPostings, aggressive, epochUpdates);
//		double queriesToThisConsolidant = ratioLife * epochQueries / aggressive;
//		double seeksBenefitRoughly = IOStats(0,static_cast<uint64_t>(queriesToThisConsolidant*(i-1)),-1).ioTimeInMinutes();
//		double consolidationPrice = -1; //TODO: broken //consolidationCostWithPB(accSizes); //for non-evergrow the real price is usually lower -- in writeback
//		//COUTL << queriesToThisConsolidant << "," << seeksBenefitRoughly << "," << consolidationPrice << "\n";
//		wins.push_back(seeksBenefitRoughly-consolidationPrice); //record the profit (maybe negative)
//	}
//
//	auto it = std::max_element(wins.begin(),wins.end());
//	if(*it <= 0.0)
//		return 0; //no consolidation is profitable
//
//	//else we consolidate the last 2+offset(it) segments
//	auto lastK = size_t(2+std::distance(wins.begin(),it));
//	size_t sizes = 0;
//	NTIMES(lastK) {
//		sizes+=segmentStack.back();
//		segmentStack.pop_back();
//	}
//	addSegment(sizes);
//	return sizes; //std::make_pair(sizes,sssz-lastK);
//}
