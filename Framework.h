#ifndef FRAMEWORK_H_
#define FRAMEWORK_H_

#include "Lexicon.h"
#include "ProxyIO.h"
#include "UpdatesBuffer.h"
#include "DistributionModels.h"

typedef std::tuple<BufferStats,LexStats,IOStats,IOStats,IOStats,IOStats, std::string > FrameworkStats;

class Framework {
	LexiconForGolden lex;

	DiskReader consolidationReader; //what we had to read to do the merge
	DiskReader queryReader; //the reading for queries
	WriteBackBuffer consolidationWriter; //writing during merges
	WriteBackBuffer updatesWriter; //writing the updates

	DistributionInterface* updatesDistr;
	DistributionInterface* queriesDistr;
	DistributionInterface* deleteDistr;

	UpdatesBuffer* updatesBuffer;
	CasteManager castes;
	ProcessTerm* qp;
	unsigned freeUpTo;

	std::vector<unsigned> auxVectorForDeletes;
	std::vector<unsigned> auxBufferForDeletes;

	static const unsigned MAXT=(86532822); //clueweb

	size_t removeOldDocsAux(size_t currentSz, size_t maxCapacity);
	size_t removeOldDocsCastesRandom(size_t currentSz, size_t maxCapacity);
	size_t removeOldDocsCastesChrono(size_t currentSz, size_t maxCapacity);
	size_t removeOldDocsStaticRandom(size_t currentSz, size_t maxCapacity);
	size_t removeOldDocsStaticChrono(size_t currentSz, size_t maxCapacity);

	BufferStats getUBStats() const { return BufferStats(*updatesBuffer); }
	LexStats getLexStats() const { return LexStats(lex); }

	static IOStats getIO(const DiskIO& d, const std::string& label) { return IOStats(d.getTotalBytes(),d.getTotalSeeks(),label); }

	std::string getQpStats() const;
	FrameworkStats getStats() const;
	void printAllStats(bool isCastes) const;


public:
	Framework();
	~Framework();

	void setup(std::vector<listAttWithCasteId>& casteSorted, //will be erased!
			DistributionInterface* updD, DistributionInterface* queriesD,
			const std::string& qProc, const std::string& evictor, size_t postingBufferSize, size_t freeUp2, size_t expectedTerms, size_t cacheSize,
			std::pair<unsigned, unsigned> updatesToQueriesRatio,unsigned aggressive, 
			const std::string& jumpstartFile="",bool loadIOs=false);
	void reset();

	CasteStats getCasteStats() const { return CasteStats(castes); }
	//will run at most 'cycles' -- fill-empty round and at most 'postings' seen (the first stops the loop)
	std::pair<BufferStats,BufferStats> run(unsigned cycles,  const size_t postings, std::pair<unsigned, unsigned> updatesToQueriesRratio, unsigned printStatsFrequency, size_t stableCapacity,
			bool IS_RANDOM_DEL, bool IS_SKI, size_t maxPostings);

	void serializeStateToStream(std::ostream& out, std::pair<unsigned, unsigned> updatesToQueriesRatio) const;
	void jumpstartPrevStateFromStream(std::istream& in, std::pair<unsigned, unsigned> updatesToQueriesRatio, bool restoreStatsIO=false);
};

std::ostream& operator <<(std::ostream& stream, const FrameworkStats& bs);
#endif /* FRAMEWORK_H_ */
