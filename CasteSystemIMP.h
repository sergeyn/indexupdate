
inline void CasteManager::putBack(caste* cst) {
	cst->resetAfterEviction();
}

inline void CasteManager::add(size_t id, const caste& nd) { addPtr(id, new caste(nd)); }


inline const caste& CasteManager::operator [](size_t id) const {
	return (*casteVec[tidToCid[id]]);
}

inline caste& CasteManager::operator [](size_t id) {
	return (*casteVec[tidToCid[id]]);
}

/*
 * If this term is a member of a known caste C - increase(C)
 * else -- either an unseen or was observed only once -- addSingleton will handle either
 */
inline void CasteManager::increaseWithCheck(const size_t _id, unsigned by) {
	auto id = static_cast<tid_t>(_id);
	if(likely(hasCaste(id))) {
		increase(id,by);
	}
	else //unseen yet
		addLongTailTerm(id);
}


template<typename Predicate>
std::vector<caste*>  CasteManager::getSortedCopyP(Predicate p) const {
	auto cp = std::vector<caste*>(this->begin(),this->end());
	std::stable_sort(cp.begin(),cp.end(),p); //normal sort had some kind of a problem with cp.end() at some point?
	return cp;
}
