#ifndef READRAW_H_
#define READRAW_H_

//#include <fstream>
//#include <tuple>

//Read a space separated file into typed tuples

template<typename Tuple, typename Stream, int index, int last>
struct tp_filler {
	tp_filler(Tuple& tp, Stream& s) {
		typename std::tuple_element<index,Tuple>::type tmp;
		s >> tmp;	//COUT << tmp << " " << s.good() << " " ;
		std::get<index>(tp) = tmp;
		tp_filler<Tuple,Stream,index+1,last>(tp, s);
	}
};
//specialize to end recursion
template<typename Tuple, typename Stream, int N>
struct tp_filler<Tuple,Stream,N,N> { tp_filler(Tuple& , Stream& ) {} };

template<typename Tuple, typename Stream>
bool fillTupleFromStream(Tuple& tp, Stream& s) {
	tp_filler<Tuple,Stream,0,std::tuple_size<Tuple>::value>(tp,s);
	return s.good();
}

template<typename Tuple, typename Stream, char Sep, int index, int last>
struct tp_writer {
	tp_writer(Tuple& tp, Stream& s) {
		s << std::get<index>(tp) << Sep;
		tp_writer<Tuple,Stream,Sep,index+1,last>(tp, s);
	}
};
//specialize to end recursion
template<typename Tuple, typename Stream, char Sep, int N>
struct tp_writer<Tuple,Stream,Sep,N,N> { tp_writer(Tuple& , Stream& ) {} };

template<typename Tuple, typename Stream, int isEOL=0>
Stream& writeTupleToStream(Tuple& tp, Stream& s) {
	tp_writer<Tuple,Stream,' ',0,std::tuple_size<Tuple>::value>(tp,s);
	if(isEOL) s << "\n";
	return s;
}

template<typename Tuple>
class SSV {
	std::vector<Tuple> data;
	std::ifstream stream;
	bool moreFlag;
public:
	SSV(const std::string& fname,unsigned skipFirstLines=0) :stream(fname), moreFlag(true){
		if(!stream.good()) FATAL("stream");

		std::string tmpS;
		for(unsigned i=0; i<skipFirstLines; ++i) std::getline(stream,tmpS);
	}

	void readAll() { //read all to internal data
		Tuple tp;
		while(fillTuple(tp,stream))
			data.push_back(tp);
	}

	bool hasNext() const { 	return stream.good() && moreFlag; }

	Tuple getNext() { //read one by one
		Tuple tp;
		moreFlag = fillTupleFromStream(tp,stream);
		return tp;
	}

	unsigned size() const { return data.size(); }

	const Tuple& operator[](unsigned n) const { return data[n]; }
	template <unsigned index>
	const typename std::tuple_element<index,Tuple>::type get(unsigned n) const { return std::get<index>(data[n]); }
};

#endif /* READRAW_H_ */
