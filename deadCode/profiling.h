#ifndef _PROFILING_H_
#define _PROFILING_H_

class oldTimer {
private:
	timeval startT;
	double total;
	unsigned long count;

public:
	oldTimer() { reset();	}

		double getTotal() const { return total; }
		unsigned long getCount() const { return count;}

		inline void start() {/* gettimeofday(&startT,0); */}

		void reset() { total = count = 0; }

		inline double end() {
			timeval end;
			//gettimeofday(&end,0);
			double t0=(startT.tv_sec*1000000.0)+startT.tv_usec;
			double t1=(end.tv_sec*1000000.0)+end.tv_usec;

			double delta=(t1-t0);
			total+= delta;
			++count;
			return  delta;
		}
};

class sTimer {
private:
	std::chrono::time_point<std::chrono::high_resolution_clock> startT;
	double total; 
	unsigned long count;
	
public:
		sTimer() { reset();	}

		double getTotal() const { return total; }
		unsigned long getCount() const { return count;}
				
		inline void start() { startT = std::chrono::high_resolution_clock::now(); }
					
		void reset() { total = count = 0; }

		inline double end() {

			auto endT = std::chrono::high_resolution_clock::now();
			auto delta = std::chrono::duration_cast<std::chrono::microseconds>(endT-startT).count();

			total+= delta;
			++count;
			return  delta;
		}
};

struct dummyProfiler {
	static dummyProfiler dummy;
	inline void zeroCounter(size_t ) {}
	inline void stepCounter(size_t , const unsigned long int) {}
	static inline dummyProfiler& getTheInstance() { return dummy; }
};

class profilerC { //single threaded
private:
	std::map<int,std::string> comments;	
	std::map<int,sTimer> timers;
	std::map<int,unsigned long> cyclesAtStart;
	std::vector<unsigned long int> counters;

	unsigned long cycles;
	
	static profilerC GlobalProfiler;
	profilerC();

	void setCosts();
	
public:
	static inline profilerC& getTheInstance() { return GlobalProfiler; }

	size_t initNewCounter() {
		counters.push_back(0);
		return counters.size()-1;
	}

	inline void stepCounter(size_t cnt, const unsigned long int inc=1) { counters[cnt]+=inc; }
	inline void zeroCounter(size_t cnt) { counters[cnt]=0; }
	inline  unsigned long int getCounter(size_t cnt) { return counters[cnt]; }

	void add(const std::string& comment, int id) {
		comments[id] = comment;
		timers[id] = sTimer();
	} 
	
	void reset(int id) { timers[id].reset();}

	inline void start(int) {
		#ifdef TIMING
		//cyclesAtStart[id]=cycles;
		timers[id].start();
		#endif
	}

	inline double end(int) {
		#ifdef TIMING
		//double deduct = cycleCost*(cyclesAtStart[id]-cycles);
		//++cycles;
		return timers[id].end();
		#else
			return 0;
		#endif
	}
	void printReport() const;
};

void thrashSomeCache(const unsigned size);
#define STARTTIMER sTimer __localTimerFromMacro; __localTimerFromMacro.start();
#define ENDTIMER_MS  (__localTimerFromMacro.end()/1000.0)
#ifdef COUNTING
#define GLOBALPROFILER profilerC::getTheInstance()
#else
#define GLOBALPROFILER dummyProfiler::getTheInstance()
#endif
#endif
