#ifndef ENUMS_H_
#define ENUMS_H_


//#include <ostream>
//#include <cassert>
//#include <vector>
//#include <string>
//#include <algorithm>
//#include <sstream>


namespace enm {
	//NOTE don't use the word e-n-u-m in comments - the auto parser isn't terribly smart
	enum updateAction { TYPE1, TYPE2 };
	enum traverseAction { tTYPE1, tTYPE2 };
	enum ActionType { UPD, RD, NIL };
}



template<typename T>
struct _enm
{
    static const std::vector<std::string> vnames;
    static const T values[];

    static std::string to_s(T value) {
    	unsigned i=0;
    	while(i<vnames.size() && values[i]!=value /*safeguard*/) ++i;
    	assert(i<vnames.size());
    	return vnames[i];
    }

    static T from_s(const std::string& s) {
    	std::vector<std::string>::const_iterator it = std::find(vnames.begin(), vnames.end(), s);
    	assert(it!=vnames.end());
    	return values[it-vnames.begin()];
    }

    template<typename R>
    static T from_t(R v) {
    	unsigned i=0;
    	while(i<vnames.size() && values[i]!=v /*safeguard*/) ++i;
    	assert(i<vnames.size());
    	return values[i];
    }

    static std::vector<std::string> getVFromS(const std::string& str) {
    	const auto len = str.length();
    	std::vector<std::string> strings;
    	std::stringstream temp;
    	for(auto i = (len-len); i < len; i ++) {
    		if(isspace(str[i])) continue;
    	    else if(str[i] == ',') {
    	        strings.push_back(temp.str());
    	        temp.clear();
    	    }
    	    else temp<< str[i];
    	}
    	strings.push_back(temp.str());
    	return strings;
    }

    struct iterator {
    	unsigned id;
    	iterator(unsigned i=0) : id(i){}
    	T operator*() const{ return _enm::values[id];}
    	iterator& operator++() { ++id; return *this; }
    	bool operator!=(const iterator& it) const { return id!=it.id;}
    };

    static iterator begin() { return iterator(); }
    static iterator end() { return iterator(vnames.size()); }
};


#define __ENM_(name, ...) \
template<> const std::vector<std::string> _enm<name>::vnames(getVFromS(#__VA_ARGS__));\
template<> const name _enm<name>::values[] = {__VA_ARGS__};

#endif /* ENUMS_H_ */
