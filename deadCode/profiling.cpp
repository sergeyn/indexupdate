/*
 * profiling.cpp
 *
 *  Created on: Jan 12, 2012
 *      Author: sergeyn
 */
#include "profiling.h"
#include "Globals.h"

profilerC profilerC::GlobalProfiler;
dummyProfiler dummyProfiler::dummy;

enum counters { TEST1, TEST2, NOP };
//__ENM_(counters,TEST1, TEST2, NOP);
profilerC::profilerC() :cycles(0) {
	//setCosts();
	for(unsigned i=0; i<NOP; ++i) {
		initNewCounter();  //initialize named counters
		abort();
		//add(_enm<enm::counters>::vnames[i],		int(i)); //init named timers
	}
}

void profilerC::printReport() const {
	std::map<int,sTimer>::const_iterator it;
	for ( it=timers.begin() ; it != timers.end(); ++it ) {
		int id = (*it).first;
		const sTimer& t = (*it).second;
		const std::string cm((*comments.find(id)).second);
		if(t.getCount()<1) {
			COUTV(5) << "skipping timer " << cm << ENDL;
			continue;
		}

		unsigned long count = t.getCount();
		double total = 	t.getTotal()/1000.0; //in ms
		
		COUT1 << id << ": "<< cm <<
		" total(ms): " << total /*-(cycleCost*(t.getCount()/100000000))*/
	 	<< /*"(" << total << */" count: " << count  <<
		" avg: " <<   total/count << ENDL;

	}
		abort();
//	for(unsigned long c : counters) {
//		//if(c>0) {COUT1 << i << ": " << _enm<enm::counters>::vnames[i] << " count: " << c << ENDL;}
//		++i;
//	}
}

void thrashSomeCache(const unsigned size) {
             char *c = static_cast<char *>(malloc(size));
             for (unsigned i = 0; i < 0xffff; i++)
               for (unsigned j = 0; j < size; j++)
                 c[j] = char(i^j);
             free(c);
}
