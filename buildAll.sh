#!/usr/bin/env bash
make clean
if [ -z "$1" ] 
  then
    make clean && make -j 4 && cp upd updHD
    make clean && make -j 4 ssd=1 && cp upd updSSD
  else 
    make clean && make -j 4 largeflags=$1 && cp upd updHD_$1
    make clean && make -j 4 largeflags=$1 ssd=1 && cp upd updSSD_$1
fi
