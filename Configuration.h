/*
 * Configuration.h
 *
 *  Created on: 9 Jan 2016
 *      Author: sergeyn
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include "Globals.h"
#include "Numeric.h"
#include "Options.h"

struct configuration {
	unsigned printStatsFrequency;
	size_t MAXT;

	//updates/queries distribution
	unsigned newTermFrequency;
	unsigned newQueryTermFreq;

	size_t postingBufferSize; //size of the update buffer in postings

	//caste size limits
	size_t minWBSize;
	size_t maxWBSize;

	unsigned avgPostingInDoc;
	unsigned qTermsInTraining;
	unsigned freeUpTo;
	unsigned aggressive;
	unsigned szOfPostingBytes;

	unsigned updatesCountInStream;
	unsigned queriesCountInStream;
	unsigned castesScale;

	bool forceMergeGeoInCastes;
	std::pair<unsigned,unsigned> compareMargin;

	unsigned runCycles;
	size_t runPostings;

	std::string qprocAlg;
	size_t stableCapacity;
	size_t CACHESZ;
	bool IS_DEL_RAND;
	bool IS_SKI;
	bool LOADPROXIES;
	std::string JUMPSTART;

	configuration() {
		printStatsFrequency = 64;
		MAXT = NUMERIC::MAXT;

		//updates/queries distribution
		newTermFrequency = NUMERIC::newTermFrequency;
		newQueryTermFreq = NUMERIC::newQueryTermFreq;

		postingBufferSize = 1<<24; //size of the update buffer in postings

		//caste size limits
		minWBSize = NUMERIC::bytesReadInTimeOfSeek/NUMERIC::szOfPostingBytes;
		maxWBSize = NUMERIC::bytesReadBeforeTimeout/NUMERIC::szOfPostingBytes;

		avgPostingInDoc = NUMERIC::avgPostingInDoc;
		qTermsInTraining = NUMERIC::qTermsInTraining;
		freeUpTo = 10; //percents of UB size
		aggressive = 1;
		szOfPostingBytes = NUMERIC::szOfPostingBytes;

		updatesCountInStream = NUMERIC::updatesCountInStream;
		queriesCountInStream = NUMERIC::queriesCountInStream;
		castesScale = 1;
		compareMargin = NUMERIC::compareMargin;

		runCycles=0;
		runPostings=0;
		stableCapacity = 0;
		CACHESZ = 0;

		IS_DEL_RAND = false;
		IS_SKI = true;
		LOADPROXIES = false;
		JUMPSTART = "";

		forceMergeGeoInCastes = false;
	}


	// Config modifiers:

	//if nothing compares equal -- never merge: std::make_pair(1000,1) //too large num will cause an overflow
	//if all compares equal -- always merge: std::make_pair(0,1)
	//if the margin is around 1, it is the logarithmic scheme -- std::make_pair(15,16) -- default scheme
	static configuration remergeAlways(const configuration& cfg) {
		configuration nCfg(cfg);
		nCfg.compareMargin = std::make_pair(0,1);
		return nCfg;
	}
	static configuration remergeNever(const configuration& cfg) {
		configuration nCfg(cfg);
		nCfg.compareMargin = NUMERIC::compareMarginForNever;
		return nCfg;
	}
	static configuration remergeLog(const configuration& cfg) {
		configuration nCfg(cfg); //uses default compare margin
		nCfg.compareMargin = NUMERIC::compareMargin;
		return nCfg;
	}

	static configuration mergeSelect(unsigned mergerSelect,const configuration& cfg) {
		switch (mergerSelect) {
			case 1: return remergeAlways(cfg); break;
			case 2: return remergeLog(cfg); break;
			case 0: return remergeNever(cfg); break;
			default: FATAL("no merge of that kind"); break;
		}
		return cfg;
	}
	static configuration ratesUpdatesQueries(const configuration& cfg, unsigned u, unsigned q) {
		configuration nCfg(cfg);
		nCfg.updatesCountInStream=u;
		nCfg.queriesCountInStream=q;
		COUT4 << "Upd("<<nCfg.updatesCountInStream<<") Queries("
				<< nCfg.queriesCountInStream << ")" << ENDL;
		return nCfg;
	}

	static const configuration& getConfig() { return globalConfig; }
	static const Options& getOptions() { return globalOptions; }
private:
	friend int main(int argc, char * argv[]);
	static configuration globalConfig;
	static Options globalOptions;
};


void signal_handler(int signal)
{
  COUTL << "caught abrt" << ENDL;
  exit(signal);
}


#endif /* CONFIGURATION_H_ */
