#include "UpdatesBuffer.h"
#include "Numeric.h"
#include "Lexicon.h"
#include "QP.h"
#include "Asserts.h"
#include "DeleteBuffer.h"
#include "IO.h"
#include "BufferManageSimulator.h"
#include "Prognosticator.h"

constexpr bool testDegradation(false); 	//Note: set to true when testing degradation!

void UpdatesBuffer::CasteEvictor::coreReset(unsigned _round) {
	CHEAP_ASSERT(_round<2,"why so many rounds?")
	round = _round;

	if(round == 0) {
		shares.clear();
		shares.resize(sizes.size(),1);
		//compensate first ones:
		shares[0] = 4; shares[1] = 3; shares[2] = 2;
		TASSERT(shares.size()>2,"too small")

		auto total = shares.size()-3+(4+3+2); //first 3 sum up to 10
		auto cShare = maxUbPostings / total;
		//CHEAP_ASSERT(cShare>=minWbSz,"can't work")
		for(size_t i=0; i<shares.size(); ++i)
			shares[i] *= cShare;
	}
	else {
		shares.clear();
		shares.resize(sizes.size(),0);
	}
}

void UpdatesBuffer::CasteEvictor::injectSizesSim(const std::vector<size_t>& rsizes) {
	sizes = rsizes;
}
//----------------------------------------------------------------------------
//void UpdatesBuffer::CasteEvictor::reset(unsigned _round) {
//	auto& casteManager =  buffer->castes;
//	casteManager.pushLongTailTerms(_round != 0); //TODO: this is fishy!!!
//	sortedCopy = casteManager.getSortedCopy2();
//
//	sizes.clear(); sizes.reserve(sortedCopy.size());
//	queries.clear(); queries.reserve(sortedCopy.size());
//	for(auto c : sortedCopy) {
//		sizes.push_back(c->ubPostings);
//		queries.push_back(c->predictedQueries + c->totalQueries);
//	}
//
//	UpdatesBuffer::CasteEvictor::coreReset(this->buffer->getMaxAllowedPostings(),_round);
//}
//----------------------------------------------------------------------------
UpdatesBuffer::CasteEvictor::CasteEvictor(size_t maxUbPosts, const UpdatesBuffer* ub) :
		maxUbPostings(maxUbPosts),round(0) {
	CHEAP_ASSERT(maxUbPosts > 0 || !ub,"zero ub");
	if(ub) {
		sortedCopy = ub->castes.getSortedCopy2();
	}
}
//----------------------------------------------------------------------------
size_t UpdatesBuffer::CasteEvictor::nextId() {
	if(sizes.empty())
		return  BUFF_LIMIT;

	sizes.pop_back();
	shares.pop_back();
	return sizes.size();
}
//----------------------------------------------------------------------------
tid_t  UpdatesBuffer::CasteEvictor::next() {
	auto nxt = nextId();
	if(nxt == BUFF_LIMIT) {
		FATAL("not here")
		coreReset(round+1);
		nxt = nextId();
	}
	auto cst = sortedCopy.at(nxt);
	TASSERT(!cst->members.empty(),"empty caste!")
	return cst->members.front();
}
//----------------------------------------------------------------------------
UpdatesBuffer::UpdatesBuffer(std::string /*evctr*/,
		DistributionInterface& preloadedD,
		CasteManager& cstM,
		WriteBackBuffer& wbb,WriteBackBuffer& cwbb, DiskReader& crdr,
		listAttributesUpdater& lex, LexiconForGolden& lexP,
		size_t threshold,
		tid_t reserve, QPBase* qp) :
				totalPostings(0),totalSeenPostings(0),evictions(0),maxAllowedPostings(threshold),castes(cstM),
				distribution(reinterpret_cast<DistrFromFile&>(preloadedD)),
				writeBackBuffer(wbb), consolidationWriteBackBuffer(cwbb),consolidationReader(crdr),
				globalLexUpd(lex),globalLexProvider(lexP), remerge(consolidationWriteBackBuffer,consolidationReader,lex),
				urfManager(wbb,cwbb,crdr), qProc(qp),environment(0),totalExperimentPostings(0) {
	tidsToLists.resize(reserve);
}

//----------------------------------------------------------------------------
void UpdatesBuffer::reset() {
	tidsToLists.clear(); //note: all pending things in buffer will be lost. consider forcing WB before.
	totalPostings = 0;
	totalSeenPostings = 0;
	//note 2: maybe caste stats ought to be changed as well
}
//----------------------------------------------------------------------------
void UpdatesBuffer::jumpstart(bool shouldReset, bool isCastes) { //perform updates of the system (cheaper perhaps) without caring for eviction until maxPosting is reached
//	sTimer t; t.start();
	if(shouldReset)
		reset();
	unchar pct = 0;
	while(!isFull(isCastes)) {
		for(unsigned i=0; i<512; ++i) {
			update();
			update();
			update();
		}
		if(pct!=percentFull()) {
			pct = percentFull();
			CERRV(5) << (unsigned)pct << "% buf\r"; std::cerr.flush();
		}
	}
	//evictor->reset();
	COUT3 << "Jumpstart ended\n"; // " << t.end()/1000.0f << " ms" << ENDL;
}
//----------------------------------------------------------------------------
tid_t ubLexiconAndPostings::getMaxTid() const { //not O(1)!
	for(int i=data.size()-1; i>=0; --i)
		if(data[i]) return i;
	return 1;
}
//----------------------------------------------------------------------------
bool UpdatesBuffer::isFull(bool isCastes) const {
	//compensate the special long case
	return (totalPostings -
		    (isCastes ? castes.back().ubPostings : 0)
		>= maxAllowedPostings);
}
//----------------------------------------------------------------------------
size_t UpdatesBuffer::evictWithURange()
{
#ifdef URF
	++evictions;
	const size_t totalEvicted = urfManager.process(globalLexUpd,globalLexProvider);
	totalSeenPostings += totalEvicted;
	CHEAP_ASSERT(totalEvicted<=totalPostings,"evicting more than we have")
	totalPostings-=totalEvicted;
	return totalEvicted;
#endif
	return 0;
}
//----------------------------------------------------------------------------
//in chrono -- remove from most ancient segments -- only if full removal
unsigned UpdatesBuffer::tryShiftStack(HistDeletes& deletes, std::vector<size_t>& sizeStack) {
	auto shiftStack = deletes.deleteChronoMonolithicTidsByStack(sizeStack);
	if(shiftStack > 0) {
		sizeStack.erase(sizeStack.begin(),sizeStack.begin()+shiftStack);
		shiftMembers(shiftStack); //fix flags!
	}
	return shiftStack;
}
//----------------------------------------------------------------------------
void UpdatesBuffer::tryShiftStackForCastes(HistDeletes& deletes) {
	for(auto cst : castes) {
		auto& stack = cst->getSegmentStackUnsafe();
		auto shiftStack = deletes.deleteChronoCasteTidsByStack(cst->casteId, stack);
		if(shiftStack > 0) {
			stack.erase(stack.begin(),stack.begin()+shiftStack);
			shiftMembers(shiftStack,cst->members); //fix flags!
			if(stack.empty())
				COUT << "caste " << cst->casteId << " is empty " << ENDL;
		}
	}
}
//----------------------------------------------------------------------------
void UpdatesBuffer::shiftMembers(unsigned by, const std::vector<tid_t>& members) {
	static_cast<LexiconForGolden&>(globalLexUpd).shiftAllFlags(by,members);
}
//----------------------------------------------------------------------------
void UpdatesBuffer::touchGlobalWithMax() {
	tid_t maxTermId = tidsToLists.getMaxTid();
	flagsData zeroF;
	b_zeroFlags(zeroF);
	LexiconForGolden& globalLex = static_cast<LexiconForGolden&>(globalLexUpd);
	globalLex.orFlag(maxTermId,zeroF); //will cause a single resize per batch ;)
}

void ubLexiconAndPostings::clear() {
	auto sz = data.size();
	data.clear();
	data.resize(sz,0);
}
size_t ubLexiconAndPostings::countTerms() const {
	return std::count_if(data.begin(),data.end(),
			[](const did_t& p){return p;});
}
//----------------------------------------------------------------------------
size_t UpdatesBuffer::evictWithRemerge(HistDeletes& deletes) {
	++evictions;
	totalSeenPostings += totalPostings;
	const auto totalEvicted = totalPostings;
	touchGlobalWithMax();
	//perform merging according to chosen monolithic policy
	auto oldSz = remerge.getStackSize();
	remerge.currentBatch(deletes,totalPostings, environment);
	auto currentSz = remerge.getStackSize();
	CHEAP_ASSERT(currentSz <= MAX_SEGMENTS,"too many segments");

	genericUpdateGlobal(static_cast<LexiconForGolden&>(globalLexUpd),
			make_irange(tidsToLists.getMaxTid() + 4),oldSz,currentSz);

	tidsToLists.clear();
	totalPostings=0;
	return totalEvicted;
}
//----------------------------------------------------------------------------
//ret true if merge
template<typename IdsContainer>
bool  UpdatesBuffer::genericUpdateGlobal(LexiconForGolden& globalLexUpdate,
						const IdsContainer& tids, unsigned oldSz, unsigned currentSize) {

	CHEAP_ASSERT(currentSize < MAX_SEGMENTS,"too long!")

	auto masks = MasksForLexFixing::safeMasksForLexFixing(oldSz,currentSize); //maskAwayLow,removeHigh,orMask
	bool mergeFlag = (oldSz >= currentSize); // { x 1 -> x 2 } or { x 2 1 -> x 4 }

	globalLexUpdate.resize(*std::max_element(tids.begin(),tids.end()));

	if(mergeFlag) { //there is a merge!
		//we only set to 1 those that had at least one bit set in removed segments
		for(auto candidate : tids)
			globalLexUpdate.setCondBit(candidate,std::get<0>(masks),std::get<1>(masks),std::get<2>(masks));
	}

	//COUTL << "tidsToLists: " <<  tidsToLists.size() << " given: " << std::distance(tids.end(),tids.begin())-1 << ENDL;
	auto newSegmentFlag = std::get<2>(masks);
	for(auto candidate : tids) {
		const did_t len = tidsToLists[candidate];
		if(len) {
			globalLexUpdate.orFlag(candidate,newSegmentFlag);
			globalLexUpdate.addLength(candidate,len);
		}
	}

	return mergeFlag;
}

//----------------------------------------------------------------------------
template<typename IdsContainer>
void  UpdatesBuffer::tweakSingletonsForGlobal(LexiconForGolden& globalLexUpdate, const IdsContainer& tids) {
	flagsData flag = getZeroFlag();
	b_setBit(flag,0);
	b_setBit(flag,1);
	b_setBit(flag,2);

	for(auto candidate : tids)
			globalLexUpdate.setFlag(candidate,flag);
}
//----------------------------------------------------------------------------
//this happens often for small ub + low query setups
//but if we use log-merge here with branching of sizeof(flagsData) we should have small overhead
//perform remerging, but assuming the new data is in
ConsolidationStats UpdatesBuffer::handleCasteSegmentOverflow(caste& cst) {
	auto where = ReMergePolicy::whereTelescopic(cst.getSegmentStack());
	if(where >= MAX_SEGMENTS - 1) //sort of dirty hack...
		where -= 4;
	auto oldSz = cst.size()-1;
	++cst.overflowConsolidations;
	auto paidForIO = auxConsolidateTail(cst.getSegmentStackUnsafe(),where,0,1);
	auto mergingFlag = genericUpdateGlobal(static_cast<LexiconForGolden&>(globalLexUpd),
									cst.members, oldSz, cst.size());

	CHEAP_ASSERT( mergingFlag, "why wasn't cond. used?")
	return paidForIO;
}

//----------------------------------------------------------------------------
ConsolidationStats  UpdatesBuffer::attemptAtEvictionCasteConsolidation(caste& cst, HistDeletes& deletes) {
	const bool overflowSegFlag = cst.size() + 1 >= MAX_SEGMENTS;
	auto oldSz = cst.size();
	cst.addSegment(cst.ubPostings);
	//special chrono case:
	if(environment == Env::CHRONO && cst.size() == 1
			&& cst.casteId < deletes.castesCount() && deletes.casteDeletes(cst.casteId)) {
		COUTL << cst.casteId << " has " << cst.getSegmentStackUnsafe()[0] << " and deletes: " << deletes.casteDeletes(cst.casteId) << ENDL;
		CHEAP_ASSERT(cst.getSegmentStackUnsafe()[0]>=deletes.casteDeletes(cst.casteId),"surely deleting more than we have");
		deletes.partialDeleteBeforeMerge(cst.casteId,cst.getSegmentStackUnsafe()[0]);
	}

	if(overflowSegFlag || testDegradation) { //this caste is going to overflow the flagData!
		if(cst.casteId != castes.back().casteId) { //this is not the last caste (singletones)
			return handleCasteSegmentOverflow(cst); //will call update global lex inside
		}
		else {
			++cst.overflowConsolidations;
			tweakSingletonsForGlobal(static_cast<LexiconForGolden&>(globalLexUpd),
					cst.members);
			return ConsolidationStats(0,0,cst.ubPostings,0); //zero seeks, because we batch all non-consolidating writes!
		}
	}

	//consolidation consists of: segmentStack fixing, lexicon flags fixing
	if(cst.evictionRounds) { //if there is something to consolidate

		cst.prepareTokens();
		auto where = cst.whereAffordConsoidation(totalSeenPostings,qProc->getMaxAndUB().first);
		if(where >= cst.size()-1) {
			++cst.stats[caste::AT_EVICTION_WRITE_BACK];
		}
		if( environment == Env::CHRONO && where == 0 )  //merging all to one in Chrono!
			deletes.partialDeleteBeforeMerge(cst.casteId,cst.getSegmentStackUnsafe()[0]);

		auto stackCopy(cst.getSegmentStack());
		auto consdata =  cst.anyConsolidateTail(where);
		//perhaps there was no consolidation?
		if(oldSz >= cst.size() ) {//ok, there was merge!
			if(environment == Env::RNDDEL) {
				auto reimbursePostingsWritten = deletes.deleteRndCasteTidsByStack(cst.casteId,stackCopy,where);
				consdata.wdata -= reimbursePostingsWritten;
				COUT4 << "Reimbursed " << reimbursePostingsWritten << " postings for " << cst.casteId << ENDL;
			}

			const double cost = ConsTime::consolidationDataToMinutes(consdata);
			cst.tokens -= cost; //in a vanilla ski-rental we do not tweak the tokens
			auto mergingFlag = genericUpdateGlobal(static_cast<LexiconForGolden&>(globalLexUpd),
					cst.members, oldSz, cst.size());

			CHEAP_ASSERT( mergingFlag, "why wasn't cond. used?")
			CHEAP_ASSERT(consdata.rdata,"why no reads?");
			if(cst.epochQueries == 0)
				CHEAP_ASSERT(cst.tokens>=0,"why negative?");

			return consdata;
		}
		else {
			CHEAP_ASSERT(consdata.rdata == 0, "something is fishy!");
			CHEAP_ASSERT(oldSz + 1 == cst.size(), "something is fishy!");
		}
	}
	//not consolidating at all:
	auto mergingFlag = genericUpdateGlobal(static_cast<LexiconForGolden&>(globalLexUpd),
			cst.members, oldSz, cst.size());
	CHEAP_ASSERT( false == mergingFlag, "why used cond.")
	return ConsolidationStats(0,0,cst.ubPostings,0); //zero seeks, because we batch all non-consolidating writes!
}
//----------------------------------------------------------------------------
//eviction demands:
//1) reducing the postings in UB
//2) updating lengths of terms (members)
//3) updating the flags of the terms
//		3.1) Note, if we made a consolidation we update the flags according to new segmentStack config
ConsolidationStats UpdatesBuffer::evictOneCase(caste& cst,HistDeletes& deletes) {
	totalPostings -= cst.ubPostings;
	totalSeenPostings += cst.ubPostings;
	//COUTL << cst.casteId << " " << cst.ubPostings << ENDL;
	auto paidForIo = attemptAtEvictionCasteConsolidation(cst,deletes);
	for(auto candidate : cst.members)
		tidsToLists.erase(candidate);

	cst.totalPostings += cst.ubPostings;
	cst.evicted(cst.ubPostings);
	castes.reset(cst.casteId); //will reset the caste, including ubPostings
	CHEAP_ASSERT(1 <= cst.size(),"no segments!");
	return paidForIo;
}
//----------------------------------------------------------------------------
void simulateForPrognosticator(CasteManager& cm, size_t maxUBPostings, size_t totalExpPostings, unsigned freeUpPostings) {
	const double AGGRESSIVE = cm.getAggressive();
	Upd::BufferManageSimulator sim(maxUBPostings);
	auto castes = cm.getSortedCopy2();
	std::vector<size_t> cEpochUpdates;
	CHEAP_ASSERT(castes.back()->casteId == castes.size() && castes.back()->members.size() > 1024*1024, "bad last caste")
	castes.pop_back();
	cEpochUpdates.resize(castes.size(),0);
	for(auto cst : castes)
		cEpochUpdates[cst->casteId - 1] = cst->epochUpdates;
	sim.predictForPrognosticator(cEpochUpdates,cm.getSortedUBSizes(),totalExpPostings,freeUpPostings);

	const auto evictedSizes = sim.getEvictions();
	CHEAP_ASSERT(evictedSizes.size() == cEpochUpdates.size(),"missed");
	for(unsigned i = 0; i < evictedSizes.size(); ++i) {
		caste& cstRef = cm.getByCID(i+1);
		CHEAP_ASSERT(cstRef.epochQueries,"can't divide")
		std::vector<size_t> casteSegments(cstRef.getSegmentStack().begin(),cstRef.getSegmentStack().end());
		casteSegments.insert(casteSegments.end(),evictedSizes[i].begin(),evictedSizes[i].end());
		cstRef.predictedEvictions = casteSegments;
		if(casteSegments.size()>1) {
			BaseScheduler bs(casteSegments,
							double(cstRef.totalPostings+cstRef.ubPostings)/(double(cstRef.totalQueries+1)*AGGRESSIVE)); //double(cstRef.epochUpdates)/double(cstRef.epochQueries)
			bs();
			if(cstRef.casteId == 3) {
				for(double agg : {0.5, 1.0, 6.0}) {
					BaseScheduler bs(casteSegments,
								double(cstRef.totalPostings+cstRef.ubPostings)/(double(cstRef.totalQueries+1)*agg)); //double(cstRef.epochUpdates)/double(cstRef.epochQueries)
					bs();
					COUTL << agg << ' ' << bs.totalCostForSeeks << ENDL;
				}

			}
			cstRef.consolidations = bs.consolidations();
		}
		else
			cstRef.consolidations.clear();
	}
}
//----------------------------------------------------------------------------
size_t UpdatesBuffer::evictWithCastes(did_t freeUpto,HistDeletes& deletes) {
	CHEAP_ASSERT(maxAllowedPostings>=freeUpto,"wrong limits!")
	ConsolidationStats updateIO;
	ConsolidationStats consolidateIO;
	size_t totalEvicted = 0;
	castes.pushLongTailTerms(false);

	if((*castes.begin())->epochQueries)
		simulateForPrognosticator(castes, maxAllowedPostings, totalExperimentPostings - totalSeenPostings, maxAllowedPostings-freeUpto);

	UpdatesBuffer::CasteEvictor evictor(maxAllowedPostings,this);
	evictor.injectSizesSim(castes.getSortedUBSizes());
	evictor.coreReset(0);
	tid_t candidateCaste = evictor.next(); //fetch the first candidateId

	while(1) { // caste eviction loop
		auto& cst = castes[candidateCaste]; //get a candidate from candidateId
		totalEvicted += cst.ubPostings;

		auto thisIO = evictOneCase(cst,deletes);
											  //no consolidation
		ConsolidationStats &whereToWriteTo = (thisIO.rdata == 0) ? updateIO : consolidateIO;
		whereToWriteTo += thisIO;
		if(totalPostings<maxAllowedPostings-freeUpto)
			break;
		candidateCaste = evictor.next(); //fetch next candidateId
	}//end eviction loop

	//fix all the stats and report to IO proxies
	++evictions;
	consolidationReader.readBytes(PostingSize::bytesOfPostings(consolidateIO.rdata),consolidateIO.rseeks);
	consolidationWriteBackBuffer.registerPostingReads(consolidateIO.wdata,consolidateIO.wseeks);

	writeBackBuffer.push_back(updateIO.wdata); //we write all the evicted things at once and pay one seek only
	return totalEvicted;
}
//----------------------------------------------------------------------------
inline did_t operator+(did_t l, const ubList& rhs) { return l+rhs.length; }
inline did_t operator+(did_t l, const std::pair<tid_t, ubList>& rhs) { return l+rhs.second.length; }

BufferStats::BufferStats(const UpdatesBuffer& b) {
	totalPostings = b.totalPostings;
	totalSeenPostings = b.totalSeenPostings;
	termsCount = b.tidsToLists.countTerms();
	if(termsCount)
		avgLen = float(std::accumulate(b.tidsToLists.begin(),b.tidsToLists.end(),0ull))/float(termsCount);
	else
		avgLen = 0;
	evictions = b.evictions;
}

std::ostream& operator <<(std::ostream& stream, const BufferStats& bs) {
	stream << STATMRK << " #live-terms: " << bs.termsCount << " #postings-in-buffer: " << bs.totalPostings
			<< " avg.: " << bs.avgLen << " #evictions: " << bs.evictions << " #totalSeenPostings: " << bs.totalSeenPostings << std::endl;
	return stream;
}

//----------------------------------------------------------------------------
bool UpdatesBuffer::isAllCasteCached(const caste& /*cst*/) const {
	FATAL("deprecated")
//	for(auto candidate : cst.members)
//		if(qProc->isMiss(candidate))
//			return false;
	return true;
}

