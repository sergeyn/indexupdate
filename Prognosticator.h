/*
 * AssumptionTest.h
 *
 *  Created on: Sep 13, 2015
 *      Author: sergeyn
 */

#ifndef ASSUMPTIONTEST_H_
#define ASSUMPTIONTEST_H_

/*
 * We start with no consolidations:
 *
 *   s0 -1 s1 -1 s2 -1 ...
 *
 *   where si is the size of the i-th evicted segment
 *
 *   Consolidation canas only occur at index 2*k+1 where 0<=k and iff sz[2k+1] == -1
 *
 *   The cheapest and most profitable consolidations are of <small seg> + <small in mem seg> as early as possible
 *
 *   It is going to be coded as: 2*k+1 -- consolidation place, left count, right count, gain
 */

//#include "Numeric.h"
#include "Types.h"
#include "Globals.h"

bool testAssumption(const uint64_t queryPerWindow = 1, const uint64_t dataMult = 1000000,const offset_t totalEvictions = 512, bool isPrint=false);
bool testAssumption(const std::vector<size_t>& evictions, double updatesToQRatio, bool isPrint=false);

offset_t findCons(const std::vector<ConsolidationPacket>& consSchedule, offset_t szAfterEviction);
offset_t makeOffsetFromCons(const std::vector<size_t>& segs, ConsolidationPacket cp);
//std::vector<consolidation> findOptimalConsolidation(const std::vector<size_t>& evictedSizes, double updatesToQueriesRatio);

constexpr int64_t _border(-1);

inline std::vector<int64_t> make_hangers(const std::vector<size_t>& segments) {
	//last border is a terminator for sumUntil
	std::vector<int64_t> ss(segments.size()*2);
	for(offset_t i=0; i<segments.size(); ++i){
		ss[2*i] = int64_t(segments[i]);
		ss[2*i+1] = _border;
	}
	return ss;
}
//-------------------------------------------------------------------
//from is a border!
inline size_t dirSum(const std::vector<int64_t>& ss, int direction, offset_t from, offset_t& stop) {
	int64_t acc = 0;
	int i = from + direction;
	while(i >= 0 && ss[i]!=_border ) {
		acc += ss[i];
		i += direction;
	}
	stop = i < 0 ? 1 : i;
	return acc;
}
//-------------------------------------------------------------------
inline size_t oneWinQueries(double updatesToQueriesRatio, size_t updates) {
	return updatesToQueriesRatio < 0.00001 ? 0.0 : double(updates)/updatesToQueriesRatio;
}
//-------------------------------------------------------------------
//where  -- the next segment after the end of rightmost
inline size_t windowsQueries(const std::vector<int64_t>& queryPerWindow, offset_t where) {
	if(where<2) //the first two before-eviction queries are for free in our strange model ;)
		where = 2;
	return std::accumulate(queryPerWindow.begin()+where,queryPerWindow.end(),0ULL);
}

class BaseScheduler {
protected:
	std::vector<ConsolidationPacket> resVector;
	std::vector<int64_t> evictedSizes;
	const double updateToQueryRatio;
	std::vector<int64_t> queryPerWindow; //!! this the number of queries -before- eviction!!
	offset_t totalEvictions;
	unsigned castesCount;
public:
	double totalCostForSeeks;
	BaseScheduler(const std::vector<size_t>& evictedSzs, double ratio) :
		updateToQueryRatio(ratio),
		totalEvictions(evictedSzs.size()),
		castesCount(1),
		totalCostForSeeks(0)
	{
		CHEAP_ASSERT(totalEvictions>1,"too few evictions")
		queryPerWindow.resize(totalEvictions,0);
		for(auto i : make_irange(totalEvictions))
			queryPerWindow[i] = oneWinQueries(updateToQueryRatio,evictedSzs[i]);
		evictedSizes = make_hangers(evictedSzs);
	}
	virtual ~BaseScheduler(){}

	double getTotalCostIfNoConsolidation() const;
	const std::vector<ConsolidationPacket>& consolidations() const { return resVector; }
	void setCasteCount(unsigned castes) { castesCount = castes; }
	virtual ConsolidationPacket getBestGain();
	virtual const std::vector<ConsolidationPacket>&  operator()();

	virtual size_t windowsQueriesFrom(offset_t where) const { return windowsQueries(queryPerWindow,where); }
	virtual double consolidationCostFor(ConsolidationPacket& packet) const;
};

class ChronoScheduler : public BaseScheduler {
public:
	ChronoScheduler(const std::vector<size_t>& evictedSzs, double ratio) :
		BaseScheduler(evictedSzs,ratio){}

	virtual size_t windowsQueriesFrom(offset_t where) const { return where*0; }
	virtual double consolidationCostFor(ConsolidationPacket& /*packet*/) const { return 0;}

};

class RndScheduler : public BaseScheduler {
public:
	RndScheduler(const std::vector<size_t>& evictedSzs, double ratio) :
		BaseScheduler(evictedSzs,ratio){}

	virtual double consolidationCostFor(ConsolidationPacket& packet) const;

};


#endif /* ASSUMPTIONTEST_H_ */
