#include "Asserts.h"
#include "ReMergePolicy.h"
#include "Numeric.h"
#include "DeleteBuffer.h"
#include "Types.h"

std::pair<unsigned,unsigned> ReMergePolicy::compareMargin(std::make_pair(15,16)); //will be (re)set in main

ReMergePolicy::ReMergePolicy(WriteBackBuffer& wb, DiskReader& rdr, listAttributesUpdater& lex):
			writeB(wb), reader(rdr), totalMerges(0),lexUpd(lex) {}

bool ReMergePolicy::isComparableSize(size_t stackBottom,size_t stackTop) {
	return ((stackBottom * compareMargin.first) / compareMargin.second) <= stackTop;
}
//2,1

// https://oeis.org/A007814
// the reads per eviction are the above seq:
// 0,1,0,2,0,1,0,3,0,1,0,2  formula for a(1)=1, a(0)=0, a(2n):= 1+a(n)
// the amount of read blocks is given by n XOR (n-1) or by  2^(A007814(n)+1)-1
// write blocks are readBlocks+1


ConsolidationStats ReMergePolicy::mergeMulti(unsigned startFrom,std::vector<size_t>& sizeStack,DiskReader& reader, WriteBackBuffer& writeB) {
	auto paidForIO = auxConsolidateTail(sizeStack,startFrom,0,1);
	reader.readBytes(PostingSize::bytesOfPostings(paidForIO.rdata),paidForIO.rseeks);
	writeB.registerPostingReads(paidForIO.wdata,paidForIO.wseeks);
	return paidForIO;
}

ConsolidationStats ReMergePolicy::mergeMulti(unsigned startFrom) {
	++totalMerges;
	return ReMergePolicy::mergeMulti(startFrom,sizeStack,reader,writeB);
}

void ReMergePolicy::rndReimburse(const std::vector<size_t>& beforeStackCopy, unsigned stopAt, HistDeletes& deletes) {
	auto reimbursePostingsWritten = deletes.deleteRndMonolithicTidsByStack(beforeStackCopy,stopAt);
	writeB.rewindSeeksAndPostingWrites(0,reimbursePostingsWritten); //compensate the writer -- he didn't really write those
	sizeStack.back() -= reimbursePostingsWritten; //fix the latest merged
	//if(reimbursePostingsWritten)
		COUTL << "Reimbursed in RND for writing " << reimbursePostingsWritten << " postings\n";

}

//perform remerging according to set policy, but assuming the new data is in
size_t ReMergePolicy::currentBatch(HistDeletes& deletes, size_t totalPostings, unsigned environment) {
	sizeStack.push_back(totalPostings);

	unsigned stopAt = whereTelescopic(sizeStack); //an optimization of a telescope case -- do a multi-way merge
	if( environment == Env::CHRONO && stopAt == 0 ) //merging all to one in Chrono!
		deletes.partialDeleteBeforeMerge(sizeStack[0]);

	std::vector<size_t> beforeStackCopy(sizeStack);
	auto paidForIO = mergeMulti(stopAt); //changes the size of stack
	CHEAP_ASSERT(stopAt+1 == sizeStack.size(),"merge is off in the weeds")

	if( environment == Env::RNDDEL && paidForIO.rdata)  //we had a merge in the rnd case:
		rndReimburse(beforeStackCopy,stopAt,deletes);

	return stopAt;
}

