#ifndef UPDATESBUFFER_H_
#define UPDATESBUFFER_H_

#include "Globals.h"
#include "Interfaces.h"
#include "Registry.h"
#include "ProxyIO.h"
#include "CasteSystem.h"
#include "Types.h"
#include "ReMergePolicy.h"
#include "Asserts.h"
#include "URange.h"
#include "DistributionModels.h"

//#define URF

struct ubList {
	tid_t tid;
	did_t length;

	explicit ubList(tid_t t=0, did_t l=0) : tid(t),length(l){}
	template<typename T> explicit ubList(const std::pair<const T, ubList>& pr) { tid = pr.second.tid; length = pr.second.length; }

	inline operator listAttributes() const { return listAttributes(tid,length,0); }
};

/*
class ubLexiconAndPostings : public listAttributesProvider{
	std::unordered_map<tid_t,ubList> data; //holds list data (basically: lexicon + postings)

public:
	inline size_t length(tid_t id) const { return data.at(id).length; }
	inline const void* getRaw(tid_t id) const {return &data.at(id); }

	inline ubList& operator[](tid_t id) { return data[id]; }
	inline void reserve(size_t sz) {data.reserve(sz); }
	inline size_t size() const { return data.size(); }
	inline void clear() { data.clear(); }
	inline void erase(tid_t id) { data.erase(id); }
	inline std::unordered_map<tid_t,ubList>::const_iterator begin() const { return data.cbegin(); }
	inline std::unordered_map<tid_t,ubList>::const_iterator end() const { return data.cend(); }
	tid_t getMaxTid() const; //not O(1)!

	unchar  flagBitsSet(tid_t ) const {FATAL("bubu"); return 0;}
};
*/
class QPBase; //forward

class ubLexiconAndPostings : public listAttributesProvider{
	std::vector<did_t> data; //holds list data (basically: lexicon + postings)

public:
	inline size_t length(tid_t id) const { FATAL("bubu"); return data[id]; }
	inline const void* getRaw(tid_t) const {return nullptr;}

	inline void add(tid_t tid, size_t add) { resize(tid+1); data[tid]+=add;} //it is not a quick one to get rid of the resize here
	inline const did_t& operator[](tid_t id) const{ SAFE_ASSERT(id<data.size(),"index out of range"); return data[id]; }
	//inline void reserve(size_t sz) {data.reserve(sz); }
	inline void resize(size_t sz) { if(data.size()<sz) data.resize(sz+1024,0); }
	inline size_t size() const { return data.size(); }
	void clear();
	inline void erase(tid_t id) {
		SAFE_ASSERT(id<data.size(),"index out of range");
		data[id]=0;
	}

	size_t countTerms() const; //not O(1)

	inline std::vector<did_t>::const_iterator begin() const { return data.cbegin(); }
	inline std::vector<did_t>::const_iterator end() const { return data.cend(); }
	tid_t getMaxTid() const; //not O(1)!
	size_t getLexReservedSize() const { return data.size(); }

	unchar  flagBitsSet(tid_t ) const {SCREAM("bubu"); return 0;}
};

class LexiconForGolden;
constexpr size_t BUFF_LIMIT(1UL<<33);

class UpdatesBuffer {
	friend class BufferStats;

	//scalars:
	size_t totalPostings; //postings count currently
	size_t totalSeenPostings; //all the postings that went through the system
	size_t evictions;

	const size_t maxAllowedPostings;

	//containers:
	ubLexiconAndPostings tidsToLists;

	//SERVICE PROVIDERS:
	CasteManager& castes;
	DistrFromFile& distribution;
	//EvictorABC* evictor;
	WriteBackBuffer& writeBackBuffer;
	WriteBackBuffer& consolidationWriteBackBuffer;
	DiskReader& consolidationReader;

	listAttributesUpdater& globalLexUpd;
	listAttributesProvider& globalLexProvider;
	ReMergePolicy remerge;

	URangeManager urfManager;

	QPBase* qProc;

	unsigned environment;
	size_t totalExperimentPostings;

	void reset();
	bool isAllCasteCached(const caste& cst) const;
	ConsolidationStats evictOneCase(caste& candidateCaste,HistDeletes& deletes);

public:

	class CasteEvictor : public EvictorABC, public RegistryEntry {
		std::vector<caste*> sortedCopy; //caste pointers sorted -- not used in simulator
		//std::vector<size_t> queries; //q. count for castes -- not used in simulator

		std::vector<size_t> sizes;
		std::vector<size_t> shares;
		size_t maxUbPostings;
		unsigned round;
	public:
		CasteEvictor(size_t maxUbPosts = 0, const UpdatesBuffer* ub = 0);
		unsigned getRound() const { return round; }

		//next 2 only used in UpdateBuffer
		tid_t next(); //note: returns termId, not a casteid, but the term is surely a member in one caste
		//void reset(unsigned round);

		//next 2 only touch sizes+shares
 		void coreReset(unsigned round);
		size_t nextId(); //return the index in sortedCopy

		//only used in simulator
		void injectSizesSim(const std::vector<size_t>& rsizes);

		REGISTRY_MACRO("CasteEvictor")
	};

	UpdatesBuffer(std::string evctr,
			DistributionInterface& preloadedD, CasteManager& cstM,
			WriteBackBuffer& wbb, WriteBackBuffer& cwbb, DiskReader& crdr,
			listAttributesUpdater& lex, LexiconForGolden& lexP,
			size_t threshold, tid_t reserve=0, QPBase* qp=nullptr);
	void jumpstart(bool shouldReset, bool isCastes); //perform updates of the system (cheaper perhaps) without caring for eviction until maxPosting is reached
	inline void update() { updateWithTerm(tid_t(distribution.DistrFromFile::next())); } //draw from given distribution and update
	bool isFull(bool isCastes) const;
	unchar percentFull() const { return unchar((100.0*totalPostings)/maxAllowedPostings); }
	size_t getMaxAllowedPostings() const { return maxAllowedPostings; }
	size_t innerLexSize() const { return tidsToLists.getLexReservedSize(); }
	size_t postingsInBuffer() const { return totalPostings; }
	size_t seenPostings() const { return totalSeenPostings; }
	void setTotalSeenPostings(size_t pst) { totalSeenPostings = pst; }
	size_t getEvictions() const { return evictions; }
	void setEnv(unsigned e) { environment = e; }
	unsigned getEnv() const { return environment; }
	void setTotalExpPostings(size_t p) { totalExperimentPostings = p; }

	//alg. specific code:
	inline void updateWithTerm(tid_t term) { //update with term given by caller
		#ifdef URF
		urfManager.accumulatePosting(term);
		#else
		tidsToLists.add(term,1);
		//ubList& lst = tidsToLists[term]; //get the list from local lexicon (adding if new)
		//lst.tid = term; //in case it was a new one set its tid
		//++lst.length; //in a real system -- append posting. Probably will have special treatment for singletons
		castes.increaseWithCheck(term); //update the casteManager -- //TODO: don't pay for this in gold

		#endif
		++totalPostings; //bookkeeping
	}
	size_t evictWithRemerge(HistDeletes& deletes);
	size_t evictWithURange();
	size_t evictWithCastes(did_t freeUpto,HistDeletes& deletes); //will cause eviction of castes to write buffer
	ConsolidationStats handleCasteSegmentOverflow(caste& cst);
	ConsolidationStats attemptAtEvictionCasteConsolidation(caste& cst, HistDeletes& deletes);

	std::vector<size_t>& unsafeGetReMergeStack() { return remerge.unsafeGetStack(); }
	unsigned tryShiftStack(HistDeletes& deletes, std::vector<size_t>& sizeStack);
	void tryShiftStackForCastes(HistDeletes& deletes);
	void shiftMembers(unsigned by, const std::vector<tid_t>& members = {});

private:

	template<typename IdsContainer>
	bool genericUpdateGlobal(LexiconForGolden& globalLexUpdate, const IdsContainer& tids, unsigned oldSz, unsigned currentSize);

	template<typename IdsContainer>
	void tweakSingletonsForGlobal(LexiconForGolden& globalLexUpdate, const IdsContainer& tids);

	void updateGlobalLexInMonolithic();
	void touchGlobalWithMax();
};

class BufferStats {
public:
	did_t totalPostings;
	size_t totalSeenPostings;
	tid_t termsCount;
	size_t evictions;
	float avgLen;
	BufferStats(const UpdatesBuffer& b);

	//should be only 1!	//static tid_t countInvalidTerms(const UpdatesBuffer& b) { return std::count_if(b.tidsToLists.begin(),b.tidsToLists.end(),[](const ubList& p){return p.tid==0;});  }
};

std::ostream& operator <<(std::ostream& stream, const BufferStats& bs);

#endif /* UPDATESBUFFER_H_ */

