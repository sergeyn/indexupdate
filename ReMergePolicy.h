#ifndef REMERGEPOLICY_H_
#define REMERGEPOLICY_H_

#include "Interfaces.h"
#include "ProxyIO.h"
//#include <vector>

class HistDeletes;
class ReMergePolicy {
	std::vector<size_t> sizeStack;
	WriteBackBuffer& writeB;
	DiskReader& reader;
	unsigned totalMerges;
	listAttributesUpdater& lexUpd;

	static std::pair<unsigned,unsigned> compareMargin;
	unsigned whereTelescopic() const;
	ConsolidationStats mergeMulti(unsigned startFrom);
	void rndReimburse(const std::vector<size_t>&, unsigned stopAt, HistDeletes& deletes);

public:
	//TODO:: replace with a single caste...
	ReMergePolicy(WriteBackBuffer& wb, DiskReader& rdr, listAttributesUpdater& lex);
	//return the segment where postings of this batch will end up (after all merges)
	size_t currentBatch(HistDeletes& deletes, size_t totalPostings, unsigned environment);
	size_t getStackSize() const { return sizeStack.size(); }
	std::vector<size_t>& unsafeGetStack() { return sizeStack; }

	static void setCmpMargin(std::pair<unsigned,unsigned> mrg) { compareMargin=mrg; }
	static std::pair<unsigned,unsigned> getCmpMargin() { return compareMargin; }
	template<typename T>
	static unsigned whereTelescopic(const std::vector<T>& sizeStack);
	//static consData mergeStack(unsigned startFrom,std::vector<size_t>& sizeStack);
	static ConsolidationStats mergeMulti(unsigned startFrom,std::vector<size_t>& sizeStack,DiskReader& reader, WriteBackBuffer& writeB);
	static bool isComparableSize(size_t stackBottom, size_t stackTop);
};

//can't get empty stack, but can get a 1-sized
//if one segment -- returns 0
//if last two are not comparable, returns size-1
//return of size-1 usually means -- write down the last one!
//if every suffix is comparable to the one before suffix, will return 0
template<typename T>
offset_t ReMergePolicy::whereTelescopic(const std::vector<T>& sizeStack) {
	TASSERT(sizeStack.size(),"segment size stack is never empty")
	auto i= unsigned(sizeStack.size()-1);
	auto accumulate = sizeStack[i];
	for(; i>0; --i)
		if(! isComparableSize(sizeStack[i-1],accumulate))
			return i;
		else
			accumulate +=sizeStack[i-1];
	return 0;
}



#endif /* REMERGEPOLICY_H_ */
