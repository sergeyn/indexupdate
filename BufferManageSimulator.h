/*
 * BufferManageSimulator.h
 *
 *  Created on: Oct 18, 2015
 *      Author: sergeyn
 */

#ifndef BUFFERMANAGESIMULATOR_H_
#define BUFFERMANAGESIMULATOR_H_

namespace Upd {

class BufferManageSimulator {
	size_t ubsizePostings;
	std::vector<size_t> sizes;
	std::vector<std::vector<size_t> > evictions;

	void clear(size_t castesCount);
	size_t evictionRound(size_t &totalInUb, size_t ubLimit);
public:
	static constexpr double reduceTo=(10000.0);
	BufferManageSimulator(float ubPart = 0.50f,size_t totalmem = 268435456);
	explicit BufferManageSimulator(size_t totalUbSizeP) : ubsizePostings(totalUbSizeP){}
	static void normalizeSizes(std::vector<size_t>& epochSizes);

	void run(size_t totalPostings, const size_t castesCount, const size_t minWbSz, size_t freeUpTo=10);
	void run2(size_t totalPostings, const size_t castesCount, const size_t minWbSz, size_t freeUpTo=10);
	void print() const;
	void predictForPrognosticator(std::vector<size_t>& epochSizes, const std::vector<size_t> ubPostings, size_t totalPostings, size_t freeUpTo=10);

	const std::vector<std::vector<size_t> >& getEvictions() const { return evictions; }
};

} /* namespace Upd */

#endif /* BUFFERMANAGESIMULATOR_H_ */
