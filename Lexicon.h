#ifndef LEXICON_H_
#define LEXICON_H_

#include "Globals.h"
#include "Types.h"
#include "Interfaces.h"
#include "Asserts.h"

//#include <array>

struct lexPtrGolden {
	flagsData flags;
	lexPtrGolden() : flags(flagsData()) { SAFE_ASSERT(getSetBitsInFlag() == 0,"wrongly initialized flag"); b_zeroFlags(flags); }

	static unsigned sizeOf() { return sizeof(flags); }
	inline operator size_t() const { FATAL("?"); return 0; /**(reinterpret_cast<const size_t*>(this)); */}
	inline unchar getSetBitsInFlag() const { return unchar(b_getSetBitInFlags(flags)); }
	inline void zeroFlags() {b_zeroFlags(flags); }
	inline void andMaskFlags(flagsData mask) { b_andMaskFlags(flags,mask);}
	inline void orMaskFlags(flagsData mask) { b_orMaskFlags(flags,mask);}
	inline void shift(unsigned howMuch) { b_shift(flags,howMuch); }
	inline void shiftL(unsigned howMuch) { b_shiftL(flags,howMuch); }
};

struct listMetaDataForGlobalLex  {
	did_t length;
	lexPtrGolden offsetNFlags;
	explicit listMetaDataForGlobalLex(did_t len = 0) : length(len){}
};

class LexiconForGolden : public listAttributesProvider, public listAttributesUpdater {
public:
	void unitTest(unsigned oldStackSz, unsigned newStackSz);
	typedef listMetaDataForGlobalLex nodeType;
	friend class LexStats;

	LexiconForGolden(tid_t reserve=256*1024);
	virtual ~LexiconForGolden(){}

	void clear();
	void resize(tid_t newSz);
	inline void add(tid_t tid, did_t length, caste_t) { resize(tid+1); addLength(tid,length); /*data[tid].casteId = caste;*/ }
	inline void add(tid_t tid) { add(tid,1,0); }

	inline nodeType operator[](tid_t tid) const { SAFE_ASSERT(tid<data.size(),"index out of range"); return data[tid]; }
	inline nodeType& operator[](tid_t tid) { SAFE_ASSERT(tid<data.size(),"index out of range"); return data[tid]; }
	inline bool known(tid_t tid) const { return tid<data.size() && data[tid].offsetNFlags.getSetBitsInFlag();  }

	inline size_t freq(tid_t) const { return 0; }
	inline void addLength(tid_t id, did_t add) { data[id].length+=add; }
	inline size_t length(tid_t id) const {return data[id].length;}
	inline size_t offset(tid_t id) const {return data[id].offsetNFlags;}
	inline flagsData flags(tid_t id) const {return data[id].offsetNFlags.flags;}
	inline unchar flagBitsSet(tid_t id) const {return data[id].offsetNFlags.getSetBitsInFlag();}
	inline void shiftFlag(tid_t id,unsigned howMuch) {data[id].offsetNFlags.shift(howMuch);}
	inline const void* getRaw(tid_t id) const {return &data[id]; }

	void addLength(const std::vector<std::pair<tid_t,did_t> >&); //batch addLength

	inline void setCondBit(tid_t id, const flagsData& maskAwayLow, const flagsData&  removeHigh, const flagsData&  orMask) { b_setCondBit(data[id].offsetNFlags.flags,maskAwayLow,removeHigh,orMask); }
	void shiftAllFlags(unsigned shift,const std::vector<tid_t>& members);
	void deleteOldInBatch(const std::vector<unsigned>& hist, unsigned shift);
	void deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<tid_t>& members, const unsigned shift);
	void deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<tid_t>& members, const std::vector<unsigned>& shift);
	void deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<unsigned>& shift);
	void updateSegmentInfo(const unsigned /*preMergeStackSize*/, const unsigned /*afterMergeStackSize*/);
	inline void orFlag(tid_t id, const flagsData& newFlag) {
		if(id+1>data.size())
			resize(id+1);
		data[id].offsetNFlags.orMaskFlags(newFlag);
	}
	inline void setFlag(tid_t id, flagsData newFlag) {
		if(id+1>data.size())
			resize(id+1);
		data[id].offsetNFlags.flags = newFlag;
	}

	size_t termsCount() const { return data.size();}

	std::vector<nodeType>::const_iterator begin() const { return data.cbegin(); }
	std::vector<nodeType>::const_iterator end() const { return data.cend(); }
private:
	std::vector<nodeType> data; //dense tid to data mapping
	size_t oldSizeMb;
};

class LexStats {
public:
	tid_t spaceBytes;
	tid_t validTerms;
	size_t sumOfLens;

	const LexiconForGolden& lex;
	LexStats(const LexiconForGolden& l);
};

std::ostream& operator <<(std::ostream& stream, const LexStats& ls);
#endif /* LEXICON_H_ */
