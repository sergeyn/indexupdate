def done(arr)
	arr.sort! { |x,y| y.split()[0].to_i <=> x.split()[0].to_i }
	puts arr.join("\n")
	puts
	arr.clear
end

i = 0
prev = ''
blockArr = [] 
File.new(ARGV[0]).each { |l|
	i+=1
	if(i%2==1) 
		nextN = l.split('_')[0..-2].join('')
		done(blockArr) if(prev != nextN)
		prev = nextN
		blockArr << nextN
	else
		blockArr << l.chomp
	end
}
done(blockArr)




