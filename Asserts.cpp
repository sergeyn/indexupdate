/*
 * Asserts.cpp
 *
 *  Created on: Oct 22, 2014
 *      Author: sergeyn
 */

#include "Asserts.h"

namespace Asserts {
void defaultCheap(assertData d)
{
	std::cout 			//Colors::red
			<< d.file << ":" << d.line << " ";
	std::cout.flush();
	if(d.cb)
		d.cb();
	else
		std::cout << d.message;
	std::cout << std::endl; //Colors::colorEnd
	abort();
}

handlerF_t handlers::handleCheap(&defaultCheap);
handlerF_t handlers::handleNormal(&defaultCheap);
handlerF_t handlers::handleSafe(&defaultCheap);
}

