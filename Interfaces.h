#ifndef INTERFACES_H_
#define INTERFACES_H_

#include "Globals.h"
#include "Types.h"
#include "FlagsData.h"

#include <unordered_map>

//class UpdateActionDecider {
//public:
//	virtual enm::updateAction get(tid_t) = 0;
//	virtual ~UpdateActionDecider(){}
//};

//class TraverseActionDecider {
//public:
//	virtual enm::traverseAction get(tid_t) = 0;
//	virtual ~TraverseActionDecider(){}
//};

class ScoreCalculator {
public:
	virtual void calc(did_t did, scr_t raw) const = 0;
	virtual ~ScoreCalculator(){}
};

//class DiskPtr { //some implementations of this abc can actually hop through multiple locations...
//public:
//	virtual ReadStats readAll() = 0; //will read the data into predefined storage and parse it correctly
//	virtual ReadStats read(size_t sz) = 0; //will read the data into predefined storage and parse it correctly
//	virtual ReadStats write(size_t sz, const unchar* data, tid_t tid) = 0; //some implementations could ignore the tid
//	virtual bool hasNextBlock() const = 0;
//	virtual void copyNextBlockDid(std::vector<did_t> dest) const = 0;
//	virtual ~DiskPtr(){}
//};
//-------------------------------------------------------------------
//class PtrFactory {
//public:
//	virtual ~PtrFactory(){}
//	virtual DiskPtr* get(tid_t, enm::updateAction) const = 0;
//};
//-------------------------------------------------------------------
class DistributionInterface {
public:
	virtual ~DistributionInterface(){static_assert( sizeof(tid_t)== sizeof(did_t), "|tid_t| != |did_t|" );}
	virtual size_t next() = 0;
	virtual DistributionInterface* clone() const { return nullptr; }
	virtual void   setMax(size_t mx) = 0;
	virtual void   reseed(size_t /*newseed*/){}
	virtual void   reset(size_t /*newseed*/){FATAL("");}
};
//-------------------------------------------------------------------
class listAttributesProvider {
public:
	virtual ~listAttributesProvider(){}
	virtual size_t    freq(tid_t ) const {return 0;}
	virtual size_t    offset(tid_t ) const {return 0;}
	virtual size_t    length(tid_t ) const = 0;
	virtual flagsData flags(tid_t ) const {return flagsData();}
	virtual unchar    flagBitsSet(tid_t) const { return 0;}
	virtual size_t 	  termsCount() const { return 0;}
	virtual const void* getRaw(tid_t ) const {return nullptr;}
};
//-------------------------------------------------------------------
class listAttributesUpdater {
public:
	virtual ~listAttributesUpdater(){}
	//note the convention is now: size of removed vector is the old-stack-size, while added is the aget-merge-stack size!
	virtual void updateSegmentInfo(const unsigned /*preMergeStackSize*/, const unsigned /*afterMergeStackSize*/){FATAL("?");}
	//virtual void orFlag(tid_t id, const flagsData& newFlag)=0;
	virtual void setFlag(tid_t , flagsData ){FATAL("?");}
	//virtual void setCondBit(tid_t, flagsData, flagsData, const flagsData[2]){FATAL("?");}
	virtual void addLength(tid_t id, did_t add)=0;
	virtual void addLength(const std::vector<std::pair<tid_t,did_t> >&){FATAL("?");} //batch addLength
};
//-------------------------------------------------------------------
class UpdatesBuffer;
class EvictorABC {
protected:
	//const UpdatesBuffer* buffer;
public:
	virtual ~EvictorABC(){}
	virtual tid_t next() = 0;
	//virtual void reset(unsigned round) = 0; //will be deprecated

	//next 2 only touch sizes+shares
	virtual void coreReset(size_t /*maxUbPostings*/, unsigned /*round*/) {}
	virtual unsigned getRound() const { return 0; }
	virtual size_t nextId() {return 0;} //return the index in sortedCopy
	virtual void injectSizesSim(const std::vector<size_t>& /*rsizes*/){}
};
//-------------------------------------------------------------------
template<typename Node, typename Cmp = std::less<Node> >
class PrioQueueABC {
public:
	PrioQueueABC(){}
	virtual ~PrioQueueABC(){}
	virtual const Node& top() const = 0;
	virtual void pop() = 0;
	virtual void increase(const size_t& id, unsigned by=1) = 0;
	virtual void add(size_t id, const Node& nd) = 0;
	virtual const Node& operator[](size_t id) =0;
};
//-------------------------------------------------------------------
class DiskIO {
public:
	virtual ~DiskIO(){}
	virtual uint64_t getTotalBytes() const =0;
	virtual uint64_t getTotalSeeks() const =0;
};
//-------------------------------------------------------------------
class DiskReaderI : public DiskIO{
public:
	virtual ~DiskReaderI(){}
	virtual void readBytes(size_t sizeBytes)=0;
	virtual uint64_t getTotalBytes() const =0;
	virtual uint64_t getTotalSeeks() const =0;
};
//-------------------------------------------------------------------
class DiskWriterI : public DiskIO{
public:
	virtual ~DiskWriterI(){}
	virtual void push_back(size_t postings) =0;
	virtual void rewindSeeksAndPostingWrites(size_t /*seeks*/, size_t /*posts*/){}
};
//-------------------------------------------------------------------
class HistDeletes;
class ProcessTerm {
public:
	static const std::string n__globalLex;//=("globalLex");
	static const std::string n__lexUpd;//=("lexUpd");
	static const std::string n__casteManager;//=("casteManager");

	typedef std::unordered_map<std::string,listAttributesUpdater*> upMap;
	typedef std::unordered_map<std::string,listAttributesProvider*> getMap;

	virtual ~ProcessTerm(){}
	virtual size_t operator()(tid_t)=0;
	virtual void setup(
			std::unordered_map<std::string,listAttributesUpdater*> updaters,
			std::unordered_map<std::string,listAttributesProvider*> providers,
			DiskReaderI& qrdr,
			DiskWriterI& cwrt,
			DiskReaderI& crdr,
			DiskWriterI& uwrt) =0;

	virtual void setDeletesDistr(DistributionInterface* d) = 0;

	virtual size_t totalAskedQs() const=0;
	virtual size_t totalServedFromDiskQs() const=0;
	virtual size_t totalPostingsServed() const = 0;
	virtual size_t totalPostingsMissed() const = 0;
	virtual void printHist() const =0;
	//virtual void setIsNever(bool){}
	virtual void populateChrono(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid = nullptr) = 0; //populate deleted buffer
	virtual void populateRnd(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid = nullptr) = 0; //populate deleted buffer
	//virtual size_t getDeletedCountAndZero() { return 0;} //get postings removed during query-triggered consolidations in castes
	virtual HistDeletes* getInnerDelBuffer() = 0;

	virtual void setUB(void*){}
	virtual void setIsSkiAndRandom(unsigned /*env*/,bool/*isSki*/,bool/*isRandom*/,size_t/*maxP*/){}
	virtual std::pair<size_t,const void*> getMaxAndUB()const {return std::make_pair(0,nullptr); } //quick and dirty...
	virtual const std::string getCname() const = 0;
};
//-------------------------------------------------------------------
// a class can enter the registry only if it has a unique non-empty name and a "getOne function that returns ptr to newly constructed instance
class RegistryEntry {
public:
        virtual const std::string name() const = 0;
        virtual void* getOne() const = 0;
        virtual ~RegistryEntry(){}
};
#endif /* INTERFACES_H_ */
