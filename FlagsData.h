/*
 * FlagsData.h
 *
 *  Created on: Aug 5, 2014
 *      Author: sergeyn
 */

#ifndef FLAGSDATA_H_
#define FLAGSDATA_H_

#include "Types.h"
#include <cassert>
/*


msb         <------ lsb
    ...  4   3  [2]  1  0       //bit offsets
         e   d  [c]  b  a       //bit values
         0   0  [1]  0  0       //or Mask
         1   1  [1]  0  0       //presence discovery mask
         0   0  [0]  1  1       //clear upper bits mask
         0   0 c|d|e b  a       // res
*/
inline unsigned typedBitSetCount(unsigned int x) { return static_cast<unsigned>(__builtin_popcount (x)); }
inline unsigned typedBitSetCount(unsigned long x) { return static_cast<unsigned>(__builtin_popcountl (x)) ; }
inline unsigned typedBitSetCount(unsigned long long x) { return static_cast<unsigned>(__builtin_popcountll (x)) ; }

/*
 * is this correct?
inline unsigned typedHiBitSet(unsigned int x) { return (8*sizeof(x)) - static_cast<unsigned>(__builtin_clz (x)) ; }
inline unsigned typedHiBitSet(unsigned long x) { return (8*sizeof(x)) - static_cast<unsigned>(__builtin_clzl (x)) ; }
inline unsigned typedHiBitSet(unsigned long long x) { return (8*sizeof(x)) - static_cast<unsigned>(__builtin_clzll (x)) ; }
*/

//#define LARGEFLAGS 1024
#ifdef LARGEFLAGS
typedef std::bitset<LARGEFLAGS> flagsData;
inline void b_zeroFlags(flagsData& flags) { flags.reset(); };
inline void b_andMaskFlags(flagsData& flags, const flagsData& mask) { flags &= mask; }
inline void b_orMaskFlags(flagsData& flags, const flagsData& mask) { flags |= mask;  }
inline void b_oneFlags(flagsData& flags) { flags.set(); }
inline unsigned b_getHighestSetBitInFlags(const flagsData& flags) {
	for(unsigned i=flags.size()-1; i>0; --i)
		if(flags[i]) return i;
	return flags[0];
}
inline unsigned b_getSetBitInFlags(const flagsData& flags)  { return flags.count();}
inline void b_setBit(flagsData& flags, unsigned bit) { flags[bit]=1;}
inline void b_unsetBit(flagsData& flags, unsigned bit) { flags[bit]=0; }
inline void b_shift(flagsData& flags,unsigned howMuch) { assert(howMuch); flags>>=howMuch; }
inline void b_shiftL(flagsData& flags,unsigned howMuch) { assert(howMuch); flags<<=howMuch; }

inline void b_setCondBit(flagsData& flags, const flagsData& maskAwayLow, const flagsData& removeHigh, const flagsData orMask) {
	//set the bit only if any of the bits from that offset were on
	flagsData copy = flags;
	b_andMaskFlags(copy,maskAwayLow); //hide the low bits
	const auto index = (b_getSetBitInFlags(copy) != 0); //index is 1 only if there were set high bits
	b_andMaskFlags(flags,removeHigh); //zero the high bits
	if(index)
		b_orMaskFlags(flags,orMask); //conditional or
}
#else
typedef uint64_t flagsData;
inline unsigned b_getSetBitInFlags(const flagsData& flags)  { return typedBitSetCount(flags); }
//inline unsigned b_getHighestSetBitInFlags(const flagsData& flags)  { return typedHiBitSet(flags); }
inline void b_setBit(flagsData& flags, unsigned bit) { setBit(&flags,bit); }
inline void b_unsetBit(flagsData& flags, unsigned bit) { unsetBit(&flags,bit); }

inline void b_zeroFlags(flagsData& flags) { flags=0; }


inline void b_andMaskFlags(flagsData& flags, flagsData mask) { flags &= mask; }
inline flagsData bi_andMaskFlags(flagsData flags, flagsData mask) { return flags & mask; }
inline void b_orMaskFlags(flagsData& flags, flagsData mask) { flags |= mask; }
inline void b_shift(flagsData& flags,unsigned howMuch) { flags = flags>>howMuch; }
inline void b_shiftL(flagsData& flags,unsigned howMuch) { flags = flags<<howMuch; }
inline flagsData bi_shift(const flagsData& flags,unsigned howMuch) { return flags>>howMuch; }
inline flagsData bi_shiftL(const flagsData& flags,unsigned howMuch) { return flags<<howMuch; }

inline void b_setCondBit(flagsData& flags, const flagsData maskAwayLow, const flagsData removeHigh, const flagsData orMask) {
	//set the bit only if any of the bits from that offset were on
	const auto mask = b_getSetBitInFlags(flags & maskAwayLow) != 0;
	b_andMaskFlags(flags,removeHigh); //zero the high bits
	b_orMaskFlags(flags,orMask*mask); //conditional or
}

#endif

inline flagsData getZeroFlag() { return flagsData(0); }
inline flagsData getOneFlag() { return ~(getZeroFlag()); }
inline void b_oneFlag(flagsData& flags) { flags = ~(getZeroFlag()); }

	constexpr size_t MAX_SEGMENTS(8*sizeof(flagsData));

	typedef std::tuple<flagsData,flagsData,flagsData> flgTuple;

	struct MasksForLexFixing {
		static flgTuple s_masks[MAX_SEGMENTS];
		static void initMasks();
		static flgTuple safeMasksForLexFixing(const unsigned /*preMergeStackSize*/, const unsigned /*afterMergeStackSize*/);

		static void testFlagsData(unsigned k, unsigned p);
	private:
		//@bitOffset -- the bit (zero counting) that marks the new segment
		static flgTuple get(unsigned bitOffset);
		static flgTuple getFlagsForConditionalSet(unsigned bitOffset) ;
	};



#endif /* FLAGSDATA_H_ */
