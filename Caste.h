/*
 * Caste.h
 *
 *  Created on: 5 Dec 2015
 *      Author: sergeyn
 */

#ifndef CASTE_H_
#define CASTE_H_

#include "Types.h"
#include "IO.h"
#include "Globals.h"

//=============================================================================================================
struct caste {
	did_t totalPostings; //total postings in the system (excluding the ones in UB!)
	size_t totalQueries; //all the queries that hit the caste (including cached)
	size_t totalSeeks; //total query seeks for this caste (-1ed)

	did_t ubPostings; //how many postings are in the UB

	std::vector<tid_t> members; //list of members, formed by splitter
	caste_t casteId; //the id of this caste

	size_t accSeeks; //seeks we accumulate doing queries. sometimes zeroed

	unsigned evictionRounds; //in how many evicitions participated
	double tokens; //tokens we get by trading seeks and


	//stats for the histogram
	std::vector<size_t> actualEvictions;
	std::vector<size_t> actualQuerySeeks;

	did_t predictedUpdates; //load from training data when creating caste
	did_t predictedQueries; //load from training data when creating caste
	size_t epochUpdates; //store during record-epoch for think
	size_t epochQueries; //store during record-epoch for think

	size_t overflowConsolidations;

	std::vector<size_t> predictedEvictions;
	std::vector<ConsolidationPacket> consolidations;

	//Simulator only:
	uint64_t consolidationData;
	double consolidationPaid;
	uint64_t consolidationSeeks; //temp

	size_t stats[16];
	enum { AT_EVICTION_WRITE_BACK=0 };

	void printHistData()const;

protected:
	std::vector<size_t> segmentStack; //sizes of the segments
public:
	std::vector<double> consolidationCosts; //states the costs (in minutes) of consolidating tails of seg.stack

	//Disk IO related:
	static size_t minWBSize; //we suffer greatly if we allow a caste to be smaller than this
	static size_t maxWBSize; //don't care now

	caste();

	inline size_t totalQs() const { return totalQueries; }
	inline size_t totalSs() const { return totalSeeks+accSeeks; }
	inline size_t size() const { return segmentStack.size(); }

	inline void updateReg(unsigned addPostings = 1) { ubPostings+=addPostings; }
	inline void queryReg(unsigned segmentsCount = 1) { accSeeks += segmentsCount-1;  ++totalQueries;   }

	inline const std::vector<size_t>& getSegmentStack() const { return segmentStack; }
	inline std::vector<size_t>& getSegmentStackUnsafe() { return segmentStack; }

	//	size_t consolidateAuxThink(bool isRandom, ProcessTerm* hackGetMaxPosts=nullptr, float aggressive=1.0);
//	size_t consolidateStableChronoThink(float aggressive) { return consolidateAuxThink(false,nullptr,aggressive); }
//	size_t consolidateStableRandomThink(float aggressive) { return consolidateAuxThink(true,nullptr,aggressive); }
//	size_t consolidateEvegrowThink(float aggressive, ProcessTerm* hackGetMaxPosts) { return consolidateAuxThink(true /*really don't care*/,hackGetMaxPosts,aggressive); }
//	size_t consolidateAnySkiRental(size_t lastInMem/*0 or 1*/, size_t ispb=1 /*float aggressive*/);

	//used for construction of the caste system
	//void addMember(const listAttributes& list);
	void addMember(tid_t list, did_t length, did_t freq);
	void sortMembers();
	static bool predicateSorterPrint(const caste& lhs, const caste& rhs);
	static std::vector<caste> convertSplitterToCastes(const std::vector<listAttWithCasteId>& sorted);

	//used for eviction + consolidation
	void evicted(size_t p);
	inline void resetAfterEviction() { ubPostings=0; } //assuming the members didn't change -- only zero the lengths
	inline void addSegment(size_t newS) { segmentStack.push_back(newS); /*rebuildCostsForInRoundPB();*/}
	void prepareTokens();
	const std::vector<double>& getCCosts() const { return consolidationCosts; }
	void rebuildCostsForEviction() { rebuildCPrice(consolidationCosts,segmentStack,0); }
	void rebuildCostsForInRoundPB() { rebuildCPricePB(consolidationCosts,segmentStack); } //DONOT use this costs vector in eviction stage!
	inline ConsolidationStats anyConsolidateTail(offset_t offset,size_t lastInMem=1 /*0 or 1*/, size_t _isPB=0 /* 0 or 1*/ ) { return auxConsolidateTail(segmentStack,offset,_isPB,lastInMem); }
	inline void clear(did_t terms) { segmentStack.clear(); segmentStack.push_back(terms); totalPostings = terms; rebuildCostsForInRoundPB();}

	void recordEpoch();
	//inline void erase(unsigned begin, unsigned end) { segmentStack.erase(segmentStack.begin()+begin,segmentStack.begin()+end);  }
	//inline void clear() {  rebuildCostsForInRoundPB();}
	offset_t whereAffordConsoidation(size_t totalSeenPostings, size_t endOfLifePostings);

	friend class ConsolidationTest_auxRebuildCPrice_Test;
};

//this is a less-than comparator for max-heap
//if returns true r is on top, if false - l is on top
struct compareCastePtr {
	// * we want castes with enough postings to be on top
	// * in those we prefer to evict castes that are bound to have less queries
	// * we want pushed-promoted-singletones to be on top
	inline bool operator()(caste* l, caste* r) const {
		TASSERT(l && r, "bad ptrs");
		return (l->ubPostings > caste::minWBSize && r->ubPostings > caste::minWBSize) ?
				l->predictedQueries > r->predictedQueries :
				l->ubPostings < r->ubPostings;}
};

std::ostream& operator <<(std::ostream& stream, const caste& cst);
//=============================================================================================================

inline void caste::addMember(tid_t list, did_t length, did_t freq) {
	members.push_back(list);
	predictedUpdates += length;
	predictedQueries += freq;
}



#endif /* CASTE_H_ */
