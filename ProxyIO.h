#ifndef WRITEBACKBUFFER_H_
#define WRITEBACKBUFFER_H_

#include "Globals.h"
#include "Interfaces.h"
#include "IO.h"

class WriteBackBuffer : public DiskWriterI {
	//LexiconForGolden& lex;
	uint64_t bytesWritten;
	uint64_t totalSeeks;
	//std::vector<std::pair<tid_t,did_t> > jobQ;

public:
	WriteBackBuffer(/*LexiconForGolden& l*/);
	WriteBackBuffer(uint64_t bw, uint64_t ts):bytesWritten(bw),totalSeeks(ts){}
	uint64_t getTotalBytes() const { return bytesWritten; }
	uint64_t getTotalSeeks() const { return totalSeeks; }

	void push_back(size_t postings);
	void registerPostingReads(size_t postings, size_t seeks);
	void rewindSeeksAndPostingWrites(size_t seeks, size_t posts);
	//void push_bucket(const std::vector<std::pair<tid_t,did_t> >& batch);
};

class DiskReader : public DiskReaderI {
	uint64_t bytesRead;
	uint64_t totalSeeks;
public:
	DiskReader():bytesRead(0),totalSeeks(0){}
	DiskReader(uint64_t br, uint64_t ts):bytesRead(br),totalSeeks(ts){}
	inline void readBytes(size_t sizeBytes) { ++totalSeeks; bytesRead+=sizeBytes; }
	inline void readBytes(size_t sizeBytes, unsigned seeks) { bytesRead+=sizeBytes; totalSeeks += seeks; }
	inline void rewindOneSeek() { --totalSeeks; }
	uint64_t getTotalBytes() const { return bytesRead; }
	uint64_t getTotalSeeks() const { return totalSeeks; }

};

ConsolidationStats consolidateTwoSegments(size_t a, size_t b, bool secondInMem, size_t reduceWrite = 0, size_t reduceRead = 0, size_t memBuffer = NUMERIC::mergingMemBuffer);
#endif /* WRITEBACKBUFFER_H_ */
