#exec perl -x -S $0 ${1+"$@"} # -*- perl -*-
#!/usr/bin/perl
#
#  Create option information

$heredoc = <<END;
/* #This file was generated automatically! Do not edit */
#ifndef _WEST_OPTIONS_
#define _WEST_OPTIONS_

#include "Globals.h"

struct OptionRanges
{
	#RANGE_DEC#
	OptionRanges() : 
		#RANGE_DEF#
	{}
};

struct Options
{
	
	#DECLARATIONS#

	unsigned countAllOpts;
	OptionRanges	valueRanges;

	Options() : 
		#DEFAULTS#
	{}

	unsigned signatureF(const unsigned& value)	{ return value; }
	unsigned signatureF(const unsigned long& value)	{ return value; }
	unsigned signatureF(const float& value)		{ return unsigned(1000000*value); }
	unsigned signatureF(const double& value)	{ return unsigned(1000000*value); }
	unsigned signatureF(const STRING& value)	{ return value.size(); }

	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, STRING& target)	{ if(argName == name) { target=argVal; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, unsigned& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, unsigned long& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, double& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; } 
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, float& target)		{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }

	bool parseArg(const STRING& argName, const STRING& argVal)
	{
		#PARSE_ARG#
		return false;
	}

	STRING getVal(const STRING& optName)
	{
		#GET_VAL#
		CERR << "Unknown option: " << optName << ENDL; exit(1);
		return "";
	}

	void parseFromCmdLine(int argc, char** argv, const int id=-99999)
	{
		const STRING idSuffix(":"+ toString(id));

		for (int i=0; i<argc; ++i)
		{
			STRING argvi(argv[i]);
			if (argvi[0] != '-')
			{
				CERR << "Unexpected argument: " << argvi << ENDL; goto _PrintUsage; 
			}
			else if (argc <= i+1)
			{
				CERR << "Value missing for argument: " << argvi << ENDL; goto _PrintUsage;
			}
			// for all other args, the val should be numeric
			//else if (!isNumber(argv[i+1]))
			//{
			//	CERR << "Unexpected value " << argv[i+1] << " for argument: " << argv[i] << ENDL; goto _PrintUsage;
			//}
			else if (argvi.find(':') != STRING::npos)  // id specific opt
			{
				size_t s = argvi.find(idSuffix);
				if(s == STRING::npos) { ++i; continue; } // not of this id
				argvi.erase(s,argvi.size()-s);
				if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; }
			}
			else if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; } // regular opt

			CERR << "Unknown option: " << argv[i] << ENDL;
			
			_PrintUsage:
			CERR << "Usage:" << ENDL
			#USAGE#
			<< ENDL;

			exit(1);
		}
	}

	bool testAllOptions() const 
	{
		#SANITY#
		return true;
	}

	template<class B> void print(B& pOut)
	{
		pOut << "----------------------------" << SLASHN;
		#define PRINT_OPT(external_name, internal_name) { pOut << external_name  << " = " << toString(internal_name) << SLASHN; }
		#PRINTOPT#
		pOut << "----------------------------" << ENDL;
	}
	
	template<class B> void printCSV(B& pOut)
	{
		#PRINTCSV#
	}

	unsigned signature()
	{
		unsigned res = 0;
		#SIGNATURE#
		return res;
	}
};

#endif
END

#"' ------------------------------------------------------------------------
use POSIX ();

sub parse_csv {
  my $text = shift; ## record containing comma-separated values
  my @new = ();
  push(@new, $+) while $text =~ m{
    ## the first part groups the phrase inside the quotes
    "([^\"\\]*(?:\\.[^\"\\]*)*)",?
      | ([^,]+),?
      | ,
    }gx;
    push(@new, undef) if substr($text, -1,1) eq ',';
    return @new; ## list of values that were comma-spearated
}
## Use like this: @goodlist = parse_csv($csvlist);
#"' ------------------------------------------------------------------------
open FILE, "Options.cfg" or die $!;
@ranges_dec		= ();
@ranges_def		= ();
@declarations	= ();
@defaults		= ();
@parse			= ();
@getval			= ();
@usage			= ();
@printopt		= ();
@signature		= ();
@sanity			= ();
@optsCSV		= ();
while (<FILE>) {
	next unless( $_ =~ /^ROPT\((.*)\);$/); #skip non opt lines
	#example: ROPT(unsigned,PREPROCESSING,0,"bla bla",0,1);
	@arr = parse_csv($1);
	$name = $arr[1];
	$s_name = lc($name);
	$minv = (@arr >= 5) ? $arr[4] : 0;
	$maxv = (@arr >= 6) ? $arr[5] : "std::numeric_limits<$arr[0]>::max()";
	@csvArr = @arr;
	unless($csvArr[2] =~ /^\d/)
	{
		$csvArr[2] = "\"<<" . $csvArr[2] . "<<\""; 
	}
	if(@csvArr >= 5)
	{
		unless($csvArr[4] =~ /^\d/)
		{
			$csvArr[4] = "\"<<" . $csvArr[4] . "<<\""; 
		}
	}
	if(@csvArr >= 6)
	{
		unless($csvArr[5] =~ /^\d/)
		{
			$csvArr[5] = "\"<<" . $csvArr[5] . "<<\""; 
		}
	}
	push(@optsCSV, "");
	#push(@optsCSV, "pOut << \"" . join(',', @csvArr) . "\\n\";");
	push(@ranges_dec, "$arr[0] ${name}_MINVAL, ${name}_MAXVAL;");
	push(@ranges_def, "${name}_MINVAL($minv), ${name}_MAXVAL($maxv), //$arr[0]");
	push(@declarations, "$arr[0] $name;");
	unless($arr[0] =~ 'STRING') {
		push(@defaults, "$name($arr[2]), //$arr[0]");
	}
	else {
		push(@defaults, "$name(\"$arr[2]\"), //$arr[0]");
	}
	push(@parse,"if (convertArg(argName, argVal, STRING(\"-$s_name\"), $name)) return true;");
	push(@getval,"if (optName == STRING(\"-$s_name\")) return toString($name);");
	push(@usage," << \"\t-$s_name <val=$arr[2]> // \" << \"$arr[3]\" << ENDL");
	push(@printopt,"PRINT_OPT(\"-$s_name\", $name);");
	push(@signature,"res ^= (unsigned(rand()) * signatureF($name));"); 
	push(@sanity,"if ($name < valueRanges.${name}_MINVAL || $name > valueRanges.${name}_MAXVAL) { CERR<<  \" $name is out of range: [$minv,$maxv]\" << ENDL; return false;}");	

}
push(@ranges_dec,"unsigned countAllOpts;");
$szopts = @declarations;
push(@ranges_def,"countAllOpts($szopts)");
push(@defaults, "countAllOpts($szopts)");
$t = join("\n\t",@declarations);
$heredoc =~ s/#DECLARATIONS#/$t/;
$t = join("\n\t\t",@defaults);
$heredoc =~ s/#DEFAULTS#/$t/;
$t = join("\n\t\t",@parse);
$heredoc =~ s/#PARSE_ARG#/$t/;
$t = join("\n\t\t",@getval);
$heredoc =~ s/#GET_VAL#/$t/;
$t = join("\n\t\t\t",@usage);
$heredoc =~ s/#USAGE#/$t/;
$t = join("\n\t\t",@printopt);
$heredoc =~ s/#PRINTOPT#/$t/;
$t = join("\n\t\t",@signature);
$heredoc =~ s/#SIGNATURE#/$t/; 
$t = join("\n\t",@ranges_dec);
$heredoc =~ s/#RANGE_DEC#/$t/; 
$t = join("\n\t\t",@ranges_def);
$heredoc =~ s/#RANGE_DEF#/$t/; 
$t = join("\n\t\t",@sanity);
$heredoc =~ s/#SANITY#/$t/;
$t = join("\n\t\t",@optsCSV);
$heredoc =~ s/#PRINTCSV#/$t/;

print $heredoc
