/*
 * Lexicon.cpp
 *
 *  Created on: Jan 2, 2014
 *      Author: sergeyn
 */

#include "Lexicon.h"
#include "FlagsData.h"

LexiconForGolden::LexiconForGolden(tid_t reserve) {
	data.reserve(reserve);
	oldSizeMb = 0;
}

void LexiconForGolden::clear() {
	for(auto& val : data) {
		val.length = 0;
		val.offsetNFlags.zeroFlags();
	}
}

void LexiconForGolden::resize(tid_t newSz) {
	if(newSz<data.size()) return;
//	unsigned denseDataSize = sizeof(nodeType);
//	size_t newSizeMb = (newSz*denseDataSize) >>20;
//	if(9*newSizeMb>10*oldSizeMb){
//		oldSizeMb = newSizeMb;
//		COUT3 << "resizing lexicon to: " << newSz << " entries (" <<newSizeMb << "mb)" << ENDL;
//	}

	//auto tid = data.size();
	const size_t addMore = (1<<16);
	COUTV(6) << "Resizing lex to: " << newSz+addMore << ENDL;
	data.resize(newSz+addMore);
	//data.reserve(newSz+addMore);	//while(tid<=newSz) { data.push_back(listMetaDataForGlobalLex(tid++,0)); 	} //deprecated for now
}

//static + chrono
//void LexiconForGolden::deleteOldInBatch(const std::vector<unsigned>& hist, unsigned shift) {
//#ifdef DEBUG
//	for(size_t i = 0; i<hist.size(); ++i)
//		if(data[i].length < hist[i])
//			FATAL("decreasing len more than we have");
//#endif
//	if(shift)
//		for(size_t i = 0; i<hist.size(); ++i) {
//			data[i].length -= hist[i];
//			data[i].offsetNFlags.shift(shift);
//		}
//	else
//		for(size_t i = 0; i<hist.size(); ++i)
//			data[i].length -= hist[i];
//}
void LexiconForGolden::shiftAllFlags(unsigned shift,const std::vector<tid_t>& members) {
	if(members.empty())
		for(auto &meta : data)
			meta.offsetNFlags.shift(shift);
	else
		for(auto id : members)
			data[id].offsetNFlags.shift(shift);
}

void LexiconForGolden::addLength(const std::vector<std::pair<tid_t,did_t> >& input) //batch addLength
{
	CHEAP_ASSERT(input.size(),"Empty batch vector!");
	resize(input.back().first);
	for(const std::pair<tid_t,did_t>& tpPair :  input)
		data[tpPair.first].length += tpPair.second;
}

// castes + chrono
void LexiconForGolden::deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<tid_t>& members, const unsigned shift) {
	if(shift)
		for(auto member:members) {
			data[member].length -= hist[member];
			data[member].offsetNFlags.shift(shift);
		}
	else
		for(auto member:members) {
			data[member].length -= hist[member];
		}
}
//for static + random
void LexiconForGolden::deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<unsigned>& shift) {
	if(shift.size())
		FATAL("broken")
	else
		for(size_t i = 0; i<hist.size(); ++i)
					data[i].length -= hist[i];
}

inline flagsData makeMask(unsigned indx) {
	flagsData f;
	b_zeroFlags(f);
	for(unsigned i=0; i<indx; ++i)
		b_setBit(f,i);
	return f;
}

//for castes + random
void LexiconForGolden::deleteOldInBatch(const std::vector<unsigned>& hist, const std::vector<tid_t>& members, const std::vector<unsigned>& shift) {
	if(shift.size()) {
		for(unsigned i=0; i<shift.size(); ++i) {
			//simple case
			if(shift[i]==i) {
				for(auto member:members) {
					data[member].length -= hist[member];
					data[member].offsetNFlags.shift(1);
				}
			}
			else { //x := (x>>(i+1)<<i)|(x&mask)
				//we already shifted i positions
				const unsigned bit = shift[i]-i;
				const flagsData mask = makeMask(bit);

				for(auto member:members) {
					data[member].length -= hist[member];
					flagsData& flag = data[member].offsetNFlags.flags;
					flag = ((flag>>(bit+1))<<bit)|(flag&mask);
				}
			}
		} //for shifts
	}
	else {
		for(auto member:members) {
			data[member].length -= hist[member];
		}
	}
}

void LexiconForGolden::updateSegmentInfo(const unsigned //oldStackSz
		, const unsigned //newStackSize
		) {
	FATAL("deprecated")
}

LexStats::LexStats(const LexiconForGolden& l) : lex(l) {
	spaceBytes = tid_t(lex.data.size() * sizeof(LexiconForGolden::nodeType));
	//validTerms = std::count_if(lex.data.begin(),lex.data.end(),[](const Lexicon::nodeType& n){return n.offsetNFlags.flags!=0;});
	validTerms = tid_t(std::count_if(lex.data.begin(),lex.data.end(),[](const LexiconForGolden::nodeType& n){return n.length!=0;}));
	sumOfLens = 0; //std::accumulate(lex.lengths.begin(),lex.lengths.end(),size_t(0),binOp);
}

std::ostream& operator <<(std::ostream& stream, const LexStats& ls) {
	stream << STATMRK << " Lex-valid-terms: " << ls.validTerms << " totalPosts: " << ls.sumOfLens << " Space(bytes): " << ls.spaceBytes << ENDL;
	return stream;
}



void LexiconForGolden::unitTest(unsigned , unsigned ) {
//	//there are two cases:
//	//[1] adding a single segment at the end of the stack
//	//[2] merging the new guy with old ones
//
//
//	//fill data
//	data.clear();
//	listMetaDataForGlobalLex meta;
//	for(size_t i=0; i<(1<<oldStackSz); ++i) {
//		meta.offsetNFlags.flags = i;
//		data.push_back(meta);
//	}
//	COUTL << oldStackSz << " to " << newStackSz << ENDL;
//	//COUTL << data.size() << " entries " << data.back().offsetNFlags.flags << " is last" << ENDL;
//	size_t newSegmentFlag = 1;
//	newSegmentFlag = newSegmentFlag <<(newStackSz-1);
//	size_t dataSz = data.size();
//
//	if(newStackSz==oldStackSz+1) { //case [1]
//		for(size_t i=0; i<dataSz; ++i)
//			orFlag(i,newSegmentFlag);
//
//		for(size_t i=0; i<dataSz; ++i)
//			if(data[i].offsetNFlags.flags!=i+newSegmentFlag) FATAL("UTF");
//
//	} //end case [1]
//
//	else { 	//case[2]
//		std::vector<unsigned> dummy(oldStackSz,0);
//		updateSegmentInfo(dummy,newStackSz);
//		for(size_t i=0; i<dataSz; ++i) {
//			size_t oldFlag = i;
//			bool hadStuff = false;
//			for(unsigned b=newStackSz-1; b<=oldStackSz-1; ++b) {
//				hadStuff = hadStuff || getBit(&oldFlag,b);
//		//		COUTL<<oldFlag << " -- "  << b << " --> ";
//				unsetBit(&oldFlag,b);
//			//	COUT << oldFlag << ENDL;
//			}
//			if(hadStuff)setBit(&oldFlag,newStackSz-1);
//		//	COUTL << oldFlag << ENDL;
//			if(data[i].offsetNFlags.flags!=oldFlag) {
//				COUTL << i  << ": " << oldFlag << " != " <<data[i].offsetNFlags.flags << ENDL; FATAL("UTF");
//			}
//		}
//	} //end case[2]
	/*
	    Lexicon lx;
	    lx.unitTest(0,1);
	    lx.unitTest(1,2);
	    lx.unitTest(3,4);
	    lx.unitTest(4,5);
	    lx.unitTest(6,7);
	    lx.unitTest(7,8);
	    lx.unitTest(3,1);
	    for(unsigned i=1; i<11; ++i)
	    	lx.unitTest(11,11-i);
	    for(unsigned i=1; i<16; ++i)
	    	lx.unitTest(16,16-i);
	*/
}
