#ifndef ASSERTS_H_
#define ASSERTS_H_


//inspired by the stuff John Lakos presented at cppcon14

//during compilation we enable a level of assert active: none->cheap->all->safe

// CHEAP_ASSERT will always fire. Shouldn't be inside inline functions!
// TASSERT will fire in debug mode
// SAFE_ASSERT is a costly thing you want to enable only in a special mode

// we also support different runtime reaction to an assertion being eval-ed to fault
// Asserts::setHandlerCheap,setHandlerNormal,setHandlerSafe

namespace Asserts {
	struct assertData {
		unsigned line;
		std::string file;
		std::string message;
		std::function<void()> cb;
		assertData(unsigned ln, const std::string& f, const std::string& msg):line(ln),file(f),message(msg) {}
		assertData(unsigned ln, const std::string& f, std::function<void()> _cb) : line(ln),file(f),cb(_cb) {}
	};

	typedef void (*handlerF_t)(assertData d);
	void defaultCheap(assertData d) __attribute__ ((noreturn));

	struct handlers {
		static handlerF_t handleCheap;
		static handlerF_t handleNormal;
		static handlerF_t handleSafe;
	};

#define SCREAM(message) {\
		Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),std::string(message));\
		defaultCheap(assertiondata);\
	}

#define FATAL(message) SCREAM(message)

#define _SCREAM(code) {\
		auto cb = [&](){ code };\
		Asserts::assertData assertiondata(cb);\
		defaultCheap(assertiondata);\
	}

#ifdef ASSERTS_NONE
#define CHEAP_ASSERT(c,m)
#else
#define CHEAP_ASSERT(condition, message)\
	if(__builtin_expect((!(condition)),0)) {\
		Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),std::string(message));\
		(*Asserts::handlers::handleCheap)(assertiondata);\
	}
#define CHEAP__ASSERT(condition, code)\
		if(__builtin_expect((!(condition)),0)) {\
			auto cb = [&](){ code };\
			Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),cb);\
			(*Asserts::handlers::handleCheap)(assertiondata);\
		}
#endif
#ifdef DEBUG
#define TASSERT(condition, message)\
	if(__builtin_expect((!(condition)),0)) {\
		Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),std::string(message));\
		(*Asserts::handlers::handleNormal)(assertiondata);\
	}
#define T__ASSERT(condition, code)\
	if(__builtin_expect((!(condition)),0)) {\
			auto cb = [&](){ code };\
			Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),cb);\
			(*Asserts::handlers::handleNormal)(assertiondata);\
		}
#else
#define TASSERT(condition, message)
#endif
#ifdef SAFE_ASSERTS_ON
#define SAFE_ASSERT(condition, message)\
	if(!(condition)) {\
		Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),std::string(message));\
		(*Asserts::handlers::handleSafe)(assertiondata);\
	}
#define SAFE__ASSERT(condition, code)\
		if(!(condition)) {\
			auto cb = [&](){ code };\
			Asserts::assertData assertiondata(__LINE__,std::string(__FILE__),cb);\
			(*Asserts::handlers::handleSafe)(assertiondata);\
		}
#else
#define SAFE_ASSERT(c,m)
#endif
} //end namespace


#endif /* ASSERTS_H_ */
