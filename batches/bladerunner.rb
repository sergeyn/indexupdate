#!/usr/bin/ruby 

require 'socket' 
require 'thread'
#-----------------------------------------------------------------------
def assert(msg=nil)
        raise msg || "Assertion failed!" unless yield
end
#-----------------------------------------------------------------------
# @outs -- array of output streams
def putsA(outs=[],msg="",shouldFlush = true)
    begin
        outs.each do |s| 
            s.puts msg 
            s.flush if shouldFlush
        end
    rescue
        # live with it. Can't be sure we have a valid logging stream
    end
end
#=======================================================================
class BladeRunner
	#concurrency_cap is max cores we want to utilize -- at most such tasks will run
	#mem_cap -- sum of all task memories can't be higher than this
	def initialize(concurrency_cap, mem_cap, out=[STDOUT])
		@con_cap = concurrency_cap
		@mem_cap = mem_cap
		@cpu_pool = 0
		@mem_pool = 0
		@hsh = {}
		@outs = out
		@mutex = Mutex.new
	end
	#-------------------------------------------------------------------
	def from_file fname
		#Format: <memcap cmd>
		File.new(fname).each do |l|
			next unless l
			ar = l.split(' ')
			next unless ar.size > 1
			begin
				putsA @outs, l
				cmd = (ar[1..-1].join ' ').chomp
				cap = ar[0].to_f
				assert("Bad mem cap '#{ar[0]}'") {cap.between? 1,99 }
				add(cmd,ar[0].to_f)
			rescue => error
				putsA @outs, error.message
			end	
		end
	end
	#-------------------------------------------------------------------
	def dump out
		@hsh.each_value {|t| out.puts "#{t.mem_cap} #{t.cmd}" if t.status = 'W'} 
	end
	#-------------------------------------------------------------------
	def addt(task)
		if @hsh[task.cmd]
			putsA @outs, "warning: duplicate cmd => #{task.cmd}. Are you rerunning?" 
		end 
		@hsh[task.cmd] = task 
	end
	#-------------------------------------------------------------------
	def add(cmd,mem_cap)
		addt(TaskToRun.new(cmd.chomp,mem_cap))
	end
	#-------------------------------------------------------------------
	def all_done?
		@hsh.each_value.all? { |t| t.status == "D" or t.status == "X"}
	end
	#-------------------------------------------------------------------
	def stats
		@hsh.each_value.group_by{ |t| t.status}#.each { |k,v|  puts "#{k} : #{v}"]}
	end
	#-------------------------------------------------------------------
	def print_stats a
		keys = a.size>1 ? a[1] : stats.keys
		putsA @outs, Time.now
		putsA @outs, "#{@con_cap} #{@mem_cap}"
		@outs.each { |s|
			keys.each { |k|  
				s.puts "[[#{k}]]"
				s.puts stats[k].map {|t| t.cmd}.join "\n"
			}
			s.flush
		}
	end
	#-------------------------------------------------------------------
	def exec s
		a = s.split
		return if a.empty?
		@mutex.synchronize do
			if a[0] == "ADD" and a.size>2 and a[1].to_f.between?(1,99)
				add(a[2..-1].join(' '),a[1].to_f)
			elsif a[0] == "LOAD" and a.size==2 and File.exist? a[1]
				from_file a[1]
			elsif a[0]=="STATS"
				print_stats a
			elsif a[0]=="DUMP" and a.size==2
				out = File.new(a[1],'w')
				dump out
			elsif a[0]=='MEM' and a.size==2
				@mem_cap = a[1].to_i
			elsif a[0]=='CPU' and a.size==2
				@con_cap = a[1].to_i
			else
				putsA @outs, "broken command: #{s}"
			end
		end
	end
	#-------------------------------------------------------------------
	def cycle
		@mutex.synchronize do
			assert("overcommit") {@cpu_pool <= @con_cap}
			#find those that ended
			finished = @hsh.each_value.select {|t| t.status == "D"}
			assert("size mismatch") {finished.size<=@cpu_pool}
			unless finished.empty?
				@mem_pool -= finished.inject(0) {|sum,t| sum+t.mem_cap} #return their mem
				assert("mem leaking") {@mem_pool>=0}
				@cpu_pool -= finished.size #and cpu
				finished.each { |t| @hsh[t.cmd].status = "X" }
			end
			
			sent = 0
			return 0 if(@cpu_pool == @con_cap) #no resources
			@hsh.each_value do |t| #now try to run some
				if t.status == "W" and @mem_pool + t.mem_cap <= @mem_cap
					t.alert_run
					Thread.new {t.run}
					@mem_pool += t.mem_cap
					@cpu_pool += 1
					sent += 1
					return sent if @cpu_pool == @con_cap
				end
			end
			return sent
		end
	end
end
#=======================================================================
class TaskToRun
	attr_accessor :cmd
	attr_accessor :mem_cap
	attr_accessor :status
	
	def initialize(cmd,mem_cap=7)
	        @cmd = cmd
		@mem_cap = mem_cap
		@status = "W"
    end
	
	def alert_run
		@status = "R"
	end
	def alert_done
		@status = "D"
	end
	
	def run
		alert_run
		%x[#{@cmd}]
		alert_done
	end
end
#=======================================================================
#clients:  ruby -e "require 'socket'; TCPSocket.new('127.0.0.1', 3333).write(ARGV[0])" STATS
#clients:  ruby -e "require 'socket'; TCPSocket.new('127.0.0.1', 3333).write(ARGV[0])" 'ADD 8 updHD xyz'
#server run: /usr/bin/ruby ../bladerunner.rb 12 82 60 &
cpu_cap,mem_cap,sleep_time  = (ARGV[0] || 1).to_i,
							  (ARGV[1] || 81).to_i,
							  (ARGV[2] || 60).to_i
outArr = [File.new('/tmp/bladerunner.log','a')]
$stderr = outArr[-1]
$stderr.puts 'Errors redirected here'
$stderr.flush
putsA outArr, Time.now
putsA outArr, [cpu_cap,mem_cap,sleep_time]  


runner = BladeRunner.new(cpu_cap,mem_cap,outArr)

#-------------------------------------------------------------------
Thread.new { 
	loop { #Running cycles loop
		runner.cycle
		sleep sleep_time
	}
}
#-------------------------------------------------------------------
server = TCPServer.new('', 3333)  # to listen on port
loop { # cmds listening server
	client = server.accept       # Wait for a client to connect
	cmd = client.gets.chomp
	client.close                 # Disconnect from the client
	exit if cmd == "EXIT"
	putsA outArr, "Exec: " + cmd
	runner.exec cmd
}

