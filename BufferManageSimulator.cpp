/*
 * BufferManageSimulator.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: sergeyn
 */

#include "BufferManageSimulator.h"
#include "Asserts.h"
#include "Globals.h"
#include "UpdatesBuffer.h"

namespace Upd {

BufferManageSimulator::BufferManageSimulator(float ubPart, size_t totalmem) : ubsizePostings(static_cast<size_t>(totalmem*ubPart)) {}

void BufferManageSimulator::clear(size_t castesCount) {
	sizes.clear();
	evictions.clear();
	sizes.resize(castesCount,0);
	evictions.resize(castesCount);
}

//we assume uniform distr. of postings
//and that castes are ordered by more important first
void BufferManageSimulator::run(size_t totalPostings, const size_t castesCount, const size_t minWbSz, size_t freeUpTo) {
	clear(castesCount);
	constexpr size_t batch(32000);
	size_t stopAt = ubsizePostings * (100-freeUpTo) / 100;
	size_t totalInUb = 0;
	//simulation loop
	for(size_t totalWentThrough = 0; totalWentThrough<totalPostings; ) {
		for(auto& cs : sizes) //round robin
			cs += batch;

		totalInUb += batch*castesCount;
		totalWentThrough += batch*castesCount;

		if(totalInUb > ubsizePostings) { //start eviction
			for(auto it=int(castesCount)-1; it>=0 && totalInUb > stopAt; --it) { //first round -- respecting minwb rule
				auto i = size_t(it);
				if(sizes[i]>minWbSz) {
					totalInUb -= sizes[i];
					evictions[i].push_back(sizes[i]);
					sizes[i]=0;
				}
			}
			for(auto it=int(castesCount)-1; it>=0 && totalInUb > stopAt; --it) {//2nd round -- anything goes
				auto i = size_t(it);
				if(sizes[i]) {
					totalInUb -= sizes[i];
					evictions[i].push_back(sizes[i]);
					sizes[i]=0;
				}
			}
			CHEAP_ASSERT(totalInUb <= stopAt, "wrong eviction")
			TASSERT(std::accumulate(sizes.cbegin(),sizes.cend(),size_t(0)) == totalInUb, "bad eviction")
		} //end eviction
	}

	auto i = 0;
	for(auto& v : evictions) {
		COUT<< ENDL <<i++ << " ";
		for(auto s : v) COUT << s << " ";
	}

}

void BufferManageSimulator::run2(size_t totalPostings, const size_t castesCount, const size_t /*minWbSz*/, size_t freeUpTo) {
	clear(castesCount);

	constexpr size_t batch(32000);
	size_t stopAt = ubsizePostings * (100-freeUpTo) / 100;
	size_t totalInUb = 0;
	//simulation loop
	for(size_t totalWentThrough = 0; totalWentThrough<totalPostings; ) {
		for(auto& cs : sizes) //round robin
			cs += batch;

		totalInUb += batch*castesCount;
		totalWentThrough += batch*castesCount;

		if(totalInUb > ubsizePostings) { //start eviction
			UpdatesBuffer::CasteEvictor ev(ubsizePostings,nullptr);
			unsigned round = 0;
			while(totalInUb > stopAt) {
				CHEAP_ASSERT(round<3,"too many rounds")
								ev.injectSizesSim(sizes);
				ev.coreReset(0);
				for(auto it=ev.nextId(); it!=BUFF_LIMIT && totalInUb > stopAt; it=ev.nextId()) {
					totalInUb -= sizes[it];
					evictions[it].push_back(sizes[it]);
					sizes[it]=0;
				}
				++round;
			}
			TASSERT(std::accumulate(sizes.cbegin(),sizes.cend(),0UL) == totalInUb, "bad eviction")
		} //end eviction
	}



}

void BufferManageSimulator::print() const {
	for(auto& v : evictions) {
		for(auto s : v)
			COUT << s << ",";
		COUT << ENDL;
	}
}

void BufferManageSimulator::normalizeSizes(std::vector<size_t>& epochSizes) {
	CHEAP_ASSERT(!epochSizes.empty(),"why empty?")
	double smallest = *std::min_element(epochSizes.begin(),epochSizes.end());
	CHEAP_ASSERT(smallest > reduceTo, "too small");
	double div = smallest / reduceTo;
	for(auto& s : epochSizes) {
		s = round(double(s)/div);
		//COUTL << s << ENDL;
	}
}

size_t BufferManageSimulator::evictionRound(size_t &totalInUb, size_t ubLimit) {
	UpdatesBuffer::CasteEvictor ev(ubsizePostings,nullptr);
	ev.injectSizesSim(sizes); //dummy now
	unsigned round = 0;
	size_t totalEvicted = 0;
	//COUTL << totalInUb << ENDL;
	while(totalInUb > ubLimit) {
		CHEAP_ASSERT(round<3,"too many rounds");
		ev.coreReset(round);
		for(auto it=ev.nextId(); it!=BUFF_LIMIT && totalInUb > ubLimit; it=ev.nextId()) {
			//COUTL << it << " " << sizes[it] << ENDL;
			const size_t sz = sizes[it];
			totalInUb -= sz;
			totalEvicted+=sz;
			evictions[it].push_back(sz);
			sizes[it]=0;
		}
		++round;
	}
	CHEAP_ASSERT(std::accumulate(sizes.cbegin(),sizes.cend(),0UL) == totalInUb, "bad eviction")
	//COUTL << std::accumulate(sizes.cbegin(),sizes.cend(),0UL) << " left in buffer with round=" << round << "\n";
	return totalEvicted;
}

//Assume we are before next eviction.
//We know the epoch data for every caste
//And we get the vector of ubPostings
//Our goal is to simulate the next evictions
//*here the castes are 0-indexed and are in the order returned by get-sorted!!!
void BufferManageSimulator::predictForPrognosticator(std::vector<size_t>& epochSizes, std::vector<size_t> ubPostings,size_t totalPostings, size_t ubLimit) {
	const size_t castesCount = epochSizes.size();

	//get rid of last one
	if(ubPostings.size() > castesCount)
		ubPostings.pop_back();
	CHEAP_ASSERT(ubPostings.size() == castesCount,"more castes?");

	clear(castesCount);
	normalizeSizes(epochSizes);

	//	COUTL << "ubsizePostings: " << ubsizePostings << " ubLimit: " << ubLimit << ENDL;
	sizes = ubPostings;
	size_t totalInUb = std::accumulate(sizes.begin(),sizes.end(),0ULL);

	size_t totalEvicted = 0;

	if(totalInUb > ubsizePostings) 	//first evictions are out of loop:
		evictionRound(totalInUb, ubLimit);

	//simulation loop
	while(totalEvicted<totalPostings) {
		for(unsigned i=0; i<castesCount; ++i) { //basic cycle add ~10,000 to every caste
			const size_t sz = epochSizes[i];
			sizes[i] += sz;
			totalInUb += sz;
		}

		if(totalInUb > ubsizePostings) { //start eviction if no space left in buffer
			totalEvicted += evictionRound(totalInUb, ubLimit);
		}
	}
	//the last thing we must do is to push the non-evicted data from UB as an aux. data helper:
	for(unsigned i=0; i<castesCount; ++i) {
		evictions[i].push_back(sizes[i]);
	}
}


} /* namespace Upd */
