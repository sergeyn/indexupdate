##Optimal consolidation

**Given**:   
SZ is a list of N+1 uints &mdash; N eviction sizes and 1 posting count in UB after last eviction (perhaps 0).  
UQ is updates to queries ratio (double)  

We first form a query-per-window (QPW) list.  
QPW[0] is the amount of queries we expect to see by the time of first eviction SZ[0].  
QPW[i] is the amount of queries we expect to see by the time of i+1 eviction SZ[i].
QPW[i] = SZ[i] * UQ 

####Example
SZ=[A,B,<C>] is the list of 2 evictions + 1 UB postings  
|SZ|=3
 
During simulation for iteration 0, we shall register QPW[0] queries multiplied by seeks needed, which is 0.  
During simulation for iteration 1, we shall register QPW[1] queries multiplied by seeks needed, which is still 0.  
During simulation for iteration 2, we shall register QPW[2] queries multiplied by seeks needed, which is still 1 (unless a merge was made, then 0).  
During simulation for iteration i, we shall register QPW[i] queries multiplied by seeks needed, i-1 (unless we had merges).  

**Total cost if no merges:**
  
```c
     for( i = 2; i<N+1)  
          sumSeeks += QPW[i] * (i-1)  
```
		
**Calculate the seeks saved by consolidating two blocks**

```
.[k m n].[o p].<future evictions>
|       |     |
left   pos.   right 
border        border 
```

Because it makes sense to merge as soon as possible, we know that there are no evictions after p (at runtime),  
hence the consolidation is going to allow -1 seek for all queries after that point.  
I.e. the sum of QPW[i] s.t. *i > right border*

####System
The main memory budget (8GB) is split between the Update-Buffer and the Term-Cache.   
TC was formerly evicting using LRU, but now it is operating a Landlord policy.  
When UB is full, always and logarithmic are writing-back everything to disk.  
In our system the eviction happens on a by-caste basis via policy somewhat reminiscent of a Landlord.  
Often updated and queried castes tend to remain in the UB much longer, and thus it acts as a form of caching.  

The consolidation of the segments on disk into a single contiguos block is scheduled:  
* by ski-rental &mdash; when the accumulated overhead of having those extra seeks for this caste becomes larger than the cost of the consolidation  
* by prognosticator &mdash; after projecting the sizes of future segments   
and the amount of queries hitting the caste we calculate the optimal consolidation schedule  
using an offline algorithm. If in that schedule a merge of exisiting segments is due, we perform it.     
The cost of projection and finding optimal is so low, we can afford to run it at every eviction from UB.

Problems with the prognosticator:
a. is not TC aware, in practice, if some terms of the caste are in TC they will not require disk IO.  
b. the projection of updates and queries is very good, but not perfect. It will be even worse in a volatile environment.   
c. the cost of consolidation only approximates the actual (though still simulated) costs.  



  

