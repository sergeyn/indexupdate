#include "Registry.h"

unsigned globalVerbosity;
Registry globalRegistry;

Registry::~Registry() {
        for(auto ptr : registry)
                delete ptr.second;
}

void Registry::registerClass(const RegistryEntry* newClass) {
        const std::string& name = newClass->name();
        COUT4 << "Registering: " << name << ENDL;
        if(name.empty() || registry.count(name)) FATAL("classname '"+name+"' is a duplicate");
        registry[name]=newClass; //this guy better be alive... (perhaps a shared_ptr?)
}
