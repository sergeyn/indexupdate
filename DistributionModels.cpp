#include "DistributionModels.h"
#include "Asserts.h"
#include "Globals.h"

/*
 std::vector<did_t> SparseHistogram::getDense() const {
 std::vector<did_t> res;
 if(data.empty()) {
 res.resize(maxKey+1,smooth);
 return res;
 }
 auto it = data.begin();
 res.reserve(maxKey+1);
 res.push_back(0);
 for(key_t i=1; i<=maxKey; ++i) { //note the 1 and the <=
 if(unlikely(it->first==i)) {
 res.push_back(it->second);
 ++it;
 }
 else
 res.push_back(smooth);
 } //end for
 return res;
 }
 */
/*
 std::fstream myFile ("/tmp/tidsUpdate.bin", std::ios::out | std::ios::binary);
 for(unsigned i=0; i<(1<<30); ++i) {
 tid_t nxt = distribution->next();
 myFile.write ((char*)&nxt, sizeof (tid_t));
 }  myFile.flush();  myFile.close();
 */
void DistrFromFile::openFile() {
	if (file.is_open())
		file.close();
	file.open(filename, std::ifstream::binary);

	if (!file)
		SCREAM("can't open " + filename);

}

void DistrFromFile::shuffle() {
	CHEAP_ASSERT(seekTos.empty(),"empty");
	seekTos.resize(fileSizeInBuffers,0);
	for(auto i : make_irange(fileSizeInBuffers))
		seekTos[i] = i*BSZ;
	std::shuffle(seekTos.begin(),seekTos.end(),generator);
}


void DistrFromFile::reset() {
	static_assert(BSZ%sizeof(tid_t)==0, "must have aligned buffer");
	openFile();
	file.seekg(0, file.end);
	fileSizeInBuffers = _rfdiv(file.tellg(), BSZ);
	file.seekg (0, file.beg);
}

DistrFromFile::DistrFromFile(const std::string& fname) :filename(fname),rdLimit(0),current(0),fileSizeInBuffers(0){
	generator.seed(0);
	buffer.resize(BSZ);
	reset();
}
DistrFromFile::DistrFromFile(const DistrFromFile& rhs) :filename(rhs.filename), rdLimit(0),current(0),fileSizeInBuffers(0){
	generator.seed(0);
	buffer.resize(BSZ);
	reset();
}

DistributionInterface* DistrFromFile::clone() const {
#ifdef DEBUG
	#warning Unseens are off
#endif
	return new DistrFromFile(*this);
}

void DistrFromFile::fillBufferNext() {
	if(seekTos.empty())
		shuffle();
    auto where = seekTos.back();
    seekTos.pop_back();
	file.seekg(where);
	if(!file)
		openFile();
	file.read(reinterpret_cast<char*>(buffer.data()), sizeof(tid_t)* BSZ);
	rdLimit = unsigned(file.gcount() / long(sizeof(tid_t)));
	current = 0;
}

DistributionInterface* DiscreteWithUnseens::clone() const {
	SCREAM("DiscreteWithUnseens::clone stub");
	return nullptr;
}
