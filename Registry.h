#ifndef REGISTRY_H_
#define REGISTRY_H_

#include "Interfaces.h"


        //-------------------------------------------------------------------
        class Registry {
                hashMapStd<std::string,const RegistryEntry*> registry;
                void registerClass(const RegistryEntry* newClass);

        public:
                Registry() {}
                ~Registry();

                template<typename T>
                void registerClass();

                template<typename T>
                T* getInstanceOf(const std::string& className);
        };

        template<typename T>
        T* Registry::getInstanceOf(const std::string& className) {
                if(!registry.count(className)) FATAL("["+className+"] was not registered");
                T* ptr = reinterpret_cast<T*>(registry[className]->getOne());
                return ptr;
        }

        template<typename T>
        void  Registry::registerClass() {
                registerClass(new T);
        }

        //Note 1: should be placed in the public part of the class...
        //Note 2: the name can be any string -- doesn't have to be the name of class. Can be used for aliasing
        #define REGISTRY_MACRO(classNameString) const std::string name() const { return classNameString;} void* getOne() const { return new std::decay<decltype(*this)>::type(); }

        extern Registry globalRegistry;


#endif /* REGISTRY_H_ */
