require 'optparse'
require 'open3'


def runCmdWithPrints cmd
  Open3.popen2e(cmd) do |stdin, stdout_err, wait_thr|
    while line = stdout_err.gets
      puts line
    end
  end
end

$opt= { 'verbosity' => 4 }
$opt['updatebszkb']=[262144] 
$opt['stablecapacity']=[0]
$opt['cachesz']=[0]
$opt['postings']=[5440000000]
$opt['monolithicmerge']=[1]
$opt['upsrate']=[1000000]
$opt['queryrate']=[16]
$opt['qproc']=['QpWithCastes','QpNoWriteBack']
  
optparse = OptionParser.new do |o|
  o.on('-u', '--updatebszkb V',Array,'Update buffer size in kb') { |b| $opt['updatebszkb'] = b.map {|v| v.to_i} }
  o.on('-s', '--stablecapacity V',Array,'the size of stable case in postings. default 0 for growing to inf') { |b| $opt['stablecapacity'] = b.map {|v| v.to_i} }
  o.on('-c', '--cachesz V',Array,'the size of the cache in postings. default 0 for none') { |b| $opt['cachesz'] = b.map {|v| v.to_i} }
  o.on('-p', '--postings V',Array,'total simulation postings') { |b| $opt['postings'] = b.map {|v| v.to_i} }
  o.on('-m', '--monolithicmerge V',Array,'1: merge always, 2:merge Log, 0: never merge //unless alg is QpWithCastes') { |b| $opt['monolithicmerge'] = b.map {|v| v.to_i} }
  o.on('-n', '--updatesrate V',Array,'updates rate') { |b| $opt['upsrate'] = b.map {|v| v.to_i} }
  o.on('-q', '--queryrate V',Array,'so many queries for every *updatesrate* updates') { |b| $opt['queryrate'] = b.map {|v| v.to_i} }
  o.on('-a', '--qproc V',Array,'qproc algs names') { |b| $opt['qproc'] = b }
  o.on('-h') { puts o; exit }
  o.parse!
end

def genCmdLines dir='/tmp/'
  lines = []
  $opt['qproc'].each { |alg|
    #one exception -- there is no point run with different monolitic merge options for QpWithCastes
    merge = (alg == 'QpWithCastes') ? [1] :  $opt['monolithicmerge']    
    combinations = $opt['updatebszkb'].product $opt['stablecapacity'],$opt['cachesz'],$opt['postings'],merge,$opt['upsrate'],$opt['queryrate'],[alg]
    names = ['updatebszkb','stablecapacity','cachesz','postings','monolithicmerge','upsrate','queryrate','qproc']
    combinations.each { |aopts|
      cmd = ["./upd "]
      names.each_with_index { |n,i| cmd << "-#{n} #{aopts[i]}" }
      cmd << "| tee #{dir}/log_#{aopts.join('_')}" 
      lines << cmd.join(' ')
    }  
  }
  lines
end


optparse.parse!
dirname = ARGV[0] || "/tmp"
dirname = dirname + "/#{Time.now.day}_#{Time.now.month}_#{Time.now.hour}/"
%x[mkdir -p #{dirname}]
  
unless(ARGV[0])
  p $opt
  puts genCmdLines(dirname).join("\n")
else
  f = File.new(dirname+'/run.sh','w') 
  cmds = genCmdLines(dirname)
  $opt.each { |k,v| f.puts "# #{k} \t #{v}" }
  f.puts cmds.join("\n") 
  f.close
  cmds.each { |c|  runCmdWithPrints c }
end  