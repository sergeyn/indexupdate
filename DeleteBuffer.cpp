/*
 * DeleteBuffer.cpp
 *
 *  Created on: Jul 29, 2015
 *      Author: sergeyn
 */

#include "DeleteBuffer.h"
#include "DistributionModels.h"

std::pair<unsigned,size_t> auxDelChronoByStack(size_t size, const std::vector<size_t>& sizeStack);


	HistDeletes::HistDeletes(DistributionInterface* delDist) : deleteDist(delDist),deletedTids(0){
		casteBuffers.resize(512,0);
	}

	void HistDeletes::auxPopulate(size_t count) {
		tempTidBuffer.clear(); tempTidBuffer.reserve(count);
		for(size_t i=0; i<count; ++i) {
			tempTidBuffer.push_back(tid_t(deleteDist->next()));
		}
		deletedTids += count;
	}

	void HistDeletes::updateLexiconForChrono(size_t termsCount) {
		if(termsCount>=chronoDeletesPerTerm.size()) {
			COUT4 << "In HistDeletes::populate T.count: " << termsCount << "\n";
			chronoDeletesPerTerm.resize(termsCount+1024,0);
		}

		for(auto tid : tempTidBuffer)
			++chronoDeletesPerTerm[tid]; //add +1 deletion to a term sampled from deleteDistr
	}

	void HistDeletes::updatePerCasteDeleteCounts(const std::vector<caste_t>* tid2caste) {
		const auto sz = tid2caste->size();
		for(auto tid : tempTidBuffer) {
			unsigned cid = tid<sz ? (*tid2caste)[tid] : 0; //0 - is invalid cid, will be ignored
			if(unlikely(cid >= casteBuffers.size()))
				casteBuffers.resize(cid+128);
			++casteBuffers[cid];
		}
	}


	void HistDeletes::populateChrono(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid) {
				auxPopulate(count);
				updateLexiconForChrono(termsCount);
				if(tid2cid)
					updatePerCasteDeleteCounts(tid2cid);
	}
	void HistDeletes::populateRnd(size_t count, const std::vector<caste_t>* tid2cid) {
				auxPopulate(count);
				if(tid2cid)
					updatePerCasteDeleteCounts(tid2cid);
	}

	//see how many full segments can be purged starting from most ancient
	//returns stoppping index (one past removed) -- returns 0 when no move
	//and total removed
	std::pair<unsigned,size_t> auxDelChronoByStack(size_t size, const std::vector<size_t>& sizeStack) {
		if(size == 0 || sizeStack.size() == 0 )
			return {0,0ull};

		auto oldSize = size;
		unsigned index = 0;
		while(size > sizeStack[index])
			size -= sizeStack[index++];
		CHEAP_ASSERT(size<std::accumulate(sizeStack.begin(),sizeStack.end(),0ull),"removing too much")
		return std::make_pair(index,oldSize-size);
	}

	void HistDeletes::auxPartial(size_t& deletes, size_t& segmentSize) {
		CHEAP_ASSERT(deletes<=segmentSize,"more deletes than stack!");
		segmentSize -= deletes;
		if(deletes)	COUTV(5) <<  "Reimbursed in Chrono for writing " << deletes << " postings\n";
		deletes = 0;
	}

	void HistDeletes::partialDeleteBeforeMerge(size_t& segmentSize) {
		auxPartial(deletedTids,segmentSize);
	}
	void HistDeletes::partialDeleteBeforeMerge(unsigned cid,size_t& segmentSize) {
		if(cid>=casteBuffers.size()) casteBuffers.resize(cid+128,0);
		auxPartial(casteBuffers[cid],segmentSize);
	}

	// for this caste only
	unsigned  HistDeletes::deleteChronoCasteTidsByStack(unsigned cid, const std::vector<size_t>& casteSizeStack) {
		if(cid>=casteBuffers.size()) casteBuffers.resize(cid+128,0);

		auto ret = auxDelChronoByStack(casteBuffers[cid],casteSizeStack);
		casteBuffers[cid] -= ret.second;
		return ret.first;
	}

	//see how many full segments can be purged starting from most ancient
	unsigned  HistDeletes::deleteChronoMonolithicTidsByStack(const std::vector<size_t>& sizeStack) {
		auto ret = auxDelChronoByStack(deletedTids,sizeStack);
		deletedTids -= ret.second;
		return ret.first;
	}

	//if we were to consolidate a tail starting at - startAt
	//not-writing how many postings we were to reimburse?
	size_t auxDelRndByStack(const size_t deletedCount, const std::vector<size_t>& sizeStack, unsigned startAt) {
		CHEAP_ASSERT(startAt<sizeStack.size(), "empty tail")
		const double allPostings = std::accumulate(sizeStack.begin(),sizeStack.end(),size_t(0));
		CHEAP_ASSERT(deletedCount<=allPostings,"deleted more than we have ")
		if(startAt == 0)
			return deletedCount;

		const double tailPostings = std::accumulate(sizeStack.begin()+startAt,sizeStack.end(),size_t(0));
		size_t share = double(deletedCount) * (tailPostings/allPostings);
		return share;
	}


	size_t HistDeletes::deleteRndCasteTidsByStack(unsigned cid, const std::vector<size_t>& casteSizeStack, unsigned startAt) {
		if(cid>=casteBuffers.size())
			casteBuffers.resize(cid+128,0);

		auto ret = auxDelRndByStack(casteBuffers[cid],casteSizeStack,startAt);
		casteBuffers[cid] -= ret;
		return ret;
	}

	size_t  HistDeletes::deleteRndMonolithicTidsByStack(const std::vector<size_t>& sizeStack, unsigned startAt) {
		auto ret = auxDelRndByStack(deletedTids,sizeStack,startAt);
		deletedTids -= ret;
		return ret;
	}
