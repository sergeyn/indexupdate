#ifndef QP_H_
#define QP_H_

#include "Interfaces.h"
#include "Registry.h"
#include "Landlord.h"

class HistDeletes;


class QPBase : public ProcessTerm {
protected:
	bool isSkiForce; //is it ski-rental all the way?
	bool isRandom; //is random delete -- i.e. is env 3?
	unsigned env;
	size_t maxPostings;

public:
	std::vector<unsigned> recordQLens;
	std::vector<unsigned> recordTerms;
	void setCacheSize(size_t szInPostings);
	inline bool visitCache(tid_t term, size_t length) { return cache.maxPostings == 0 ? false : _visitCache(term,length); }
	//inline bool isMiss(tid_t term) const { return cachedLookup.count(term) == 0; }

	size_t cachedMCount() const {/*COUTL<<cachedQ.size()<<ENDL;*/ return cache.size(); }
	size_t hitsCount() const { return cache.cacheHits; }
	size_t cachedPostings() const { return cache.getTotalP(); }

	QPBase();
	~QPBase();
	virtual size_t totalAskedQs() const { return totalAsked; }
	virtual size_t totalServedFromDiskQs() const{return qsServedFromDisk; }
	virtual size_t totalPostingsServed() const{return cache.cachePostingsServed; }
	virtual size_t totalPostingsMissed() const{return cache.cachePostingsMissed; }
	virtual void printHist() const;

	virtual void populateChrono(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid = nullptr); //if count == 0 we just resize!
	virtual void populateRnd(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid = nullptr); //populate deleted buffer
	virtual void setDeletesDistr(DistributionInterface* d);
	virtual HistDeletes* getInnerDelBuffer() { return deletedBuffer; }

	virtual void setIsSkiAndRandom(unsigned e, bool s ,bool r, size_t maxP ){ env = e; isSkiForce = s; isRandom = r; maxPostings = maxP;}
	const Caching::Landlord& getCache() const {
		return cache;
	}
protected:
	bool _visitCache(tid_t term, size_t length);

	size_t acknowledgeCrhonoShift(tid_t term) const;

	listAttributesProvider* casteManager;
	size_t totalAsked;
	size_t qsServedFromDisk;
	std::unordered_map<size_t,size_t> segmentsHist;

	//cache related:
	Caching::Landlord /*MoveToFront*/ cache;
	/*
	size_t cSizeInPostings;
	size_t cCachedPostings;
	size_t cacheHits;
	size_t cachePostingsServed;
	size_t cachePostingsMissed;
	typedef std::pair<tid_t,did_t> termLen_t;
	std::list<termLen_t> cachedQ;
	typedef std::list<termLen_t>::iterator LI;
	std::unordered_map<tid_t, LI> cachedLookup;
*/
	HistDeletes* deletedBuffer;


};

class QpNoWriteBack :public QPBase , public RegistryEntry {
	listAttributesUpdater* lexUpd;
	listAttributesProvider* globalLex;

	DiskReaderI* queryReader; //the reader for queries

	unsigned getSegments(tid_t tid) const;

public:
	QpNoWriteBack();
	void setup(
			std::unordered_map<std::string,listAttributesUpdater*> updaters,
			std::unordered_map<std::string,listAttributesProvider*> providers,
			DiskReaderI& qrdr,
			DiskWriterI& ,
			DiskReaderI& ,
			DiskWriterI& );
	size_t operator()(tid_t tid);
	virtual const std::string getCname() const { return name(); }
	REGISTRY_MACRO("QpNoWriteBack")
};

class LexiconForGolden;
struct caste;
class QpWithCastes : public QPBase, public RegistryEntry {
	listAttributesUpdater* lexUpd;

	DiskReaderI* queryReader; //the reader for queries
	DiskReaderI* consolidationReader; //what we had to read to do the merge
	DiskWriterI* consolidationWriter; //writing during merges

	//bool attemptConsolidation(float FACTOR, caste& cst, LexiconForGolden* globalLex, size_t& totalPostings);
	//void consolidate(const unsigned segmentOffset, LexiconForGolden* globalLex, caste& cst, const size_t cPostings);

	bool forceGeoMerge;
public:
	const UpdatesBuffer* ubp;
	size_t zeroRet[3];

	QpWithCastes();
	virtual std::pair<size_t,const void*> getMaxAndUB()const {return std::make_pair(maxPostings,ubp); } //quick and dirty...
	virtual void setUB(void* ub){ubp=reinterpret_cast<UpdatesBuffer*>(ub);}
	void setForceGeoMerge(bool should) { forceGeoMerge = should; }
	void setup(
			std::unordered_map<std::string,listAttributesUpdater*> updaters,
			std::unordered_map<std::string,listAttributesProvider*> providers,
			DiskReaderI& qrdr,
			DiskWriterI& cwrt,
			DiskReaderI& crdr,
			DiskWriterI& );
	virtual const std::string getCname() const { return name(); }
	size_t operator()(tid_t tid);

	//size_t getDeletedCountAndZero() {auto cnt = deletedCount; deletedCount=0; return cnt;}
	REGISTRY_MACRO("QpWithCastes")
};

class QpURF :public QPBase , public RegistryEntry {
	listAttributesUpdater* lexUpd;
	listAttributesProvider* globalLex;

	DiskReaderI* queryReader; //the reader for queries

	unsigned getSegments(tid_t tid) const;

public:
	QpURF();
	void setup(
			std::unordered_map<std::string,listAttributesUpdater*> updaters,
			std::unordered_map<std::string,listAttributesProvider*> providers,
			DiskReaderI& qrdr,
			DiskWriterI& ,
			DiskReaderI& ,
			DiskWriterI& );
	size_t operator()(tid_t tid);
	virtual const std::string getCname() const { return name(); }
	REGISTRY_MACRO("QpURF")
};

#endif /* QP_H_ */
