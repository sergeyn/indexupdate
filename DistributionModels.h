#ifndef DISTRIBUTIONMODELS_H_
#define DISTRIBUTIONMODELS_H_

//#include <vector>
//#include <algorithm>
//#include <random>

#include "Globals.h"
#include "Interfaces.h"


//we generate a random posting for existing term or form a new one
//assume that the distr doesn't change
class DiscreteWithUnseens : public DistributionInterface{
	const size_t samplesCount;
	size_t newGuysCount;
	//size_t totalProduced;

	std::default_random_engine generator;
    std::mt19937 generatorMain;
	std::discrete_distribution<size_t> distribution;
	std::uniform_int_distribution<size_t> newGuys;

public:
	DiscreteWithUnseens(const std::vector<unsigned>& dense, unsigned howOftenNewTerm):samplesCount(dense.size()),newGuysCount(0),newGuys(1,howOftenNewTerm){
		distribution = std::discrete_distribution<size_t>(dense.begin(),dense.end()); //init the freqs
		//totalProduced = std::accumulate(dense.begin(),dense.end(),0);
	}

	void reseed(size_t newSeed) {
		generator.seed(newSeed);
		generatorMain.seed(newSeed);
		newGuysCount=0;
	}

	DistributionInterface* clone() const;

	void setMax(size_t ) {/*innocent stub? or is it?*/}
	size_t size() const { return samplesCount+newGuysCount; }
	//did_t total() const { return totalProduced+newGuysCount; }
	size_t unseens() const { return newGuysCount; }

	inline virtual size_t next() {
		/*if(newGuys(generator) == 1) {
			++newGuysCount;
			return size();
		}*/
		return distribution(generatorMain);
	}
};

#include <fstream> //currently doesn't support unseens
class DistrFromFile : public DistributionInterface{
	std::ifstream file;
	std::string filename;

	static constexpr size_t  BSZ  = 8*4096*4096;
	std::vector<tid_t> buffer;
	unsigned rdLimit;
	unsigned current;
	size_t fileSizeInBuffers; //in buffer-sizes

	std::default_random_engine generator;
    std::vector<size_t> seekTos;

	void openFile();
	void reset();
	void shuffle();
public:
	void fillBufferNext();
	void setMax(size_t ) {/*innocent stub? or is it?*/}
	DistrFromFile(const std::string& fname);
	DistrFromFile(const DistrFromFile& rhs);
	DistributionInterface* clone() const;
	inline size_t next() {
		if(current>=rdLimit)
			fillBufferNext();
		return buffer[current++];
	}
	void   reset(size_t /*newSeed*/){ reset(); fillBufferNext(); }
};

/*
//we generate a random posting for existing term or form a new one
//assume that the distr doesn't change
class SparseWithUnseens : public DistributionInterface{
	size_t maxN;
	size_t newGuysCount;
	//size_t totalProduced;
	std::default_random_engine generator;
    std::mt19937 generatorMain;
	std::discrete_distribution<size_t> distribution;
	std::uniform_int_distribution<size_t> newGuys;
	std::uniform_int_distribution<size_t> newGuysDistr;
	const std::vector<tid_t>& sparseIds;

public:
	SparseWithUnseens(const std::vector<tid_t>& sprseIds, const std::vector<unsigned>& sparseFreqs, tid_t currentMax, unsigned howOftenNewGuy):
		maxN(currentMax),newGuysCount(0),newGuys(1,howOftenNewGuy),sparseIds(sprseIds){
		distribution = std::discrete_distribution<size_t>(sparseFreqs.begin(),sparseFreqs.end()); //init the freqs
		reseed(0);
		//totalProduced = std::accumulate(dense.begin(),dense.end(),0);
		//std::random_device rd;
	}

	void reseed(size_t newSeed) {
		generator.seed(newSeed);
		generatorMain.seed(newSeed);
		newGuysCount=0;
	}

	void setMax(size_t mx) {
		if(mx>maxN) {
			maxN = mx;
			newGuysDistr = std::uniform_int_distribution<size_t>(1,maxN);
		}
	}

	//tid_t size() const { return samplesCount+newGuysCount; }
	//did_t total() const { return totalProduced+newGuysCount; }
	tid_t unseens() const { return newGuysCount; }

	virtual size_t next() {
		if(newGuys(generator) == 1) {
			++newGuysCount;
			return newGuysDistr(generatorMain);
		}
		return sparseIds[distribution(generatorMain)];
	}
};
*/
//basically a vector of <id,count> ordered by id.
//If maxid == data.size => O(1) extraction, else O(logn)
//If we query id=i and it is lacking then the count for this one is 1 (or other smoothing)
// assume ordered and unique keys
//class SparseHistogram {
//	typedef tid_t key_type;
//	typedef std::pair<key_type,did_t> tdPair;
//	enum states { UNKNOWN, SORTED_KEY, SORTED_VAL };
//
//	std::vector<tdPair> data;
//	const key_type maxKey; //max but still legal key!
//	const did_t smooth;
//	int sortedAt;
//public:
//	SparseHistogram(key_type maxK = (1<<31), did_t smoothing=1) : maxKey(maxK), smooth(smoothing), sortedAt(-1){
//		data.push_back(std::make_pair(0,maxK)); //illegal value
//	}
//
//	key_type getMax() const { return maxKey; }
//	void reserve(key_type n) { data.reserve(n+1); }
//	void push_back(key_type t,did_t c) { data.push_back(std::make_pair(t,c)); }
//	void push_back_strict(key_type t,did_t c) {
//		if(data.back().first<=t || !isSortedId() ) //bubu
//			throw this;
//		data.push_back(std::make_pair(t,c));
//		++sortedAt;
//	}
//	void sortById() { std::sort(data.begin(),data.end()); sortedAt=(data.size());} //default for pair comparison is first
//	did_t operator[](key_type k) const {
//		assert(isSortedId()); //assume sorted -- no push_backs since sorting
//		if(data.size()>=1+maxKey) //if not sparse at all - O(1)
//			return data[k].second;
//		auto it = std::lower_bound(data.begin(),data.end(),std::make_pair(k,maxKey)); //do logn search
//		return (it == data.end() || it->first != k) ? smooth : it->second;
//	}
//	std::vector<did_t> getDense() const;
//	bool isSortedId() const { return sortedAt == (int)data.size(); }
//};
#endif /* DISTRIBUTIONMODELS_H_ */
