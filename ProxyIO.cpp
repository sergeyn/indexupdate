#include "ProxyIO.h"

unsigned PostingSize::szOfPostingBytes(1); //will be set in main

std::ostream& operator<<(std::ostream &out ,const ConsolidationStats& cons) {
	return out << "rdata: " << cons.rdata << "(" << PostingSize::bytesOfPostings(cons.rdata) <<
		   ") wdata: " << cons.wdata << " (" << PostingSize::bytesOfPostings(cons.wdata) <<
		   ") rseeks:" << cons.rseeks <<
		   " wseeks:" << cons.wseeks <<
		   " time: " << ConsTime::consolidationDataToMinutes(cons);
}

std::ostream& operator<<(std::ostream& out,const consolidation& c) {
	return out << '[' << c.first << ',' << c.second << ']';
}

ConsolidationStats auxConsolidateTail(std::vector<size_t>& sizeStack/*, std::vector<double>& consolidationPriceVector*/, offset_t offset,size_t /*_isPB -- 0 or 1*/, size_t lastInMem /*0 or 1*/){
	CHEAP_ASSERT(offset<=sizeStack.size(),"bad offset");
	if(offset >= sizeStack.size()-1) { //just write the last one to disk
		CHEAP_ASSERT(lastInMem,"not in mem?")
		return ConsolidationStats(0,0,sizeStack.back(),1);
	}
	auto cons = kWayConsolidate(sizeStack.begin()+offset,sizeStack.end(),lastInMem);
	size_t writtenPostings = std::accumulate(sizeStack.begin()+offset,sizeStack.end(),0ull); //NOTE: != cons.wdata
	sizeStack.erase(sizeStack.begin()+offset,sizeStack.end());
	sizeStack.push_back(writtenPostings);
	return cons;
}

WriteBackBuffer::WriteBackBuffer(/*LexiconForGolden& l*/) :/* lex(l),*/ bytesWritten(0), totalSeeks(0) {
}

class KWaySegmentConsolidator {
	std::vector<size_t> &segHeap;
	const size_t memSizeInPostings;
	size_t lastSize;
	ConsolidationStats cost;

	//while we can put many into buffer completely
	bool roundKWay() {
		auto smallest1 = pop();
		auto smallest2 = pop();

		size_t reads = smallest1+smallest2;
		size_t writes = reads;
		unsigned totalReadSeeks = 2;

		//if one of the smallest is in mem
		if(lastSize && (lastSize == smallest1 || lastSize == smallest2)) {
			--totalReadSeeks;
			reads -= lastSize;
			lastSize = 0;
		}

		//can't handle even the two smallest
		if(reads > memSizeInPostings/2) { //roll back
			push(smallest2);
			push(smallest1);
			if(writes != reads) //it is possible one was in-mem
				lastSize = writes-reads;
			return false;
		}

		while(!segHeap.empty()) {
			size_t seg = pop();
			if(seg == lastSize) {
				lastSize = 0;
			}
			else {
				if(reads+seg>memSizeInPostings/2) { //too much
					push(seg);
					break;
				}
				reads+=seg;
				++totalReadSeeks;
			}
			writes += seg;
		}

		push(writes);

		cost += ConsolidationStats(totalReadSeeks, reads, writes,1);
		return true;
	}

	size_t pop() {
		 std::pop_heap(segHeap.begin(), segHeap.end(),std::greater<size_t>());
		 auto smallest = segHeap.back();
		 segHeap.pop_back();
		 return smallest;
	}
	void push(size_t s) {
		segHeap.push_back(s);
		std::push_heap(segHeap.begin(), segHeap.end(),std::greater<size_t>());
	}

	inline size_t writePShare() const { return (memSizeInPostings/2); }

	void round2Way() {
		const auto writeShare = writePShare();
		const auto readShare = writeShare/2;

		auto smallest1 = pop();
		auto smallest2 = pop();

		size_t readSeeks = 0;
		readSeeks += _rfdiv(smallest1,readShare);
		readSeeks += _rfdiv(smallest2,readShare);
		size_t readSeg = smallest1+smallest2;
		size_t writeSeg = smallest1+smallest2;
		size_t writeSeeks = _rfdiv(writeSeg,writeShare);

		//fix seeks and reads if in mem
		if(lastSize && (lastSize == smallest1 || lastSize == smallest2)) {
			readSeg -= lastSize;
			readSeeks -= _rfdiv(lastSize,readShare);
			lastSize = 0; //this can happen only once per consolidation!
		}

		push(writeSeg);
		cost += ConsolidationStats(readSeeks,readSeg, writeSeg,writeSeeks);
	}

public:

	KWaySegmentConsolidator(std::vector<size_t>& segments, size_t lastInMem /*0 or 1*/, size_t memBufferPostings)
		: segHeap(segments), memSizeInPostings(memBufferPostings){

		TASSERT(lastInMem < 2, "wrong args!")
		lastSize = segHeap.back() * lastInMem;
		std::make_heap(segHeap.begin(),segHeap.end(),std::greater<size_t>());
	}

	ConsolidationStats operator()() {
		cost = ConsolidationStats();
		//attempt k-way consolidation of all the segments that fit completely into mem
		while(segHeap.size() > 1 && roundKWay())
			continue;
		while(segHeap.size() > 1) //always consolidate two smallest
			round2Way();

		return cost;
	}
};

//note: reduceRead is only applicable to a, not to b.
ConsolidationStats consolidateTwoSegments(size_t a, size_t b, bool secondInMem, size_t reduceWrite, size_t reduceRead, size_t memBuffer) {
	CHEAP_ASSERT(a>=reduceRead,"underflow reduction");

	const auto writeShare = memBuffer/2;
	const auto readShare = memBuffer/4;

	size_t readSeeks = 0;
	readSeeks += _rfdiv(a-reduceRead,readShare);
	readSeeks += secondInMem ? 0 : _rfdiv(b,readShare);
	size_t readSeg = a+(secondInMem ? 0 : b);
	size_t writeSeg = a+b-reduceWrite;
	size_t writeSeeks = _rfdiv(writeSeg,writeShare);
	return ConsolidationStats(readSeeks,readSeg, writeSeg,writeSeeks);
}

ConsolidationStats kWayConsolidateAux(std::vector<size_t> &segments, size_t lastInMem /*0 or 1*/, size_t memBufferPostings) {
	KWaySegmentConsolidator kwsc(segments,lastInMem,memBufferPostings);
	return kwsc();
}

void WriteBackBuffer::rewindSeeksAndPostingWrites(size_t seeks, size_t posts){
	totalSeeks -= seeks;
	bytesWritten -= PostingSize::bytesOfPostings(posts);
}

void  WriteBackBuffer::registerPostingReads(size_t postings, size_t seeks) {
	totalSeeks += seeks;
	bytesWritten += PostingSize::bytesOfPostings(postings);
}
void WriteBackBuffer::push_back(size_t postings) {
	//jobQ.push_back(std::make_pair(tid,postings));
	//lex.add(tid,postings);
	++totalSeeks;
//	COUTL << bytesWritten << " += " << postings<<ENDL;
	bytesWritten += PostingSize::bytesOfPostings(postings);
}


/*
void WriteBackBuffer::push_bucket(const std::vector<std::pair<tid_t,did_t> >& batch) {
	uint64_t sumP = 0;
	for(auto p : batch) {
		lex.add(p.first,p.second);
		sumP += p.second;
	}

	++totalSeeks;
	bytesWritten += (sumP * szOfPostingBytes);
}
*/
std::ostream& operator <<(std::ostream& stream, const IOStats& ws) {
	stream <<STATMRK << " "	<<
			ws.label << "-Bytes: " << ws.RW << " "<<
			ws.label << "-Seeks: " << ws.seek << " "<<
			ws.label << "-Total-time-minutes: " << ws.ioTimeInMinutes()<< ENDL;
	return stream;
}

void auxRebuildCPrice(std::vector<double> &consolidationPriceVector, const std::vector<size_t>& sizeStack, size_t lastOnDisk /*0 or 1*/, size_t /*withpb - 0 or 1*/) {
	const int sz = sizeStack.size();
	CHEAP_ASSERT(lastOnDisk<2 && sz, "wrong args!")

	consolidationPriceVector.clear();
	consolidationPriceVector.reserve(sz);
	for(auto it = sizeStack.begin(); it != sizeStack.end()-1; ++it) {
		auto cons = kWayConsolidate(it,sizeStack.end(),1-lastOnDisk);
		consolidationPriceVector.push_back(ConsTime::consolidationDataToMinutes(cons));
	}
	consolidationPriceVector.push_back(ConsTime::consolidationDataToMinutes(ConsolidationStats(0,0,sizeStack.back(),1) )); //no real consolidation - just a write-back of the last one
}

double ConsTime::consolidationDataToMinutes(const ConsolidationStats& cd) {
	return IOStats(cd.rdata + cd.wdata, cd.seeks(),-1).ioTimeInMinutes();
}

