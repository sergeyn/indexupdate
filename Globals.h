#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "Asserts.h"

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

        typedef unsigned tid_t;
        typedef unsigned did_t;
        typedef uint8_t unchar;


        #include <unordered_map>
        #define hashMapStd std::unordered_map

        #include <string>
        typedef std::string STRING;


//#define SHOWLINESALWAYS
#ifdef SHOWLINESALWAYS
        #define COUT std::cout << __FILE__ << ":" << __LINE__ << " "
        #define CERR std::cerr << __FILE__ << ":" << __LINE__ << " "
#else
        #define COUT std::cout
        #define CERR std::cerr
#endif

#define COMPILE_TIME_VERBOSITY 3


        #define COUTL std::cout << __FILE__ << ":" << __LINE__ << " "
#if COMPILE_TIME_VERBOSITY>0
        #define COUT1 if(globalVerbosity>=1)COUT << " <1> "
#else
		#define COUT1 if(0) COUT
#endif
#if COMPILE_TIME_VERBOSITY>1
        #define COUT2 if(globalVerbosity>=2)COUT << " <2> "
#else
		#define COUT2 if(0) COUT
#endif
#if COMPILE_TIME_VERBOSITY>2
        #define COUT3 if(globalVerbosity>=3)COUT << " <3> "
#else
		#define COUT3 if(0) COUT
#endif
#if COMPILE_TIME_VERBOSITY>3
        #define COUT4 if(globalVerbosity>=4)COUT << " <4> "
#else
		#define COUT4 if(0) COUT
#endif
#if COMPILE_TIME_VERBOSITY>4
        #define COUTV(v) if(globalVerbosity>=v)COUT << " <" << v << "> "
		#define CERRV(v) if(globalVerbosity>=v)CERR << " <" << v << "> "
#else
		#define COUTV(v) if(0) COUT
		#define CERRV(v) if(0) COUT
#endif

#ifdef DEBUG
        #define COUTD COUTL
#else
        #define COUTD if(0) COUT
#endif
        #define LOG logFile
		#define HERE COUTL << ENDL;
		#define THROW(stuff) abort();
        const STRING redStart("\033[1;31m");
        const STRING greenStart("\033[1;32m");
        const STRING magentaStart("\033[1;33m");
        const STRING colorEnd("\033[0m");
		const char   ENDL = '\n';
		#define CENDL colorEnd << std::endl
		const std::string STATMRK("$$$");

        #define STRINGSTREAM std::stringstream
		const char SLASHN = '\n';

        template <class B> STRING toString(const B& d) { STRINGSTREAM s; s << d; return s.str(); }

        bool isNumber(const STRING& str); //this is terrible and silly!

        extern unsigned globalVerbosity;

        //does int. division and taking correct upper
        inline size_t _rfdiv(size_t a, size_t share) {
        	return /*(a<share) ? 1 : */(a/share + size_t(a%share != 0));
        }

template<typename T>
struct int_iterator :  public std::iterator<std::random_access_iterator_tag, T>{
		T value;
		public:
		int_iterator(const T& v) :value(v) {static_assert(std::is_integral<T>::value,"only works with integrals");}
		int_iterator(const int_iterator& mit) : value(mit.value) {}
		int_iterator& operator++() {++value; return *this;}
		int_iterator operator++(int) {int_iterator tmp(*this); operator++(); return tmp;}
		bool operator==(const int_iterator& rhs) {return value==rhs.value;}
		bool operator!=(const int_iterator& rhs) {return value!=rhs.value;}
		T operator*() {return value;}
};

template<typename T>
size_t operator-(int_iterator<T> smaller, int_iterator<T> larger) { return 1 + larger.value - smaller.value; }

template<typename iT>
struct range_wrap {
	iT b; iT e;
	range_wrap(const iT& _b, const iT& _e) : b(_b),e(_e){}
	iT begin() const { return b; }
	iT end() const { return e; }
};

template<typename iT>
range_wrap<iT> make_range(const iT& begin, const iT& end) { return range_wrap<iT>(begin,end); }

template<typename T>
range_wrap<int_iterator<T>> make_irange(const T& _e) { return make_range(int_iterator<T>(0),int_iterator<T>(_e)); }
template<typename T, typename Y>
range_wrap<int_iterator<T> > make_irange(const T& _b, const Y& _e) { return make_range(int_iterator<T>(_b),int_iterator<T>(_e)); }

#define NTIMES(k) for(unsigned _d__i = 0; _d__i<(k); ++_d__i)
template<typename T> constexpr const T &as_const(T &t) noexcept { return t; }
#endif /* GLOBALS_H_ */
