#ifndef CASTEASSIGNER_H_
#define CASTEASSIGNER_H_

#include "Globals.h"
#include "Caste.h"
#include "Interfaces.h"
#include "Types.h"
#include "IO.h"

//currently operates on termIds not casteIds
class CasteManager : public listAttributesProvider, public listAttributesUpdater {
protected:
	friend class CasteStats;
	typedef std::vector<caste*> caste_c_t;

	inline caste* getCaste(size_t tid) { return casteVec[tidToCid[tid]]; }

	caste_c_t casteVec;
	std::vector<caste_t> tidToCid;
	std::unordered_map<tid_t,uint8_t> longTailTerms; //operates on termIds not casteIds singletons;
	listAttributesProvider& attProvider;
	caste promotedSingletons;
	float aggressive; 

	void putBack(caste* cst);
	void promoteSingleton(tid_t tid);

public:
	explicit CasteManager(listAttributesProvider& attP);
	~CasteManager();
	void resestAll();
	void printHists();

	void increaseWithCheck(const size_t id, unsigned by = 1);
	void increase(const size_t id, unsigned by = 1) {  casteVec[tidToCid[id]]->updateReg(by); }

	void loadCastesFromAttMatrix(const std::vector<listAttWithCasteId>& input);
	void loadCastesFromFile(const std::string& fname);
	void add(size_t id, const caste& nd);
	void addPtr(size_t id, caste* cst);
	template<typename It>
	void registerMembersToCaste(It first, It last, caste_t id) {
		//they are not yet sorted (probably)
		auto maxT = *std::max_element(first,last);
		if(maxT>=tidToCid.size())
			tidToCid.resize(maxT+1024,0);
		for(; first != last; ++first)
			tidToCid[*first] = id;
	}

	void reset(caste_t cid);
	caste& getByCID(caste_t cid) { return *casteVec[cid]; }
	const caste& operator [](size_t id) const;
	caste& operator [](size_t id);
	const std::vector<caste_t>& getTidToCidMap() const { return tidToCid; }
	inline size_t size() const { return casteVec.size(); } //how many castes there are
	inline bool hasCaste(tid_t termId) const { return termId<tidToCid.size() && tidToCid[termId]>0; }
	void addLongTailTerm(tid_t termId) { longTailTerms[termId] += 1; }


	caste_c_t::const_iterator begin() const { return casteVec.cbegin()+1; }//skip first
	caste_c_t::const_iterator end() const{ return casteVec.cend(); }
	const caste& back() const { return *casteVec.back(); }
	caste_c_t::iterator begin() { return casteVec.begin()+1; }//skip first
	caste_c_t::iterator end() { return casteVec.end(); }

	std::vector<caste*> getSortedCopy() const; //make best eviction candidates to be in the end
	std::vector<caste*> getSortedCopy2() const; //make best eviction candidates to be in the end
	template<typename Predicate>
	std::vector<caste*> getSortedCopyP(Predicate p) const; //make best eviction candidates to be in the end

	std::vector<size_t> getSortedEpochSizes() const;
	std::vector<size_t> getSortedUBSizes() const;

	size_t pushLongTailTerms(bool force=false);
	
	void setAggressive(unsigned ag) { aggressive = ag; }
	float getAggressive() const { return aggressive; }
	static void splitInPlaceFromFileToCout(const std::string& fname, unsigned threshold=1024*1024);

	//make caste manager disguise itself as a provider/updater of listAtt
	//convention:
	//flags(tid) returns #segments of the term!
	//length(tid) returns the length of this term
	//offset(tid) returns the length of entire caste
	//freq(tid) returns the #segments for caste
	size_t    offset(tid_t id) const { return casteVec[tidToCid[id]]->totalPostings;  }
	size_t    length(tid_t id) const {return attProvider.length(id);} //delegate to global lex...
//	flagsData flags(tid_t id) const {flagsData f = { (*(*handles[tidToCid[id]])).segmentsCount }; return f;}
	flagsData flags(tid_t id) const {return attProvider.flags(id);} //delegate to global lex...
	size_t    freq(tid_t id) const {return casteVec[tidToCid[id]]->size();}
	const void* getRaw(tid_t ) const {return nullptr;}
	void orFlag(tid_t ,const flagsData& ){}
	void addLength(tid_t, did_t){}
	virtual unchar   flagBitsSet(tid_t ) const { FATAL("bubu"); return 0;}

};

class CasteStats {
	const CasteManager& csm;
public:
	CasteStats(const CasteManager& l);
	void print(std::ostream& stream, bool printAll=true)const;
	void printSegmentsCount(std::ostream& stream)const;
	void printHist(std::ostream& stream)const;
};
std::ostream& operator <<(std::ostream& stream, const CasteStats& cst);

//=============================================================================================================
//input: <ids,length,freq> and a min threshold (perhaps scaled)
//result: mapping id to caste
class CasteSplitter {
public:
	explicit CasteSplitter(size_t minT, size_t factor=24, unsigned vertDiv=16);

	unsigned splitInPlace(std::vector<listAttWithCasteId>& input); //note -- input is going to be altered
	//void splitInPlaceFromFileToCout(const std::string& fname);
	void fourWaySplitInPlace(std::vector<listAttWithCasteId>& input, caste_t runningCasteId);
	//template<typename Iterator>
	//void splitACopy(Iterator begin, Iterator end, std::vector<listAttWithCasteId>& result, size_t reserve=0);
	//template<typename Iterator>
	//void fourWaySplitACopy(Iterator begin, Iterator end, std::vector<listAttWithCasteId>& result, caste_t runningCasteId, size_t reserve=0);

private:
	unsigned minThreshold;
	const unsigned verticalDiv;
	const size_t factor;
};
//=============================================================================================================

#include "CasteSystemIMP.h"

#endif /* CASTEASSIGNER_H_ */
