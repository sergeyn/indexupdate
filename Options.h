/* #This file was generated automatically! Do not edit */
#ifndef _WEST_OPTIONS_
#define _WEST_OPTIONS_

#include "Globals.h"

struct OptionRanges
{
	size_t VERBOSITY_MINVAL, VERBOSITY_MAXVAL;
	size_t IMBUE_MINVAL, IMBUE_MAXVAL;
	size_t PRINTSTATSFREQ_MINVAL, PRINTSTATSFREQ_MAXVAL;
	size_t CYCLES_MINVAL, CYCLES_MAXVAL;
	size_t FREEPCT_MINVAL, FREEPCT_MAXVAL;
	size_t AGGRESSIVE_MINVAL, AGGRESSIVE_MAXVAL;
	size_t MONOLITHICMERGE_MINVAL, MONOLITHICMERGE_MAXVAL;
	size_t UPSRATE_MINVAL, UPSRATE_MAXVAL;
	size_t QUERYRATE_MINVAL, QUERYRATE_MAXVAL;
	size_t IS_RANDOM_DEL_MINVAL, IS_RANDOM_DEL_MAXVAL;
	size_t IS_SKI_MINVAL, IS_SKI_MAXVAL;
	size_t LOADPROXIES_MINVAL, LOADPROXIES_MAXVAL;
	size_t STABLECAPACITY_MINVAL, STABLECAPACITY_MAXVAL;
	size_t POSTINGS_MINVAL, POSTINGS_MAXVAL;
	size_t TOTALMEM_MINVAL, TOTALMEM_MAXVAL;
	size_t TCPERCENT_MINVAL, TCPERCENT_MAXVAL;
	size_t UBPERCENT_MINVAL, UBPERCENT_MAXVAL;
	size_t CASTESCALE_MINVAL, CASTESCALE_MAXVAL;
	size_t FORCEGEOMERGEINCASTES_MINVAL, FORCEGEOMERGEINCASTES_MAXVAL;
	STRING AUGHELPER_MINVAL, AUGHELPER_MAXVAL;
	STRING PREDICTIONS_MINVAL, PREDICTIONS_MAXVAL;
	STRING EVICTOR_MINVAL, EVICTOR_MAXVAL;
	STRING QPROC_MINVAL, QPROC_MAXVAL;
	STRING JUMPSTART_MINVAL, JUMPSTART_MAXVAL;
	unsigned countAllOpts;
	OptionRanges() : 
		VERBOSITY_MINVAL(0), VERBOSITY_MAXVAL(10), //size_t
		IMBUE_MINVAL(0), IMBUE_MAXVAL(1), //size_t
		PRINTSTATSFREQ_MINVAL(1), PRINTSTATSFREQ_MAXVAL(2147483648), //size_t
		CYCLES_MINVAL(0), CYCLES_MAXVAL(1024000), //size_t
		FREEPCT_MINVAL(1), FREEPCT_MAXVAL(99), //size_t
		AGGRESSIVE_MINVAL(1), AGGRESSIVE_MAXVAL((1<<30)), //size_t
		MONOLITHICMERGE_MINVAL(0), MONOLITHICMERGE_MAXVAL(3), //size_t
		UPSRATE_MINVAL(0), UPSRATE_MAXVAL(1024000), //size_t
		QUERYRATE_MINVAL(0), QUERYRATE_MAXVAL(1024000), //size_t
		IS_RANDOM_DEL_MINVAL(0), IS_RANDOM_DEL_MAXVAL(1), //size_t
		IS_SKI_MINVAL(0), IS_SKI_MAXVAL(1), //size_t
		LOADPROXIES_MINVAL(0), LOADPROXIES_MAXVAL(1), //size_t
		STABLECAPACITY_MINVAL(1), STABLECAPACITY_MAXVAL(2147483648), //size_t
		POSTINGS_MINVAL(0), POSTINGS_MAXVAL(17075485964*2), //size_t
		TOTALMEM_MINVAL(0), TOTALMEM_MAXVAL(17075485964*2), //size_t
		TCPERCENT_MINVAL(0), TCPERCENT_MAXVAL(111), //size_t
		UBPERCENT_MINVAL(0), UBPERCENT_MAXVAL(111), //size_t
		CASTESCALE_MINVAL(1), CASTESCALE_MAXVAL(1000), //size_t
		FORCEGEOMERGEINCASTES_MINVAL(0), FORCEGEOMERGEINCASTES_MAXVAL(1), //size_t
		AUGHELPER_MINVAL(), AUGHELPER_MAXVAL(), //STRING
		PREDICTIONS_MINVAL(), PREDICTIONS_MAXVAL(), //STRING
		EVICTOR_MINVAL(), EVICTOR_MAXVAL(), //STRING
		QPROC_MINVAL(), QPROC_MAXVAL(), //STRING
		JUMPSTART_MINVAL(), JUMPSTART_MAXVAL(), //STRING
		countAllOpts(24)
	{}
};

struct Options
{
	
	size_t VERBOSITY;
	size_t IMBUE;
	size_t PRINTSTATSFREQ;
	size_t CYCLES;
	size_t FREEPCT;
	size_t AGGRESSIVE;
	size_t MONOLITHICMERGE;
	size_t UPSRATE;
	size_t QUERYRATE;
	size_t IS_RANDOM_DEL;
	size_t IS_SKI;
	size_t LOADPROXIES;
	size_t STABLECAPACITY;
	size_t POSTINGS;
	size_t TOTALMEM;
	size_t TCPERCENT;
	size_t UBPERCENT;
	size_t CASTESCALE;
	size_t FORCEGEOMERGEINCASTES;
	STRING AUGHELPER;
	STRING PREDICTIONS;
	STRING EVICTOR;
	STRING QPROC;
	STRING JUMPSTART;

	unsigned countAllOpts;
	OptionRanges	valueRanges;

	Options() : 
		VERBOSITY(3), //size_t
		IMBUE(1), //size_t
		PRINTSTATSFREQ(340000000), //size_t
		CYCLES(10241024), //size_t
		FREEPCT(10), //size_t
		AGGRESSIVE(1), //size_t
		MONOLITHICMERGE(2), //size_t
		UPSRATE(256), //size_t
		QUERYRATE(1), //size_t
		IS_RANDOM_DEL(1), //size_t
		IS_SKI(1), //size_t
		LOADPROXIES(0), //size_t
		STABLECAPACITY(0), //size_t
		POSTINGS(5440000000), //size_t
		TOTALMEM(1000000000), //size_t
		TCPERCENT(50), //size_t
		UBPERCENT(50), //size_t
		CASTESCALE(4), //size_t
		FORCEGEOMERGEINCASTES(0), //size_t
		AUGHELPER("AugmentsQuantBlockMaxSSE<float>"), //STRING
		PREDICTIONS("../cluewebId_Len_InterpolatedQueryFreq"), //STRING
		EVICTOR("CasteEvictor"), //STRING
		QPROC("QpNoWriteBack"), //STRING
		JUMPSTART(""), //STRING
		countAllOpts(24)
	{}

	unsigned signatureF(const unsigned& value)	{ return value; }
	unsigned signatureF(const unsigned long& value)	{ return value; }
	unsigned signatureF(const float& value)		{ return unsigned(1000000*value); }
	unsigned signatureF(const double& value)	{ return unsigned(1000000*value); }
	unsigned signatureF(const STRING& value)	{ return value.size(); }

	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, STRING& target)	{ if(argName == name) { target=argVal; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, unsigned& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, unsigned long& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, double& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; } 
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, float& target)		{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }

	bool parseArg(const STRING& argName, const STRING& argVal)
	{
		if (convertArg(argName, argVal, STRING("-verbosity"), VERBOSITY)) return true;
		if (convertArg(argName, argVal, STRING("-imbue"), IMBUE)) return true;
		if (convertArg(argName, argVal, STRING("-printstatsfreq"), PRINTSTATSFREQ)) return true;
		if (convertArg(argName, argVal, STRING("-cycles"), CYCLES)) return true;
		if (convertArg(argName, argVal, STRING("-freepct"), FREEPCT)) return true;
		if (convertArg(argName, argVal, STRING("-aggressive"), AGGRESSIVE)) return true;
		if (convertArg(argName, argVal, STRING("-monolithicmerge"), MONOLITHICMERGE)) return true;
		if (convertArg(argName, argVal, STRING("-upsrate"), UPSRATE)) return true;
		if (convertArg(argName, argVal, STRING("-queryrate"), QUERYRATE)) return true;
		if (convertArg(argName, argVal, STRING("-is_random_del"), IS_RANDOM_DEL)) return true;
		if (convertArg(argName, argVal, STRING("-is_ski"), IS_SKI)) return true;
		if (convertArg(argName, argVal, STRING("-loadproxies"), LOADPROXIES)) return true;
		if (convertArg(argName, argVal, STRING("-stablecapacity"), STABLECAPACITY)) return true;
		if (convertArg(argName, argVal, STRING("-postings"), POSTINGS)) return true;
		if (convertArg(argName, argVal, STRING("-totalmem"), TOTALMEM)) return true;
		if (convertArg(argName, argVal, STRING("-tcpercent"), TCPERCENT)) return true;
		if (convertArg(argName, argVal, STRING("-ubpercent"), UBPERCENT)) return true;
		if (convertArg(argName, argVal, STRING("-castescale"), CASTESCALE)) return true;
		if (convertArg(argName, argVal, STRING("-forcegeomergeincastes"), FORCEGEOMERGEINCASTES)) return true;
		if (convertArg(argName, argVal, STRING("-aughelper"), AUGHELPER)) return true;
		if (convertArg(argName, argVal, STRING("-predictions"), PREDICTIONS)) return true;
		if (convertArg(argName, argVal, STRING("-evictor"), EVICTOR)) return true;
		if (convertArg(argName, argVal, STRING("-qproc"), QPROC)) return true;
		if (convertArg(argName, argVal, STRING("-jumpstart"), JUMPSTART)) return true;
		return false;
	}

	STRING getVal(const STRING& optName)
	{
		if (optName == STRING("-verbosity")) return toString(VERBOSITY);
		if (optName == STRING("-imbue")) return toString(IMBUE);
		if (optName == STRING("-printstatsfreq")) return toString(PRINTSTATSFREQ);
		if (optName == STRING("-cycles")) return toString(CYCLES);
		if (optName == STRING("-freepct")) return toString(FREEPCT);
		if (optName == STRING("-aggressive")) return toString(AGGRESSIVE);
		if (optName == STRING("-monolithicmerge")) return toString(MONOLITHICMERGE);
		if (optName == STRING("-upsrate")) return toString(UPSRATE);
		if (optName == STRING("-queryrate")) return toString(QUERYRATE);
		if (optName == STRING("-is_random_del")) return toString(IS_RANDOM_DEL);
		if (optName == STRING("-is_ski")) return toString(IS_SKI);
		if (optName == STRING("-loadproxies")) return toString(LOADPROXIES);
		if (optName == STRING("-stablecapacity")) return toString(STABLECAPACITY);
		if (optName == STRING("-postings")) return toString(POSTINGS);
		if (optName == STRING("-totalmem")) return toString(TOTALMEM);
		if (optName == STRING("-tcpercent")) return toString(TCPERCENT);
		if (optName == STRING("-ubpercent")) return toString(UBPERCENT);
		if (optName == STRING("-castescale")) return toString(CASTESCALE);
		if (optName == STRING("-forcegeomergeincastes")) return toString(FORCEGEOMERGEINCASTES);
		if (optName == STRING("-aughelper")) return toString(AUGHELPER);
		if (optName == STRING("-predictions")) return toString(PREDICTIONS);
		if (optName == STRING("-evictor")) return toString(EVICTOR);
		if (optName == STRING("-qproc")) return toString(QPROC);
		if (optName == STRING("-jumpstart")) return toString(JUMPSTART);
		CERR << "Unknown option: " << optName << ENDL; exit(1);
		return "";
	}

	void parseFromCmdLine(int argc, char** argv, const int id=-99999)
	{
		const STRING idSuffix(":"+ toString(id));

		for (int i=0; i<argc; ++i)
		{
			STRING argvi(argv[i]);
			if (argvi[0] != '-')
			{
				CERR << "Unexpected argument: " << argvi << ENDL; goto _PrintUsage; 
			}
			else if (argc <= i+1)
			{
				CERR << "Value missing for argument: " << argvi << ENDL; goto _PrintUsage;
			}
			// for all other args, the val should be numeric
			//else if (!isNumber(argv[i+1]))
			//{
			//	CERR << "Unexpected value " << argv[i+1] << " for argument: " << argv[i] << ENDL; goto _PrintUsage;
			//}
			else if (argvi.find(':') != STRING::npos)  // id specific opt
			{
				size_t s = argvi.find(idSuffix);
				if(s == STRING::npos) { ++i; continue; } // not of this id
				argvi.erase(s,argvi.size()-s);
				if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; }
			}
			else if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; } // regular opt

			CERR << "Unknown option: " << argv[i] << ENDL;
			
			_PrintUsage:
			CERR << "Usage:" << ENDL
			 << "	-verbosity <val=3> // " << "The verbosity level (higher is more)" << ENDL
			 << "	-imbue <val=1> // " << "if 1 will print all numbers with thousands separator" << ENDL
			 << "	-printstatsfreq <val=340000000> // " << "print stats every that much seens postings" << ENDL
			 << "	-cycles <val=10241024> // " << "simulation cycles\n\n\t=== framework sizes and flags ===" << ENDL
			 << "	-freepct <val=10> // " << "free this much percents of UB" << ENDL
			 << "	-aggressive <val=1> // " << "how aggressive the merging should be. higher -- less merges" << ENDL
			 << "	-monolithicmerge <val=2> // " << "1: merge always, 2:merge Log, 0: never merge" << ENDL
			 << "	-upsrate <val=256> // " << "updates rate" << ENDL
			 << "	-queryrate <val=1> // " << "so many queries for every *PSTRATE* updates\n\n\t=== string values (algs) ===" << ENDL
			 << "	-is_random_del <val=1> // " << "if there are del.s are they random or chrono?" << ENDL
			 << "	-is_ski <val=1> // " << "is ski rental or thinkful?" << ENDL
			 << "	-loadproxies <val=0> // " << "if loading jumpstart should we load proxies?" << ENDL
			 << "	-stablecapacity <val=0> // " << "the size of stable case in postings. default 0 for growing to inf" << ENDL
			 << "	-postings <val=5440000000> // " << "simulation postings" << ENDL
			 << "	-totalmem <val=1000000000> // " << "memory budget (in postings)" << ENDL
			 << "	-tcpercent <val=50> // " << "TC%" << ENDL
			 << "	-ubpercent <val=50> // " << "UB%" << ENDL
			 << "	-castescale <val=4> // " << "larger values, more castes" << ENDL
			 << "	-forcegeomergeincastes <val=0> // " << "when considering consolidations, should be strictly geometric?" << ENDL
			 << "	-aughelper <val=AugmentsQuantBlockMaxSSE<float>> // " << "the class of augments processor to use" << ENDL
			 << "	-predictions <val=../cluewebId_Len_InterpolatedQueryFreq> // " << "where the updates/queries prediction is" << ENDL
			 << "	-evictor <val=CasteEvictor> // " << "which evictor to apply" << ENDL
			 << "	-qproc <val=QpNoWriteBack> // " << "Query processing method" << ENDL
			 << "	-jumpstart <val=> // " << "Jumpstart to this file" << ENDL
			<< ENDL;

			exit(1);
		}
	}

	bool testAllOptions() const 
	{
		if (VERBOSITY < valueRanges.VERBOSITY_MINVAL || VERBOSITY > valueRanges.VERBOSITY_MAXVAL) { CERR<<  " VERBOSITY is out of range: [0,10]" << ENDL; return false;}
		if (IMBUE < valueRanges.IMBUE_MINVAL || IMBUE > valueRanges.IMBUE_MAXVAL) { CERR<<  " IMBUE is out of range: [0,1]" << ENDL; return false;}
		if (PRINTSTATSFREQ < valueRanges.PRINTSTATSFREQ_MINVAL || PRINTSTATSFREQ > valueRanges.PRINTSTATSFREQ_MAXVAL) { CERR<<  " PRINTSTATSFREQ is out of range: [1,2147483648]" << ENDL; return false;}
		if (CYCLES < valueRanges.CYCLES_MINVAL || CYCLES > valueRanges.CYCLES_MAXVAL) { CERR<<  " CYCLES is out of range: [0,1024000]" << ENDL; return false;}
		if (FREEPCT < valueRanges.FREEPCT_MINVAL || FREEPCT > valueRanges.FREEPCT_MAXVAL) { CERR<<  " FREEPCT is out of range: [1,99]" << ENDL; return false;}
		if (AGGRESSIVE < valueRanges.AGGRESSIVE_MINVAL || AGGRESSIVE > valueRanges.AGGRESSIVE_MAXVAL) { CERR<<  " AGGRESSIVE is out of range: [1,(1<<30)]" << ENDL; return false;}
		if (MONOLITHICMERGE < valueRanges.MONOLITHICMERGE_MINVAL || MONOLITHICMERGE > valueRanges.MONOLITHICMERGE_MAXVAL) { CERR<<  " MONOLITHICMERGE is out of range: [0,3]" << ENDL; return false;}
		if (UPSRATE < valueRanges.UPSRATE_MINVAL || UPSRATE > valueRanges.UPSRATE_MAXVAL) { CERR<<  " UPSRATE is out of range: [0,1024000]" << ENDL; return false;}
		if (QUERYRATE < valueRanges.QUERYRATE_MINVAL || QUERYRATE > valueRanges.QUERYRATE_MAXVAL) { CERR<<  " QUERYRATE is out of range: [0,1024000]" << ENDL; return false;}
		if (IS_RANDOM_DEL < valueRanges.IS_RANDOM_DEL_MINVAL || IS_RANDOM_DEL > valueRanges.IS_RANDOM_DEL_MAXVAL) { CERR<<  " IS_RANDOM_DEL is out of range: [0,1]" << ENDL; return false;}
		if (IS_SKI < valueRanges.IS_SKI_MINVAL || IS_SKI > valueRanges.IS_SKI_MAXVAL) { CERR<<  " IS_SKI is out of range: [0,1]" << ENDL; return false;}
		if (LOADPROXIES < valueRanges.LOADPROXIES_MINVAL || LOADPROXIES > valueRanges.LOADPROXIES_MAXVAL) { CERR<<  " LOADPROXIES is out of range: [0,1]" << ENDL; return false;}
		if (STABLECAPACITY < valueRanges.STABLECAPACITY_MINVAL || STABLECAPACITY > valueRanges.STABLECAPACITY_MAXVAL) { CERR<<  " STABLECAPACITY is out of range: [1,2147483648]" << ENDL; return false;}
		if (POSTINGS < valueRanges.POSTINGS_MINVAL || POSTINGS > valueRanges.POSTINGS_MAXVAL) { CERR<<  " POSTINGS is out of range: [0,17075485964*2]" << ENDL; return false;}
		if (TOTALMEM < valueRanges.TOTALMEM_MINVAL || TOTALMEM > valueRanges.TOTALMEM_MAXVAL) { CERR<<  " TOTALMEM is out of range: [0,17075485964*2]" << ENDL; return false;}
		if (TCPERCENT < valueRanges.TCPERCENT_MINVAL || TCPERCENT > valueRanges.TCPERCENT_MAXVAL) { CERR<<  " TCPERCENT is out of range: [0,111]" << ENDL; return false;}
		if (UBPERCENT < valueRanges.UBPERCENT_MINVAL || UBPERCENT > valueRanges.UBPERCENT_MAXVAL) { CERR<<  " UBPERCENT is out of range: [0,111]" << ENDL; return false;}
		if (CASTESCALE < valueRanges.CASTESCALE_MINVAL || CASTESCALE > valueRanges.CASTESCALE_MAXVAL) { CERR<<  " CASTESCALE is out of range: [1,1000]" << ENDL; return false;}
		if (FORCEGEOMERGEINCASTES < valueRanges.FORCEGEOMERGEINCASTES_MINVAL || FORCEGEOMERGEINCASTES > valueRanges.FORCEGEOMERGEINCASTES_MAXVAL) { CERR<<  " FORCEGEOMERGEINCASTES is out of range: [0,1]" << ENDL; return false;}
		if (AUGHELPER < valueRanges.AUGHELPER_MINVAL || AUGHELPER > valueRanges.AUGHELPER_MAXVAL) { CERR<<  " AUGHELPER is out of range: [,]" << ENDL; return false;}
		if (PREDICTIONS < valueRanges.PREDICTIONS_MINVAL || PREDICTIONS > valueRanges.PREDICTIONS_MAXVAL) { CERR<<  " PREDICTIONS is out of range: [,]" << ENDL; return false;}
		if (EVICTOR < valueRanges.EVICTOR_MINVAL || EVICTOR > valueRanges.EVICTOR_MAXVAL) { CERR<<  " EVICTOR is out of range: [,]" << ENDL; return false;}
		if (QPROC < valueRanges.QPROC_MINVAL || QPROC > valueRanges.QPROC_MAXVAL) { CERR<<  " QPROC is out of range: [,]" << ENDL; return false;}
		if (JUMPSTART < valueRanges.JUMPSTART_MINVAL || JUMPSTART > valueRanges.JUMPSTART_MAXVAL) { CERR<<  " JUMPSTART is out of range: [,]" << ENDL; return false;}
		return true;
	}

	template<class B> void print(B& pOut)
	{
		pOut << "----------------------------" << SLASHN;
		#define PRINT_OPT(external_name, internal_name) { pOut << external_name  << " = " << toString(internal_name) << SLASHN; }
		PRINT_OPT("-verbosity", VERBOSITY);
		PRINT_OPT("-imbue", IMBUE);
		PRINT_OPT("-printstatsfreq", PRINTSTATSFREQ);
		PRINT_OPT("-cycles", CYCLES);
		PRINT_OPT("-freepct", FREEPCT);
		PRINT_OPT("-aggressive", AGGRESSIVE);
		PRINT_OPT("-monolithicmerge", MONOLITHICMERGE);
		PRINT_OPT("-upsrate", UPSRATE);
		PRINT_OPT("-queryrate", QUERYRATE);
		PRINT_OPT("-is_random_del", IS_RANDOM_DEL);
		PRINT_OPT("-is_ski", IS_SKI);
		PRINT_OPT("-loadproxies", LOADPROXIES);
		PRINT_OPT("-stablecapacity", STABLECAPACITY);
		PRINT_OPT("-postings", POSTINGS);
		PRINT_OPT("-totalmem", TOTALMEM);
		PRINT_OPT("-tcpercent", TCPERCENT);
		PRINT_OPT("-ubpercent", UBPERCENT);
		PRINT_OPT("-castescale", CASTESCALE);
		PRINT_OPT("-forcegeomergeincastes", FORCEGEOMERGEINCASTES);
		PRINT_OPT("-aughelper", AUGHELPER);
		PRINT_OPT("-predictions", PREDICTIONS);
		PRINT_OPT("-evictor", EVICTOR);
		PRINT_OPT("-qproc", QPROC);
		PRINT_OPT("-jumpstart", JUMPSTART);
		pOut << "----------------------------" << ENDL;
	}
	
	template<class B> void printCSV(B& pOut)
	{
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	unsigned signature()
	{
		unsigned res = 0;
		res ^= (unsigned(rand()) * signatureF(VERBOSITY));
		res ^= (unsigned(rand()) * signatureF(IMBUE));
		res ^= (unsigned(rand()) * signatureF(PRINTSTATSFREQ));
		res ^= (unsigned(rand()) * signatureF(CYCLES));
		res ^= (unsigned(rand()) * signatureF(FREEPCT));
		res ^= (unsigned(rand()) * signatureF(AGGRESSIVE));
		res ^= (unsigned(rand()) * signatureF(MONOLITHICMERGE));
		res ^= (unsigned(rand()) * signatureF(UPSRATE));
		res ^= (unsigned(rand()) * signatureF(QUERYRATE));
		res ^= (unsigned(rand()) * signatureF(IS_RANDOM_DEL));
		res ^= (unsigned(rand()) * signatureF(IS_SKI));
		res ^= (unsigned(rand()) * signatureF(LOADPROXIES));
		res ^= (unsigned(rand()) * signatureF(STABLECAPACITY));
		res ^= (unsigned(rand()) * signatureF(POSTINGS));
		res ^= (unsigned(rand()) * signatureF(TOTALMEM));
		res ^= (unsigned(rand()) * signatureF(TCPERCENT));
		res ^= (unsigned(rand()) * signatureF(UBPERCENT));
		res ^= (unsigned(rand()) * signatureF(CASTESCALE));
		res ^= (unsigned(rand()) * signatureF(FORCEGEOMERGEINCASTES));
		res ^= (unsigned(rand()) * signatureF(AUGHELPER));
		res ^= (unsigned(rand()) * signatureF(PREDICTIONS));
		res ^= (unsigned(rand()) * signatureF(EVICTOR));
		res ^= (unsigned(rand()) * signatureF(QPROC));
		res ^= (unsigned(rand()) * signatureF(JUMPSTART));
		return res;
	}
};

#endif
