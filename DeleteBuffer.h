/*
 * DeleteBuffer.h
 *
 *  Created on: Jul 29, 2015
 *      Author: sergeyn
 */

#ifndef DELETEBUFFER_H_
#define DELETEBUFFER_H_

#include "Types.h"
#include "Asserts.h"
#include "Globals.h"

/*
 *
 *  Will simply store info about deleted postings
 *  The Framework main loop will call populate every cycle -- chrono or random
 *  For chrono we keep a mini-lexicon for every term how much was deleted
 *  For castes we keep a per-caste deleted counter
 *
 *  In chrono we check if the most ancient segments were invalidated (before consolidation)
 *  In chrono for a full merge to single segment we reimburse for unneeded writes
 *  In rnd each segment gets a share of total caste deletes proportional to its size
 *
 */
class DistributionInterface;
class HistDeletes {
	DistributionInterface* deleteDist;
	std::vector<unsigned> chronoDeletesPerTerm;
	size_t deletedTids;
	std::vector<size_t> casteBuffers;
	std::vector<tid_t> tempTidBuffer;

public:
	explicit HistDeletes(DistributionInterface* delDist);

	//void deleteAllTids() { deletedTids = 0; }
	//size_t size() const { return deletedTids; } //note, isn't reduced by caste tracker!
	size_t castesCount() const { return casteBuffers.size(); }
	size_t termSize() const {return chronoDeletesPerTerm.size(); }

	inline size_t operator[](tid_t id) const {
		SAFE_ASSERT(id<chronoDeletesPerTerm.size(),"not resized enough! " + toString(id) + " vs. " + toString(chronoDeletesPerTerm.size()));
		return chronoDeletesPerTerm[id]; } //as long as we did our 'populates' it should be OK

	inline size_t casteDeletes(unsigned cid) const { return casteBuffers[cid]; }

	void populateChrono(size_t termsCount, size_t count, const std::vector<caste_t>* isCastes = nullptr);
	unsigned deleteChronoCasteTidsByStack(unsigned cid, const std::vector<size_t>& sizeStack);
	unsigned deleteChronoMonolithicTidsByStack(const std::vector<size_t>& sizeStack);
	void partialDeleteBeforeMerge(size_t& segmentSize);
	void partialDeleteBeforeMerge(unsigned cid,size_t& segmentSize);

	void populateRnd(size_t count, const std::vector<caste_t>* isCastes = nullptr);
	size_t deleteRndMonolithicTidsByStack(const std::vector<size_t>& sizeStack, unsigned startAt);
	size_t deleteRndCasteTidsByStack(unsigned cid, const std::vector<size_t>& sizeStack, unsigned startAt);



private:
	void auxPartial(size_t& deletes, size_t& segmentSize);
	void auxPopulate(size_t count);
	void updateLexiconForChrono(size_t termsCount);
	void updatePerCasteDeleteCounts(const std::vector<caste_t>* tid2caste);
};

#endif /* DELETEBUFFER_H_ */
