#!/usr/bin/env ruby
require 'json'
require 'set' 

TREESEP = '|'

#map names to options:
SINGLE_POSTING_BYTES = 4
POSTINGS = '-postings 64000000000'
UPSRATE = '-upsrate 1000000'
PRD = "-predictions newInterpolation_85x"
LOGSDIR = ""
opts = {
	'HD' => 'updHD',
	'SSD' => 'updSSD',
	'small mem -- 1gb' => "-totalmem #{(1<<30)/SINGLE_POSTING_BYTES}",	
	'high mem -- 8gb' => "-totalmem #{(1<<33)/SINGLE_POSTING_BYTES}",
	'evergrow' => '-stablecapacity 0',
	'chrono delete' => '-stablecapacity 32000000000 -is_random_del 0',
	'random delete' => '-stablecapacity 32000000000 -is_random_del 1',
	'always merge' => '-monolithicmerge 1  -qproc QpNoWriteBack',
	'log merge' => '-monolithicmerge 2  -qproc QpNoWriteBack',
	'castes, ski-rental' => '-qproc QpWithCastes -is_ski 1 -monolithicmerge 2 ',
	'castes, prognosticator' => '-qproc QpWithCastes -is_ski 0 -monolithicmerge 2 ',
	'high_q' => '-queryrate 16000',
	'medium_q' => '-queryrate 128',
	'low_q' => '-queryrate 1',
	"fix TC to 25% change UB %" => '-tcpercent 25 -ubpercent',
	"fix UB to 25% change TC %" => '-ubpercent 25 -tcpercent',
	"change UB %" => '-tcpercent 111 -ubpercent'
}

#./upd -updatebszp 536870912 -cachesz 1610612736 -monolithicmerge 1   -queryrate 128 -qproc QpNoWriteBack -is_ski 0 
def rec_flatten(dst,sub_tree,from_root = [])
	if sub_tree.is_a? Hash
		sub_tree.each { |k,v| 
			from_root << k
			rec_flatten(dst,v,from_root)
			from_root.pop
		}
	else
		dst.push [from_root.join(TREESEP),sub_tree]
	end
end


input_json = JSON.parse(IO.read(ARGV[0]))
filter = ARGV[1] ? ARGV[1].split(TREESEP).to_set : []
dst = []
rec_flatten(dst,input_json)
prefix = '/shared/home/snepomny/git/indexupdate/' #'~/blade.rb "ADD 5'
dst.each do |pr|
	arr = pr[0].split TREESEP
	fname = pr[0].gsub(TREESEP,"_").gsub(/\W+/, '')
	pr[1].each do |value| 
		next if arr.any? {|op| filter.include? op }
		s = arr.map { |k| opts[k] }
		s << value
		s << POSTINGS
		s << UPSRATE
		s << PRD
		#puts  "#{prefix} #{s.join ' '}| tee  #{LOGSDIR}/#{fname}_#{value}.raw | ./parseLogs.rb > #{LOGSDIR}/#{fname}_#{value}"
		puts  "#{prefix}#{s.join ' '} > #{LOGSDIR}#{fname}_#{value}.raw"
	end
end
