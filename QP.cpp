/*
 * QP.cpp
 *
 *  Created on: Mar 6, 2014
 *      Author: sergeyn
 */

#include "QP.h"
#include "ProxyIO.h" //for PostingSize::bytesOfPostings -- maybe move them?
#include "CasteSystem.h"
#include "Lexicon.h"
#include "URange.h"
#include "DeleteBuffer.h"

#include <unordered_map>

const std::string ProcessTerm::n__globalLex("globalLex");
const std::string ProcessTerm::n__lexUpd("lexUpd");
const std::string ProcessTerm::n__casteManager("casteManager");



QPBase::QPBase() :	 isSkiForce(false),isRandom(false), env(0), maxPostings(0),
		casteManager(nullptr),totalAsked(0),
		qsServedFromDisk(0),
		//cSizeInPostings(0),cCachedPostings(0),cacheHits(0),cachePostingsServed(0),cachePostingsMissed(0),
		deletedBuffer(nullptr)
{}

QPBase::~QPBase() {
}

void QPBase::setDeletesDistr(DistributionInterface* d) {
	deletedBuffer = new HistDeletes(d);
}

void  QPBase::populateChrono(size_t termsCount, size_t count, const std::vector<caste_t>* tid2cid) //if count == 0 we just resize!
{
	deletedBuffer->populateChrono(termsCount,count,tid2cid);
}
void  QPBase::populateRnd(size_t /*termsCount*/, size_t count, const std::vector<caste_t>* tid2cid) //populate deleted buffer
{
	deletedBuffer->populateRnd(count,tid2cid);
}


inline size_t QPBase::acknowledgeCrhonoShift(tid_t term) const { return (*deletedBuffer)[term]; }

void readDataFromSegments(DiskReader* simReader, unsigned segments, size_t postings) {
	simReader->readBytes(PostingSize::bytesOfPostings(postings));
	if (unlikely(segments==0)) //convention!
		simReader->rewindOneSeek();
	else
		simReader->readBytes(0,segments-1); //just for the seeks
}

void QPBase::setCacheSize(size_t szInPostings)
{
	cache.setMaxPostings(szInPostings);
//	if(cSizeInPostings) FATAL("can't resize cache in this implementation");
//	cSizeInPostings = szInPostings;
//	cachedLookup.reserve(10000);
}

bool QPBase::_visitCache(tid_t term, size_t length)
{
	recordQLens.push_back(length);
	recordTerms.push_back(term);
	return cache.visit(term,length);
//	auto it = cachedLookup.find(term);
//	if(it != cachedLookup.end()) {//cache hit?
//		cCachedPostings -= it->second->second; //update the length of the term
//		cCachedPostings += length;
//		it->second->second = length;
//
//		cachedQ.splice(cachedQ.begin(), cachedQ, it->second); //move to begin
//		++cacheHits;
//		cachePostingsServed += length;
//		return true;
//	}
//
//	//cache miss:
//	while(cCachedPostings+length>cSizeInPostings && !cachedQ.empty()) { //evict to accommodate with bound size policy
//		auto last = cachedQ.end(); //pick victims from end
//	    --last;
//	    cCachedPostings -= last->second;
//		cachedLookup.erase(last->first);
//		cachedQ.pop_back();
//	}
//
//	cachedQ.push_front(std::make_pair(term,length));
//	cachedLookup[term]=cachedQ.begin();
//	cCachedPostings += length;
//	cachePostingsMissed += length;
//	//COUTL << cachedQ.size() << " " << cCachedPostings  << ENDL;
//	return false;
}

QpNoWriteBack::QpNoWriteBack() : QPBase(),lexUpd(nullptr),globalLex(nullptr),queryReader(nullptr){}

QpURF::QpURF(): QPBase(),lexUpd(nullptr),globalLex(nullptr),queryReader(nullptr){}

void QpURF::setup(
		std::unordered_map<std::string,listAttributesUpdater*> updaters,
		std::unordered_map<std::string,listAttributesProvider*> providers,
		DiskReaderI& qrdr,
		DiskWriterI& ,
		DiskReaderI& ,
		DiskWriterI& ) {
	lexUpd=updaters[n__lexUpd];
	globalLex=providers[n__globalLex];
	casteManager=providers[n__casteManager];
	queryReader=(&qrdr);
}
size_t QpURF::operator()(tid_t tid) {
	++totalAsked;

	auto len = globalLex->length(tid);
	if(0==len)
		return 0;

	++qsServedFromDisk;
	//approx.
	unsigned seeks = len > URangeManager::listSzThresholdPostings ? 2 : 1; //posts for long lists are located in two places usually - rangeblock and termblock

	readDataFromSegments(reinterpret_cast<DiskReader*>(queryReader), seeks, len);
	return 0;
}


void auxPrint(const std::unordered_map<size_t,size_t>& segmentsHist) {
	if(globalVerbosity<6)
		return;
	size_t accSeeks = 0;
	size_t accQs = 0;
	COUT << "Segments\tcount\n";
	for(const std::pair<size_t,size_t>& p : segmentsHist ) {
		COUT << p.first << "\t" << p.second << "\n";
		accSeeks += p.first*p.second;
		accQs += p.second;
	}
	//TASSERT(accQs==total,"total != accQs");
	if(accQs)
		COUT << "avg. segments in a query: " << double(accSeeks)/double(accQs) << ENDL;
}

void QPBase::printHist() const {
	auxPrint(segmentsHist);
}


void QpNoWriteBack::setup (
		std::unordered_map<std::string,listAttributesUpdater*> updaters,
		std::unordered_map<std::string,listAttributesProvider*> providers,
		DiskReaderI& qrdr,
		DiskWriterI& ,
		DiskReaderI& ,
		DiskWriterI& ){

	lexUpd=updaters[n__lexUpd];
	globalLex=providers[n__globalLex];
	casteManager=providers[n__casteManager];
	queryReader=(&qrdr);
}

inline unsigned QpNoWriteBack::getSegments(tid_t tid) const {
	//if(isNeverMerge) return lexGet->flags(1); //old boo-boo convention
	return globalLex->flagBitsSet(tid);
}


size_t QpNoWriteBack::operator()(tid_t tid) {
	++totalAsked;

	//convention:
	//flags(tid) returns #segments
	//length(tid) returns the length of this term
	auto segments =  getSegments(tid);
#ifdef MNLTS
	
	if(0==segments)
		return 0;
	auto len = globalLex->length(tid);
	if(0==len)
		return 0;
	if(visitCache(tid,len))
		return 0;
#endif
#ifndef MNLTS
	if(0==segments) {
		visitCache(tid,0);
		return 0;
	}
	auto len = globalLex->length(tid);
	if(0==len) {
		visitCache(tid,len);
		return 0;
	}
	if(visitCache(tid,len))
		return 0;
#endif
	auto shiftPostings = (env == Env::CHRONO) ? this-> acknowledgeCrhonoShift(tid) : 0;
	if(len<shiftPostings) { //CHEAP_ASSERT(len>=shiftPostings,"removing more postings than we have: ")
		COUTL << len << '<' << shiftPostings << " for list: " << tid << " while all: " << deletedBuffer->termSize() << ENDL;
		FATAL("")
	}
	readDataFromSegments(reinterpret_cast<DiskReader*>(queryReader), segments,len-shiftPostings);

	++qsServedFromDisk;

 return 0;
}

QpWithCastes::QpWithCastes() : QPBase(),lexUpd(nullptr),queryReader(nullptr),
		consolidationReader(nullptr),consolidationWriter(nullptr),forceGeoMerge(false),
		ubp(nullptr){
	zeroRet[0] = zeroRet[1] = zeroRet[2] = 0;
}

void QpWithCastes::setup( //queryReader,consolidationWriter, consolidationReader,updatesWriter
		std::unordered_map<std::string,listAttributesUpdater*> updaters,
		std::unordered_map<std::string,listAttributesProvider*> providers,
		DiskReaderI& qrdr,
		DiskWriterI& cwrt,
		DiskReaderI& crdr,
		DiskWriterI& ){

	lexUpd=updaters[n__lexUpd];
	casteManager=providers[n__casteManager];
	queryReader = &qrdr;
	consolidationReader = &crdr;
	consolidationWriter = &cwrt;
}

bool readListOnly(unsigned /*segCnt, size_t listLen, size_t casteLen*/) {
	return true; //always read one list
	//return segCnt == 1; //always read all
}


std::string debugAssert1(size_t id, size_t size, const flagsData& f) {
	return "cst_" + toString(id) + "_.segStack.size="+toString(size)+" is not in par with term flags=" + toString(f) + " setBits=" + toString(b_getSetBitInFlags(f));
}

//convention:
//flags(tid) returns #segments of term (not of caste!)
//length(tid) returns the length of this term
//offset(tid) returns the length of entire caste
size_t QpWithCastes::operator()(tid_t tid) {
	++totalAsked;

	//devirtualization...
	CasteManager* manager = static_cast<CasteManager*>(casteManager);
	LexiconForGolden* globalLex = static_cast<LexiconForGolden*>(lexUpd);

	caste& cst = manager->CasteManager::operator [](tid);

	auto len = manager->CasteManager::length(tid); //delegate to global
	if(len == 0) { //not on disk
		++zeroRet[0];
		//Note the silly visit!
		visitCache(tid,len);
		if(manager->CasteManager::hasCaste(tid))
			cst.queryReg(); //no seeks, just inc totalQueries
		return 0;
	}
	else if(visitCache(tid,len)) {
		++zeroRet[1];
		cst.queryReg(); //no seeks, just inc totalQueries
		return 0;
	}

	TASSERT(manager->CasteManager::hasCaste(tid), "not yet part of caste system")
	const unsigned termSegmentsCount =  b_getSetBitInFlags(globalLex->LexiconForGolden::flags(tid));
	TASSERT(termSegmentsCount > 0,"never happened, BTW")

	++qsServedFromDisk;

	cst.queryReg(termSegmentsCount);

	auto shiftPostings = (env == Env::CHRONO) ? acknowledgeCrhonoShift(tid) : 0;
	if(len<shiftPostings) {
		//note: this can happen in castes if we evict rarely!
		return 0;
	}

	readDataFromSegments(reinterpret_cast<DiskReader*>(queryReader),termSegmentsCount,len-shiftPostings); //read list only
	return 0;
}

#ifdef SEGHIST
	segmentsHist[termSegmentsCount]+=1;
#endif

