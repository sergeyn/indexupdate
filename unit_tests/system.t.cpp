#include "gtest/gtest.h"

TEST(SystemTest, IntegralType) {
   EXPECT_EQ(sizeof(size_t), 8);
}
