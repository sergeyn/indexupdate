#include <gtest/gtest.h>

#include "../ReMergePolicy.h"
#include "../Prognosticator.h"
#include "../BufferManageSimulator.h"
#include "../Globals.h"
#include "../Types.h"
#include "../TuplesRW.h"

//make_hangers(const std::vector<size_t>& segments)
TEST(OptCons, makeHangers2) {
	auto res = make_hangers({1,2});
	std::vector<int64_t> should = {1,-1,2,-1};
	EXPECT_EQ(should,res);
}

TEST(OptCons, makeHangers3) {
	auto res = make_hangers({1,2,3});
	std::vector<int64_t> should = {1,-1,2,-1,3,-1};
	EXPECT_EQ(should,res);
}

TEST(OptCons, oneWinQueries) {
	EXPECT_DOUBLE_EQ(0.0, oneWinQueries(0.0,42*1024*1024));
}
TEST(OptCons, windowsQueries) {
	EXPECT_EQ(8,windowsQueries({1,2,3,5},0));
	EXPECT_EQ(8,windowsQueries({1,2,3,5},1));
	EXPECT_EQ(8,windowsQueries({1,2,3,5},2));
	EXPECT_EQ(5,windowsQueries({1,2,3,5},3));
	EXPECT_EQ(0,windowsQueries({1,2,3,5},4));
}

TEST(OptCons, getBestCost2_0Q) {
	BaseScheduler bs({1541589890ull,1541333370ull},0.0);
	EXPECT_TRUE(bs().empty());
}

TEST(OptCons, getBestCostSmalQ_HQ) {
	std::vector<size_t> v {773624398,388115918,192062544,97399424,47538721,18280839,8945730,8531709,773594317,387940915,192033935,97388609,47537405,18282467,8957905,8522672};
	BaseScheduler bs(v,1000*1000/0.01);
	bs();
	//for(auto c : bs.consolidations()) COUT <<c<< ENDL;
	EXPECT_TRUE(bs.consolidations().empty());
}



TEST(OptCons, consolidate2_128Q) {
	BaseScheduler bs({0,1541589890ll,1541333370ll,0},128);
	ConsolidationPacket cp(1,1,3);
	EXPECT_EQ(cp,bs()[0]);
}

TEST(OptCons, consolidatemany_HQ) {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	PostingSize::szOfPostingBytes = 4;

	std::vector<size_t> v {773624398,388115918,192062544,97399424,
						   47538721,18280839,8945730,8531709,
						   773594317,387940915,192033935,97388609,
						   47537405,18282467,8957905,8522672,
						   0};
	BaseScheduler bs(v,0.01);
	bs();
	for(auto c : bs.consolidations())
		EXPECT_EQ(c.leftMost, 1);
}


TEST(OptCons, make_sure_more_qs_make_more_consolidations) {
	std::vector<size_t> v {773624398,388115918,192062544,97399424,47538721,18280839,8945730,8531709,773594317,387940915,192033935,97388609,47537405,18282467,8957905,8522672};
	unsigned prev = 0;
	for(double d : {1000*1000/0.1, 1000*1000/0.5, 1000*1000/1.0, 1000*1000/2.0, 1000*1000/4.0, 1000*1000/8.0, 1000*1000/128.0, 1000*1000/16000.0})
	{
		//COUTL << "=========> " << d << ENDL;
		BaseScheduler bs(v,d);
		bs();
		//for(auto c : bs.consolidations()) 			COUT << c << ENDL;
		EXPECT_GE(bs.consolidations().size(),prev);
		prev = bs.consolidations().size();
	}
}

TEST(OptCons, chrono0) {
	ChronoScheduler bs({1541589890ll,1541333370ll},0);
	EXPECT_TRUE(bs().empty());
}

TEST(OptCons, rnd0) {
	RndScheduler bs({1541589890ll,1541333370ll},0);
	bs();
	EXPECT_TRUE(bs().empty());
}

//offset_t findCons(const std::vector<ConsolidationPacket>& consSchedule, offset_t szAfterEviction) {
TEST(OptCons, findConsTest) {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	PostingSize::szOfPostingBytes = 4;
	std::vector<size_t> v {773624398,388115918,192062544,97399424,47538721,18280839,8945730,8531709,773594317,387940915,192033935,97388609,47537405,18282467,8957905,8522672,0};
	BaseScheduler bs(v,1000*1000/8.0);
	bs();
/*
 * unit_tests/OptimalCons.t.cpp:100 1 - 3 - 7 gain: 1.45633
   unit_tests/OptimalCons.t.cpp:100 3 - 5 - 7 gain: 1.54662
 *
 */
	const auto& cons = bs.consolidations();
	for(auto c : cons) COUTL << c << ENDL;
	EXPECT_EQ(3, cons[findCons(cons,3)].position);
	EXPECT_EQ(7, cons[findCons(cons,5)].position);
	EXPECT_EQ(1,cons[findCons(cons,8)].position);
	EXPECT_EQ(7,cons[findCons(cons,6)].position); //missing 6
}

TEST(OptCons, TestAssumption1) {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	PostingSize::szOfPostingBytes = 4;
	const unsigned ubLimit = 1717986918 - 171798691;
	const size_t maxUBPostings =         1717986918;
	const size_t totalExpPostings = 64000000000ULL;

	std::vector<size_t> sizes {449469617,447453914,445431865,444981957,448841795,444779557,446386275,445387226,446265882,445672269,445370996,444611358,445186264,444878002,444437011,444643876,444394773,444403074,444338059,444368684,444384737 };
	Upd::BufferManageSimulator sim(maxUBPostings);
	sim.predictForPrognosticator(sizes,std::vector<size_t>(sizes.size(),0),totalExpPostings,ubLimit);

//	std::vector<size_t> v {773624398,388115918,192062544,97399424,47538721,18280839,8945730,8531709,773594317,387940915,192033935,97388609,47537405,18282467,8957905,8522672};
	for(const auto& v : sim.getEvictions()) {
		for(double d : {1000*1000/1.0, 1000*1000/2.0, 1000*1000/8.0, 1000*1000/128.0, 1000*1000/1024.0}	) {
			auto isSmallest = testAssumption(v,d);
			EXPECT_TRUE(isSmallest);
			if(!isSmallest)
				testAssumption(v,d,true);
		}
	}
}


////std::tuple<double,size_t,size_t> getBestCost(const offset_t totalEvictions, std::vector<int64_t>& ss,const std::vector<int64_t>& queryPerWindow)
//TEST(OptCons, getBestCost2_0Q) {
//	auto vec = make_hangers({1541589890,1541333370});
//	const std::vector<int64_t> queryPerWindow = {0,0};
//	auto res = getBestCost(2,vec,queryPerWindow);
//	EXPECT_EQ(std::make_tuple(0.0,0ULL,0ULL),res);
//}
//
//TEST(OptCons, getBestCost2_HugeQ) {
//	auto vec = make_hangers({1541589890,1541333370});
//	const std::vector<int64_t> queryPerWindow = {0,25000};
//	auto res = getBestCost(2,vec,queryPerWindow);
//	EXPECT_EQ(1,std::get<1>(res));
//	EXPECT_EQ(3,std::get<2>(res));
//}

//TEST(OptCons, FindOptFunAndClass) {
//	std::vector<std::vector<size_t> > actuals {
//		 {1541589890,1541333370}
//		,{773624398,388115918,192062544,97399424,47538721,18280839,8945730,8531709,773594317,387940915,192033935,97388609,47537405,18282467,8957905,8522672}
//		,{81330756,11462622,10536711,12300004,9640203,9587021,10479008,11885621,13712819,16002867,8372653,15954588,8373470,18301001,8748892,9315671,20031787,8659826,9172336,9265186,8846051,21616987,8278037,8846269,8750708,10342000,10618798,8606025,25400983,8517460,9315367,8985422,10529645,10481510,11520814,8192955,12164927,8655252,8185438,32242959,9263970,8566494,8426347,9965450,9447917,9965638,11277172,8611392,11696770,8377877,13942345,8653804,8561047,9498727,16145005,8469313,9083254,9732869,9925219,8517897,10250361,8284416,8467349,44359772,8473544,9125227,8932091,10522813,9411447,9680186,10759449,12494116,8327379,12774642,8515883,15068780,8615892,8279001,8937971,16518028,8610279,9310452,8288025,8328177,9165245,19975370,8565702,9075951,9215713,8800590,9357436,10669611,8609574,11885725,8511405,8982459,9074662,8237546,25880355,8564088,9401720,9077222,10721753,10674190,11656993,8329974,12353797,8183457,8422908,14043975,8652620,8610424,9547758,10107506,8192471,8377592,16708076,8703587,8187105,8657472,8617786,9407485,10901165,8278588,9080506,8189792,12031195,8603034,9077649,9171088,8336026,9774475,8376055,8895337,8473454,81287634,11602808,10478734,12307760,9591590,9593705,10530770,11886643,13714534,15995303,8329251,15999889,8378848,18340550,8703303,9314224,20071915,8606764,9125666,9261962,8846286,21671723,8280482,8841273,8703648,10341129,10625787,8612532,25459012,8469792,9262336,8983121,10523732,10485740,11464460,8190058,12113234,8657453,9174505,32193627,9168977,8566632,8377753,9961855,9406258,9966687,11276807,8561221,11699936,8328394,13903269,8615308,8517556,9454146,16093907,8423568,9077119,9687320,9922024,8473182,10197294,8234948,8420428,44361942,8424483,9125871,8938811,10575828,9404590,9684314,10766644,12497980,8281196,12772968,8513586,15064777,8608369,8281209,8939052,16467351,8610500,9316604,8279870,8328690,9175881,19971942,8520083,9078944,9222040,8798691,9362105,10621472,8607842,11879855,8515322,8990792,9081136,8236343,25871385,8560217,9402494,9127573,10666871,10618534,11651708,8329964,12308915,8237812,8419306,14039326,8659769,8608484,9548028,10106021,8189939,8377886,16662238,8749127,8189594,8659037,8608530,9355330,10908580,8328633,9073447,8189560,12026311,8608607,9077207,9170306,8332307,9775272,8377795,8888590,8469315}
//	};
//
//	double ratio = 1000000.0/128.0;
//	for(const auto& v: actuals) {
//		auto res = findOptimalConsolidation(v,ratio);
//		BaseScheduler bs(v,ratio);
//		bs();
//		EXPECT_EQ(res,bs.consolidations());
//	}
//}
