#include <gtest/gtest.h>

#include "../Asserts.h"
#include "../FlagsData.h"

TEST(FlagsData, Size) {
	std::cerr << MAX_SEGMENTS << " max-segs\n";
   EXPECT_EQ(sizeof(flagsData)*8, MAX_SEGMENTS);
}

TEST(FlagsData, GetZero) {
   auto f = getZeroFlag();
   EXPECT_EQ(b_getSetBitInFlags(f), 0);
}

TEST(FlagsData, AllOnes) {
   auto f = getOneFlag();
   EXPECT_EQ(b_getSetBitInFlags(f), MAX_SEGMENTS);
}

TEST(FlagsData, TestSetting1Bit) {
	for(unsigned i=0; i<MAX_SEGMENTS; ++i) {
		auto f = getZeroFlag();
		b_setBit(f,i);
		EXPECT_EQ(b_getSetBitInFlags(f), 1);
	}
}

TEST(FlagsData, TestSettingManyBits) {
	auto f = getZeroFlag();
	for(unsigned i=0; i<MAX_SEGMENTS; ++i) {
		b_setBit(f,i);
		EXPECT_EQ(b_getSetBitInFlags(f), i+1);
	}
}

TEST(FlagsData, TestOnesFlag) {
	auto f = getOneFlag();
	EXPECT_EQ(b_getSetBitInFlags(f), MAX_SEGMENTS);
}


TEST(FlagsData, TestSettingLastOne) {
	auto f = getZeroFlag();
	b_setBit(f,MAX_SEGMENTS-1);
	EXPECT_EQ(b_getSetBitInFlags(f), 1);
}


TEST(FlagsData, TestSettingPastLastOne) {
	flagsData f[2] = { getZeroFlag(), getZeroFlag()};
	b_setBit(f[0],MAX_SEGMENTS);
	b_setBit(f[0],MAX_SEGMENTS+1);
	EXPECT_EQ(b_getSetBitInFlags(f[0]), 0);
	EXPECT_EQ(b_getSetBitInFlags(f[1]), 2);
}

void testFlagsData() {
	for(unsigned p=1; p<64; ++p)
		for(unsigned k=2; k<64; ++k)
			 if(p+k<20)
				 MasksForLexFixing::testFlagsData(k,p);

	for(unsigned p=1; p<64; ++p)
		for(unsigned k=2; k<64; ++k)
			 if(p+k>=20 && p+k<28/*8*sizeof(flagsData)*/)
				 MasksForLexFixing::testFlagsData(k,p);
}


void MasksForLexFixing::testFlagsData(unsigned k, unsigned p) {
//#ifndef LARGEFLAGS
	//easy case 1:
	//easy case --> all to 1
	//zero all and set the 0-bit to OR of others
	// test 0 -> 0 with different bits:  1,2,3,4,5
        // test !0 -> 1 with different bits: 1,2,3,4,5

	//easy case --> all hi bits are 0
        //or with 0

	//examples of harder cases
	// 2 last segs to 1 seg at 1
	// 0...0abc -> 0...00[a|b]c
        // for ab=0 to 3:  for c = 0 to 1: ab*2+c -> 2+c
	// 01.0 -> 01.0
        // 10.0 -> 01.0
        // 11.0 -> 01.0
	// 01.1 -> 01.1
	// 10.1 -> 01.1
	// 11.0 -> 01.1

	//2 last segs to 1 seg at 2
	// 0...0ab.cd -> 0...00[a|b]cd
	// for ab=0 to 3: for cd = 0 to 3: ab*4+cd -> 4+cd
	// 01.00 -> 01.00
        // 10.00 -> 01.00
	// 11.00 -> 01.00 ...

	//k last segs to 1 seg at p: p>0 and k>1
        //0...0  bp+k-1 ...  bp+2 bp+1 bp+0 .  bp-1  ... b1 b0 -> 0...0 OR[bp+k-1 ...  bp+2 bp+1 bp+0].bp-1  ... b1 b0
	//for hi=0; hi<2^k; for lo=0; lo<2^p  hi<<p + lo -> 1<<k + lo

	flagsData arr[2] = { 0 , 0 };
	MasksForLexFixing::initMasks();
	std::cout << "testing with k (hi bits) =  " << k << " and p (low bits) = " << p  << std::endl;
        for(unsigned hi=unsigned(2); hi<(unsigned(1)<<k); ++hi)
		for(unsigned lo=unsigned(2); lo<(unsigned(1)<<p); ++lo) {
			#ifndef LARGEFLAGS
			flagsData f = (hi<<p)+lo;
			#else
			flagsData f;
			f[0] = (hi<<p)+lo;
			#endif
			auto masks = MasksForLexFixing::getFlagsForConditionalSet(p);//maskAwayLow,removeHigh,orMask
			//b_setCondBit(flagsData& flags, flagsData maskAwayLow, flagsData removeHigh, flagsData arr[2])
			arr[1] = std::get<2>(masks);
			b_setCondBit(f,std::get<0>(masks),std::get<1>(masks),std::get<2>(masks));

			#ifndef LARGEFLAGS
			flagsData gold = hi ? (1<<p)+lo : lo;
			#else
			flagsData gold;
			gold[0] = hi ? (1<<p)+lo : lo;
			#endif

			#ifndef LARGEFLAGS
			if(f!=gold) {
			#else
			if(f[0]!=gold[0]) {
			#endif
				std::cout << hi << "." << lo <<  " failed " << std::endl ;
				#ifndef LARGEFLAGS
				std::cout << ((hi<<p)+lo) << " -> " << f << "!=" << gold << std::endl;
				std::cout << "maskLow: " << std::get<0>(masks) << " rmHi: " << std::get<1>(masks)
					<< " orMask: " << arr[1] << std::endl;
				#else
				std::cout << ((hi<<p)+lo) << " -> " << f[0] << "!=" << gold[0] << std::endl;
				std::cout << "maskLow: " << std::get<0>(masks)[0] << " rmHi: " << std::get<1>(masks)[0]
					<< " orMask: " << arr[1][0] << std::endl;
				#endif
			}
			//std::cout << hi << "." << lo <<  " passed " << std::endl ;
		}
	std::cout << "done" <<std::endl;
//#endif
}

