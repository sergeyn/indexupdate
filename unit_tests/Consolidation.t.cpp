#include <gtest/gtest.h>

#include <unistd.h>
#include "../Prognosticator.h"
#include "../BufferManageSimulator.h"
#include "../Caste.h"
#include "../FlagsData.h"
#include "../Globals.h"
#include "../IO.h"
#include "../ReMergePolicy.h"
#include "../Types.h"

ConsolidationStats priceLogConsolidationAtEveryKEviction(unsigned k, unsigned rounds, size_t sz) {
	if(0 == sz)
		sz = 275251;

	ConsolidationStats acc;
	std::vector<size_t> sizes;
	for(auto i : make_irange(rounds)) {
		sizes.push_back(sz);
		if(i%k==0) {
			auto where = ReMergePolicy::whereTelescopic(sizes);
			if(where >= MAX_SEGMENTS - 1)
				where -= 4;

			acc += auxConsolidateTail(sizes,where,0,1);
		}
	}
	return (acc);
}


TEST(ConsolidationTest, priceLogConsolidationAtEveryKEviction) {
		EXPECT_EQ(0,ConsTime::consolidationDataToMinutes(ConsolidationStats()));
		PostingSize::szOfPostingBytes = 4;
		size_t sz = 275251;
		auto acc1 = priceLogConsolidationAtEveryKEviction(1,64*1024,sz);
		auto acc64 = priceLogConsolidationAtEveryKEviction(64,64*1024,sz);
		auto acc1k = priceLogConsolidationAtEveryKEviction(1024,64*1024,sz);
//		234505043968 :r 252543893504 :w 148816 :s 68.9716
//		162088432625 :r 162370289649 :w 81002 :s 43.8312
//		146784477025 :r 146802093089 :w 76871 :s 40.0779

		CERR << acc1 << "\n" << acc64 << "\n" << acc1k <<"\n";

//		acc = priceLogConsolidationAtEveryKEviction(1,1024,64*275251);
//		COUTL << acc.rdata << " :r " << acc.wdata << " :w " << acc.seeks << " :s " << consDataToMinutes(acc) << " \n";

		EXPECT_LT(ConsTime::consolidationDataToMinutes(acc64),ConsTime::consolidationDataToMinutes(acc1));
		EXPECT_LT(ConsTime::consolidationDataToMinutes(acc1k),ConsTime::consolidationDataToMinutes(acc64));
}

//can't get empty stack, but can get a 1-sized
//if one segment -- returns 0
//if last two are not comparable, returns size-1
//if every suffix is comparable to the one before suffix, will return 0
//returns size-1, in some contexts it means -- write down the last one!
TEST(ConsolidationTest,checkTelescopic1)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {1000};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,0u);
}

TEST(ConsolidationTest,checkTelescopic2Equal)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {16*1024, 16*1024};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,0u);
}

TEST(ConsolidationTest,checkTelescopic2NotEqual)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {16*1024, 1024};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,sizeStack.size()-1);
}

TEST(ConsolidationTest,checkTelescopic3NotEqual)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {16*1024, 8*1024, 1024};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,sizeStack.size()-1);
}

TEST(ConsolidationTest,checkTelescopic3Fold)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {16*1024, 8*1024, 8*1024};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,0);
}

TEST(ConsolidationTest,checkTelescopic4Fold)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {32*1024, 2*1024, 1024, 1024};
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(where,1);
}

TEST(ConsolidationTest,checkTelescopicAndConsolidatonSums)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	const unsigned rounds = 32;
	const size_t base = 1024;
	for(unsigned i = 0; i < rounds; ++i) {
		sizeStack.push_back(base); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		auxConsolidateTail(sizeStack,where,0,1);
		ASSERT_EQ(base*(1+i),std::accumulate(sizeStack.begin(),sizeStack.end(), 0ull));
	}
}

TEST(ConsolidationTest,checkTelescopicAndConsolidatonSumsLargeBase)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	const unsigned rounds = 32;
	const size_t base = 22000000;
	for(unsigned i = 0; i < rounds; ++i) {
		sizeStack.push_back(base); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		auxConsolidateTail(sizeStack,where,0,1);
		ASSERT_EQ(base*(1+i),std::accumulate(sizeStack.begin(),sizeStack.end(), 0ull));
	}
}

TEST(ConsolidationTest,checkTelescopicAndConsolidatonSumsNotP2)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	const unsigned rounds = 2000;
	const size_t base = 1023;
	for(unsigned i = 0; i < rounds; ++i) {
		sizeStack.push_back(base); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		auxConsolidateTail(sizeStack,where,0,1);
		ASSERT_EQ(base*(1+i),std::accumulate(sizeStack.begin(),sizeStack.end(), 0ull));
	}
}


TEST(ConsolidationTest,checkTelescopicAndConsolidatonOnLog)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	// Generate the stack size series
	//https://oeis.org/A000120
	//a(0) = 0, a(2*n) = a(n), a(2*n+1) = a(n) + 1.
	const unsigned rounds = 1024;
	std::vector<size_t> stSizes(rounds,0);
	stSizes[1] = 1;
	for(unsigned i=2; i<stSizes.size(); i+=2) {
		stSizes[i]=stSizes[i>>1];
		stSizes[i+1]=stSizes[i>>1]+1;
	}

	for(unsigned e=1; e<stSizes.size()-2; ++e) {
		sizeStack.push_back(1011); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		auxConsolidateTail(sizeStack,where,0,1);
		ASSERT_EQ(sizeStack.size(),stSizes[e]);
	}
}


TEST(ConsolidationTest,checkTelescopicAndConsolidatonEvens)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	// Generate the stack size series
	//https://oeis.org/A000120
	//a(0) = 0, a(2*n) = a(n), a(2*n+1) = a(n) + 1.
	const unsigned rounds = 1024;
	std::vector<size_t> stSizes(rounds,0);
	stSizes[1] = 1;
	for(unsigned i=2; i<stSizes.size(); i+=2) {
		stSizes[i]=stSizes[i>>1];
		stSizes[i+1]=stSizes[i>>1]+1;
	}

	for(unsigned e=1; e<stSizes.size()-2; ++e) {
		sizeStack.push_back(1011); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		const auto delta = sizeStack.size() - where;
		const auto sumW = std::accumulate(sizeStack.begin()+where,sizeStack.end(),0ull);
		const auto sumR = sumW - sizeStack.back();
		ConsolidationStats cons = auxConsolidateTail(sizeStack,where,0,1);
		if(0 == e%2) {
			ASSERT_EQ(cons.wdata,sumW);
			ASSERT_EQ(cons.wdata,sizeStack.back());
			ASSERT_EQ(cons.rdata,sumR);
			ASSERT_GE(cons.rseeks+cons.wseeks,delta); //weak constraint
		}
	}
}

TEST(ConsolidationTest,checkTelescopicAndConsolidatonOdds)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack;
	const unsigned rounds = 1024;
	const size_t sz = 1011;
	for(unsigned e=1; e<rounds; ++e) {
		sizeStack.push_back(sz); //emulate eviction
		unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
		ConsolidationStats cons = auxConsolidateTail(sizeStack,where,0,1);
		if(e%2) {
			ASSERT_EQ(where,sizeStack.size()-1);
			ASSERT_EQ(cons, ConsolidationStats(0,0,sizeStack.back(),1));
			ASSERT_EQ(sz, sizeStack.back());
		}
	}
}


TEST(ConsolidationTest,checkTelescopicAndConsolidatonConcrete1)  {
	ASSERT_EQ(ReMergePolicy::getCmpMargin(), std::make_pair(15u,16u));
	std::vector<size_t> sizeStack = {43475000, 22000000};
	sizeStack.push_back(22000000); //emulate eviction
	unsigned where = ReMergePolicy::whereTelescopic(sizeStack);
	EXPECT_EQ(0,where);
	auxConsolidateTail(sizeStack,where,0,1);
	EXPECT_EQ(sizeStack[0], 43475000 + 22000000 + 22000000);
}

TEST(ConsolidationTest,checkAlwaysMerge_8_372)  {
	PostingSize::szOfPostingBytes = 4;
	ReMergePolicy::setCmpMargin({0ul,1ul});
	std::vector<size_t> sizeStack;
	std::vector<ConsolidationStats> consStack = {ConsolidationStats()};
	const size_t sz = 171899500ull; // 171798691;
	for(unsigned i=0; i<373; ++i) 	{
		sizeStack.push_back(sz); //emulate eviction
		auto c = consStack.back();
		auto where = ReMergePolicy::whereTelescopic(sizeStack);
		ASSERT_EQ(where,0);
		c += auxConsolidateTail(sizeStack,where,0,1);
		consStack.push_back(c);
	}
	EXPECT_GE(consStack.back().seeks(), 1063070ull);
	consStack.pop_back();
	EXPECT_LE(consStack.back().seeks(), 1063070ull);
	COUT << consStack.back() << " " <<  consStack.back().seeks() << ENDL;
	ReMergePolicy::setCmpMargin(NUMERIC::compareMargin);

}

//$$$ creader-Bytes: 4774945176000 creader-Seeks: 108566 creader-Total-time-minutes: 518.639
//$$$ cwriter-Bytes: 5026259628000 cwriter-Seeks: 39 cwriter-Total-time-minutes: 532.608
TEST(ConsolidationTest,checkAlwaysMerge_75_39)  {
	PostingSize::szOfPostingBytes = 4;
	ReMergePolicy::setCmpMargin({0ul,1ul});
	std::vector<size_t> sizeStack;
	std::vector<ConsolidationStats> consStack = {ConsolidationStats()};
	const size_t sz = 1610613000ull;
	for(unsigned i=0; i<40; ++i) 	{
		sizeStack.push_back(sz); //emulate eviction
		auto c = consStack.back();
		auto where = ReMergePolicy::whereTelescopic(sizeStack);
		ASSERT_EQ(where,0);
		c += auxConsolidateTail(sizeStack,where,0,1);
		consStack.push_back(c);
	}
	EXPECT_GE(consStack.back().seeks(), size_t(108566+39));
	consStack.pop_back();
	EXPECT_LE(consStack.back().seeks(), size_t(108566+39));
	COUT << consStack.back() << " " <<  consStack.back().seeks() << ENDL;
	ReMergePolicy::setCmpMargin(NUMERIC::compareMargin);

}


TEST(ConsolidationTest, auxRebuildCPrice) {
	const size_t sz = 171899500ull;
	caste cst;
	cst.addSegment(sz);
	cst.addSegment(sz); //emulate eviction

	auto where = ReMergePolicy::whereTelescopic(cst.getSegmentStackUnsafe());
	ASSERT_EQ(where,0);

	for(unsigned i=0; i<64; ++i) {
		cst.prepareTokens(); //auxRebuildCPrice(cst.consolidationCosts,cst.segmentStack,lastOnDisk,0);

		ASSERT_EQ(cst.size(),cst.consolidationCosts.size());
		cst.tokens = 0;
		ASSERT_EQ(cst.whereAffordConsoidation(0,0),offset_t(cst.size()));
		cst.tokens = 999789456123.0;
		ASSERT_EQ(cst.whereAffordConsoidation(0,0),offset_t(0));

		auto stats = auxConsolidateTail(cst.getSegmentStackUnsafe(),where,0,1);
		ASSERT_DOUBLE_EQ(ConsTime::consolidationDataToMinutes(stats),cst.consolidationCosts[where]);

		cst.addSegment(sz); //emulate eviction
		where = ReMergePolicy::whereTelescopic(cst.getSegmentStackUnsafe());
	}
}

//if(0) { //test -- simple model
//
//	const unsigned evictions = 512;
//
//	unsigned qs[] = {1,16,64,128,512,1024, 4096,4*4096};
//	size_t segs[] = {100000,100000*5,100000*10,100000*320};
//
//	for(auto s : segs) {
//		COUTL << s << " is the seg size\n";
//		for(auto q : qs){
//			testAssumption(0,0,q,s,evictions);
//		}
//	}
//
//	int i;
//	COUTL << "waiting ";
//	std::cin >> i;
//}
//}

void nil() {
if(0) {
	Upd::BufferManageSimulator b;
	b.run2((size_t(1)<<35), 121, 275251);
	exit(0);
}
}



