#!/usr/bin/env ruby

require 'zlib'

def oneFile fname, last=false
	matrix = {}
	row32 = 0
	fstream = (fname.split('.')[-1] == 'gz') ? Zlib::GzipReader.new(open(fname)) : File.new(fname)
	fstream.each_line { |l|
		next unless l.include? '$$$'
		arr = l.sub('$$$','').split
		(0...arr.size).step(2).each { |i| 
			matrix[arr[i]] = [] unless matrix[arr[i]]
			matrix[arr[i]] << arr[i+1]
			row32 = matrix[arr[i]].size if(row32 == 0 and arr[i].include? 'totalSeenPostings' and arr[i+1].gsub(',','').to_i > 32000000000)
		}
	}

	#puts matrix.keys.sort.join " "
	#puts (matrix.keys.sort.map { |k| matrix[k][row32] }).join " "
	#puts '--------------------'
	sz = matrix[matrix.keys.first].size
	range = last ? (sz-2..sz-1):(0..sz)
	#range.each { |row|
		row = -1
		print (matrix.keys.sort.map { |k| matrix[k][row].gsub(',','') }).join " "
	        print ' ' 
	        puts (matrix.keys.sort.map { |k| ((k.include? 'Total-time-') ? matrix[k][row].gsub(',','') .to_f : 0) }).inject('+')
	#}
end

if(ARGV.size() == 1)
	oneFile(ARGV[0],false)
else
	ARGV.each {|f| 
		puts f
		oneFile(f,true)
		#puts ''
		}
end
	
