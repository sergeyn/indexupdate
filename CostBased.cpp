////
//// Created by sergeyn on 02/04/16.
////
//
//#include "CostBased.h"
//#include <assert.h>
//#include "Globals.h"
//
//#ifdef MYTESTS
//#include <gtest/gtest.h>
//#endif
//
//namespace Caching {
//
//class Term {
//public:
//	term_t term;
//	unsigned hitCount; //how many times was hit since introduction to cache
//	size_t length;
//	Term() : hitCount(1){}
//
//	inline uint64_t cost() const { return length * size_t(hitCount); }
//};
//
////static inline bool operator<(const Term &a, const Term &b) { return a.cost() < b.cost(); }
////==============================================================================================
//class BaseCache::BaseCacheIMPL {
//public:
//	std::unordered_map<term_t, Term> lookupTable;
//
//	Term *visit(term_t term) {
//		auto it = lookupTable.find(term);
//		return it == lookupTable.end() ? nullptr : &(it->second);
//	}
//
//	Term* placeNew(term_t term, size_t length) {
//		Term& t = lookupTable[term];
//		t.term = term;
//		t.length = length;
//		return &t;
//	}
//
//	void evictMany(const std::vector<term_t>& victims) {
//		for(auto v : victims)
//			evict(v);
//	}
//	inline void evict(term_t t) { lookupTable[t].length = 0; }
//	size_t size() const { return lookupTable.size(); } //not true!
//};
////==============================================================================================
//size_t BaseCache::size() const { return baseimpl->size(); }
//void BaseCache::evictMany(const std::vector<term_t>& victims) { return baseimpl->evictMany(victims);}
//Term* BaseCache::visit(term_t term) const {return baseimpl->visit(term); }
//Term* BaseCache::placeNew(term_t term, size_t length) { return baseimpl->placeNew(term,length); }
//void BaseCache::evict(term_t t) { return baseimpl->evict(t);}
//
//BaseCache::BaseCache() :
//            		cacheHits(0),cachePostingsServed(0),cachePostingsMissed(0),
//					baseimpl(new BaseCacheIMPL()){}
//BaseCache::~BaseCache() { delete baseimpl; }
//
//void BaseCache::setMaxPostings(size_t maxP) {
//	if(maxPostings)
//		FATAL("can't resize cache in this implementation");
//	maxPostings = maxP;
//}
//
//
//bool BaseCache::visit(unsigned term, size_t length) {
//	auto tptr = BaseCache::visit(term);
//	if( (!tptr) || tptr->length == 0) { //miss
//		miss(term, length);
//		cachePostingsMissed += length;
//		return false;
//	}
//	++cacheHits;
//	cachePostingsServed +=tptr->length;
//	hit(tptr, length);
//	return true;
//}
////==============================================================================================
////    static inline uint64_t bucketToCost(unsigned bucket) { //0 -> 16*16
////        return 1 << (8 + bucket);
////    }
////    TEST(CostBased, bucketToCost) {
////        EXPECT_EQ(256ull,Caching::bucketToCost(0));
////        EXPECT_EQ(512ull,Caching::bucketToCost(1));
////    }
//
//static inline uint64_t costToBucket(uint64_t cost) { //0 -> 16*16
//	const auto s = cost>>8;
//	return s ? 63 - __builtin_clzll(s) : 0;
//}
//
//class CostBased::CostBasedIMPL {
//	friend class CostBased;
//	std::vector<std::unordered_map<term_t,Term*> > buckets;
//	size_t totalPostings;
//	size_t maxPostings;
//
//public:
//	CostBasedIMPL(size_t maxPstings) :  totalPostings(0),maxPostings(maxPstings){
//		buckets.reserve(32);
//	}
//	void report(std::ostream& out)const {
//		for(const auto& bucket : buckets) {
//			uint64_t acc = 0;
//			for(auto p : bucket)
//				acc += p.second->length;
//			out << bucket.size() << "/" << acc << " ";
//		}
//		out << std::endl;
//	}
//
//	void setMaxPostings(size_t maxP) { maxPostings = maxP; }
//	void putInPlace(Term *tptr) {
//		auto bucket = costToBucket(tptr->cost());
//		if(bucket>=buckets.size())
//			buckets.resize(bucket+2);
//		buckets[bucket][tptr->term] = tptr;
//	}
//
//#define DODGY_HIT_MODE
//	void hit(Term *tptr,size_t newLength) {
//		auto oldBucket = costToBucket(tptr->cost());
//		++tptr->hitCount;
//		if(tptr->length != newLength) { //cache data not coherent
//#ifdef DODGY_HIT_MODE
//			totalPostings -= tptr->length;
//			totalPostings += newLength;
//			tptr->length = newLength;
//#elif UNCOMPR_EVICTION
//			totalPostings -= tptr->length;
//
//			buckets[oldBucket].erase(tptr->term); //always remove form old bucket
//			tptr->length = 0;
//			putInPlace(tptr); //put in 0 bucket for later removal
//			return;
//#endif
//		}
//
//		//updates of hitCount and length
//		auto bucket = costToBucket(tptr->cost());
//		if (oldBucket != bucket) { //promotion is needed
//			putInPlace(tptr);
//			buckets[oldBucket].erase(tptr->term); //always remove form old bucket
//		}
//	}
//
//	void miss(Term *tptr, std::vector<term_t>& victimList) {
//		//make space for it
//		auto len = tptr->length;
//		size_t offset = 0;
//		for(auto& bucket : buckets) {
//			for(const auto& term : bucket) {
//				if(totalPostings+len<maxPostings)
//					break;
//
//				TASSERT(totalPostings >= term.second->length, "huge length");
//				totalPostings -= term.second->length;
//				victimList.push_back(term.second->term);
//			}
//			for(unsigned i = offset; i < victimList.size(); ++i)
//				bucket.erase(victimList[i]);
//
//			if(totalPostings+len<maxPostings)
//				break;
//
//			offset = victimList.size();
//		}
//		totalPostings += len;
//		putInPlace(tptr);
//		CHEAP_ASSERT(totalPostings<maxPostings,"cache overflow!")
//	}
//};
////==============================================================================================
//void CostBased::setMaxPostings(size_t maxP) {
//	BaseCache::setMaxPostings(maxP);
//	pimpl->setMaxPostings(maxP);
//}
//
//void CostBased::miss(term_t term, size_t length) {
//	if(maxPostings <= length) //too long?!
//		return;
//	Term* tptr = placeNew(term, length);
//	std::vector<term_t> victims;
//	victims.reserve(1024);
//	pimpl->miss(tptr,victims);
//	evictMany(victims);
//
//}
//
//void CostBased::report(std::ostream& out) const { pimpl->report(out); }
//void CostBased::hit(Term *tptr, size_t length) { pimpl->hit(tptr,length); }
//size_t CostBased::getTotalP() const { return pimpl->totalPostings; }
//
//CostBased::CostBased(size_t maxPstings) : pimpl(new CostBasedIMPL(maxPstings)){
//	maxPostings=(maxPstings);
//}
//CostBased::~CostBased() { delete pimpl; }
//
//void MoveToFront::miss(term_t term, size_t length) {
//	FATAL("before go on, need to account for zeroed!")
//
//	if(maxPostings <= length) //too long?!
//		return;
//	while(totalPostings+length>maxPostings) { //evict to accommodate with bound size policy
//		auto last = termQ.end(); //pick victims from end
//		--last;
//		totalPostings -= (*last)->length;
//		localLookup.erase((*last)->term);
//		BaseCache::evict((*last)->term);
//		termQ.pop_back();
//	}
//
//	Term* tptr =  placeNew(term, length);
//	termQ.push_front(tptr);
//	localLookup[term]=termQ.begin();
//	totalPostings += length;
//}
//
//void MoveToFront::hit(Term* tptr, size_t /*length*/) {
//	termQ.splice(termQ.begin(), termQ, localLookup[tptr->term]); //move to begin
//}
//
//
//}
//
//#ifdef MYTESTS
//
//
//TEST(CostBased, costToBucket) {
//	EXPECT_EQ(0ull,Caching::costToBucket(200));
//	EXPECT_EQ(0ull,Caching::costToBucket(1));
//	EXPECT_EQ(0ull,Caching::costToBucket(0));
//	EXPECT_EQ(0ull,Caching::costToBucket(255));
//	EXPECT_EQ(0ull,Caching::costToBucket(256));
//	EXPECT_EQ(0ull,Caching::costToBucket(511));
//	EXPECT_EQ(1ull,Caching::costToBucket(512));
//}
//TEST(CostBased, testEmptyCache) {
//	Caching::CostBased cache(16);
//	EXPECT_FALSE(cache.visit(42,2));
//}
//TEST(CostBased, testHitAfterMiss) {
//	Caching::CostBased cache(16);
//	EXPECT_FALSE(cache.visit(42,2));
//}
//TEST(CostBased, testMissAfterAddingLarge) {
//	Caching::CostBased cache(16);
//	EXPECT_FALSE(cache.visit(42,16));
//	EXPECT_FALSE(cache.visit(42,16));
//}
//TEST(CostBased, testEvictionAfterEvictionOfTwoThatDoNotFit) {
//	Caching::CostBased cache(16);
//	EXPECT_FALSE(cache.visit(42,15));
//	EXPECT_FALSE(cache.visit(43,2));
//	EXPECT_TRUE(cache.visit(43,2));
//	EXPECT_FALSE(cache.visit(42,15));
//	EXPECT_FALSE(cache.visit(43,2));
//	EXPECT_FALSE(cache.visit(42,15));
//}
//TEST(CostBased, testDifferentCacheSizes) {
//	Caching::CostBased cache1(512);
//	Caching::CostBased cache2(1024);
//	Caching::CostBased cache3(1027);
//
//	size_t postings[] = {0,0,0,0};
//	size_t sizes[] = { 103, 205, 307, 411};
//
//	std::mt19937 gen(0);
//	std::discrete_distribution<> d({13, 21, 38, 40});
//	for(unsigned i=0; i<16000; ++i) {
//		auto who = d(gen);
//		auto len = sizes[who];
//		postings[0] += len;
//		postings[1] += cache1.visit(who,len) ? 0 : len;
//		postings[2] += cache2.visit(who,len) ? 0 : len;
//		postings[3] += cache3.visit(who,len) ? 0 : len;
//	}
//	//std::cout << postings[0] << " " << postings[1] << " " << postings[2] << " " << postings[3] <<  "\n";
//	EXPECT_LE(postings[1],postings[0]);
//	EXPECT_LE(postings[2],postings[1]);
//	EXPECT_LE(postings[3],postings[2]);
//	EXPECT_EQ(sizes[0]+sizes[1]+sizes[2]+sizes[3],postings[3]);
//	EXPECT_EQ(postings[0],postings[1]+cache1.cachePostingsServed);
//	EXPECT_EQ(postings[0],postings[2]+cache2.cachePostingsServed);
//	EXPECT_EQ(postings[0],postings[3]+cache3.cachePostingsServed);
//}
//
//TEST(CostBased, testDifferentCacheSizesMTF) {
//	Caching::MoveToFront cache1; cache1.setMaxPostings(512);
//	Caching::MoveToFront cache2; cache2.setMaxPostings(1024);
//	Caching::CostBased cache3;  cache3.setMaxPostings(512);
//	Caching::CostBased cache4; cache4.setMaxPostings(1024);
//	Caching::CacheInterface* caches[] = { &cache1, &cache2, &cache3, &cache4};
//
//	size_t postings[] = {0,0,0,0};
//	size_t sizes[] = { 103, 205, 307, 411};
//
//	std::mt19937 gen(0);
//	std::discrete_distribution<> d({13, 21, 38, 40});
//	for(unsigned i=0; i<16000; ++i) {
//		auto who = d(gen);
//		auto len = sizes[who];
//		for(unsigned c=0; c<4; ++c)
//			postings[c] += caches[c]->visit(who,len) ? 0 : len;
//	}
//	std::cout << postings[0] << " " << postings[1] << "\n" << postings[2] << " " << postings[3] <<  "\n";
//	EXPECT_EQ(postings[0]+cache1.cachePostingsServed,postings[1]+cache2.cachePostingsServed);
//	EXPECT_EQ(postings[1]+cache2.cachePostingsServed,postings[2]+cache3.cachePostingsServed);
//	EXPECT_EQ(postings[2]+cache3.cachePostingsServed,postings[3]+cache4.cachePostingsServed);
//}
//#endif
