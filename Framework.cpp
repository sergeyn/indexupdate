/*
 * Framework.cpp
 *
 *  Created on: Jan 2, 2014
 *      Author: sergeyn
 */

#include "Framework.h"
#include "TuplesRW.h"
#include "Numeric.h"
#include "QP.h"
#include "DeleteBuffer.h"

class Dispenser {
	const unsigned queries;
	const unsigned updates;
public:
	Dispenser(unsigned us, unsigned qs) : queries(qs), updates(us){	}

	inline unsigned getQs() const { return queries; }
	inline unsigned getUs() const { return updates; }
};
//----------------------------------------------------------------------------
Framework::Framework() : updatesWriter(/*lex*/), updatesDistr(nullptr),queriesDistr(nullptr),deleteDistr(nullptr),updatesBuffer(nullptr), castes(lex),qp(nullptr) {
	freeUpTo = 0;
}
//----------------------------------------------------------------------------
Framework::~Framework() {
	if(deleteDistr)
		delete deleteDistr;
}
//----------------------------------------------------------------------------
void Framework::setup(std::vector<listAttWithCasteId>& casteSorted,
		DistributionInterface* updateD, DistributionInterface* queriesD,
		const std::string& qProc, const std::string& evictor, size_t postingBufferSize, size_t freeUp2, size_t expectedTerms, size_t cacheSize,
		std::pair<unsigned, unsigned> updatesToQueriesRatio,unsigned aggressive,
		const std::string& jumpstartFile,bool loadIOs) {

	castes.loadCastesFromAttMatrix(casteSorted);
	castes.setAggressive(aggressive);
	casteSorted.clear();
	casteSorted = std::vector<listAttWithCasteId>();

	freeUpTo = freeUp2;
	qp = globalRegistry.getInstanceOf<ProcessTerm>(qProc);

	ProcessTerm::upMap updaters;
	ProcessTerm::getMap providers;
	if(qProc == QpWithCastes().name()) {
		updaters[ProcessTerm::n__lexUpd] = &lex;
		providers[ProcessTerm::n__casteManager] = &castes;
	}
	else if(qProc == QpNoWriteBack().name() || qProc == QpURF().name()) {
		updaters[ProcessTerm::n__lexUpd] = &lex;
		providers[ProcessTerm::n__globalLex] = &lex;
		providers[ProcessTerm::n__casteManager] = &castes;
	}
	else FATAL("wrong qproc");

	(reinterpret_cast<QPBase*>(qp))->setCacheSize(cacheSize); // (1L)<<32); //set cache for any qp method -- both castes and nowriteback
	qp->setup(updaters,providers,queryReader,consolidationWriter, consolidationReader,updatesWriter);
	//qp->setIsNever(NUMERIC::compareMarginForNever == ReMergePolicy::getCmpMargin());

	//initiate an update distribution model with it
	updatesDistr = updateD;
	queriesDistr = queriesD;
	deleteDistr = updateD->clone(); //currently the deletes are in the exact chronological order :)

	//	CasteAssigner castA;
	//	for(tid_t i = 0; i<updateDenseHistogram.size(); ++i)
	//		castA.addToBuffer(i+1,qFreqs[i],updateDenseHistogram[i]);

	//castA.organizeBufferedListsIntoCastes(castes);

	//construct an UpdatesBuffer
	//castes.loadCastesFromFile("../tid2caste");
	updatesBuffer = new UpdatesBuffer(evictor,
			*updatesDistr, castes,
			updatesWriter,consolidationWriter,consolidationReader,
			lex,lex,postingBufferSize,expectedTerms*1.1,(reinterpret_cast<QPBase*>(qp)));
	if(jumpstartFile.empty())
		reset(); //normal jumpstart
	else { //load prev. state from a file
		std::ifstream sIn(jumpstartFile);
		if(!sIn.is_open()) FATAL(jumpstartFile);
		jumpstartPrevStateFromStream(sIn,updatesToQueriesRatio,loadIOs);
		updatesBuffer->jumpstart(false,false); 	//fill it
	}
}
//----------------------------------------------------------------------------
void Framework::serializeStateToStream(std::ostream& out,std::pair<unsigned, unsigned> updatesToQueriesRatio) const {
	//dump update rates
	out << updatesToQueriesRatio.first << " " << updatesToQueriesRatio.second << " " << qp->totalAskedQs() << " " ;
	//dump io-proxy stats
	out << consolidationReader.getTotalBytes() << " " << consolidationReader.getTotalSeeks() << " "; //what we had to read to do the merge
	out << queryReader.getTotalBytes() << " " << queryReader.getTotalSeeks() << " "; //the reading for queries
	out << consolidationWriter.getTotalBytes() << " " << consolidationWriter.getTotalSeeks() << " "; //writing during merges
	out << updatesWriter.getTotalBytes() << " " << updatesWriter.getTotalSeeks() << " "; //writing the updates
	//dump ub state
	out << updatesBuffer->seenPostings() << " ";
	//dump tid-len data (without flags data for now)
	auto it = lex.begin();
	auto end = lex.end();
	out<<lex.termsCount()<<" ";
	while(it!=end) {
		out << it->length << " ";
		++it;
	}

}
//----------------------------------------------------------------------------
void Framework::jumpstartPrevStateFromStream(std::istream& in, std::pair<unsigned, unsigned> updatesToQueriesRatio, bool restoreStatsIO) {
	unsigned updRate,qRate, totalQs;
	in >> updRate >> qRate >> totalQs;

	//read io-proxy stats
	uint64_t bytesAndSeeks[8];
	for(unsigned i=0; i<8; ++i)
		in >> bytesAndSeeks[i];

	if(restoreStatsIO) {
		consolidationReader = DiskReader(bytesAndSeeks[0],bytesAndSeeks[1]);
		queryReader = DiskReader(bytesAndSeeks[2],bytesAndSeeks[3]);
		consolidationWriter = WriteBackBuffer(bytesAndSeeks[4],bytesAndSeeks[5]);
		updatesWriter = WriteBackBuffer(bytesAndSeeks[6],bytesAndSeeks[7]);
	}
	COUT3 << "restored io proxy" <<ENDL;
	size_t pst;
	in >> pst;
	updatesBuffer->setTotalSeenPostings(pst);
	//get tid-len data (set flags to 1)!
	tid_t termsCount;
	in >> termsCount;
	lex.resize(termsCount);
	lex.clear();
	did_t len;
	flagsData one(0);
	b_setBit(one,0);

	for(tid_t i=0; i<termsCount; ++i) {
		in >> len;
		lex.addLength(i,len);
		lex.setFlag(i,one);
	}
	COUT3 << "restored lex" <<ENDL;

	//rebuild q. distribution
	std::vector<uint16_t> qsCount(termsCount+10,0);
	double scale = double(updRate/double(qRate))*double(updatesToQueriesRatio.second/double(updatesToQueriesRatio.first));
	COUT3 << "simulate queries with scale of "<<scale<<ENDL;
	const unsigned qsToSample = totalQs * scale;
	for(unsigned i=0; i<qsToSample; ++i)
		qsCount[queriesDistr->next()]+=1;
	queriesDistr->reset(0);

	//fix the castes system
	for(auto cst : castes){
		assert(cst);
		size_t totals = std::accumulate(cst->members.begin(),cst->members.end(),0,[this](size_t partialresult, tid_t index){return partialresult+lex[index].length; });
		cst->clear(totals);

		cst->epochUpdates = totals;
		size_t qtotals = std::accumulate(cst->members.begin(),cst->members.end(),0,[&](size_t partialresult, tid_t index){return partialresult+qsCount[index]; });
		cst->epochQueries = qtotals;

	}
	COUT3 << "restored " <<castes.size() << " castes" <<ENDL;
}
//----------------------------------------------------------------------------
void Framework::reset() {
	updatesBuffer->jumpstart(true,false); 	//fill it
	COUT4 << "jumpstarted ub" << ENDL;
	const bool isCastes = (qp->getCname() == QpWithCastes().getCname());
	if(isCastes) {
		castes.pushLongTailTerms(false); //make sure we push right after jumpstart
		updatesBuffer->jumpstart(false,true); //push some more!
	}
}
inline void printAndSetPercentsFull(UpdatesBuffer* updatesBuffer, unsigned& pct) { 	// nice % printing
	if(pct!=updatesBuffer->percentFull()) {
		pct = updatesBuffer->percentFull();
		CERRV(3) << (unsigned)pct << "% buf\r"; std::cerr.flush();
	}
}
//----------------------------------------------------------------------------
template<typename ProcT>
inline size_t basicCycleQueryUpdate(ProcessTerm* qp, UpdatesBuffer* updatesBuffer, DistributionInterface* queriesDistr, Dispenser& dispenser) {
	ProcT& queryProcessor = reinterpret_cast<ProcT&>(*qp);
	//update: it is somewhat silly to simulate strictly interleaving stream of q&u
	//we will just do a batch of ones and then the other
	const auto qs = dispenser.getQs();
	const auto us = dispenser.getUs();

	for(unsigned c=0; c<us; ++c)
		updatesBuffer->update();
	for(unsigned c=0; c<qs; ++c)
		queryProcessor.ProcT::operator()(queriesDistr->next());

	return us;
}
//----------------------------------------------------------------------------
#define printPredictions
void recordEpochData(CasteManager& castes, double /*maxpostings*/) {
	COUT1 << "Record data for greedy prognosticator " << ENDL;
	//fix the data for all castes
	for(caste* cst : castes) {
		cst->recordEpoch();
	}

	#ifdef printPredictions
	COUTL << "cid,epochQ,epochP\n";
	for(caste* cst : castes.getSortedCopy2()) {
//			const double postingsLeft = double(maxPostings - (maxPostings/NUMERIC::recordEpochDivider));
//			const double lifeRatio = postingsLeft / maxPostings;
//			const double totalExpectedQueries = cst->epochQueries * NUMERIC::recordEpochDivider;
//			const size_t expectedQueries = totalExpectedQueries*lifeRatio;

			COUTL << cst->casteId << '|'<< cst->epochQueries << '|' << cst->epochUpdates << ENDL;
	}
	#endif

}
//----------------------------------------------------------------------------
void Framework::printAllStats(bool isCastes) const {
	COUT << getStats();

	double accPrice = -1;
	if(!isCastes) { //if always merge or log:
		//calc magic time -- the time it takes to write the postings seen in #evictions and do the queries
		accPrice = IOStats(updatesBuffer->seenPostings(),updatesBuffer->getEvictions(),-1).ioTimeInMinutes();
		accPrice += IOStats(queryReader.getTotalBytes(),qp->totalServedFromDiskQs()).ioTimeInMinutes();
	}
	if(isCastes) {
		auto zeros = reinterpret_cast<QpWithCastes*>(qp)->zeroRet;
		COUT << "\nempty: " << zeros[0] << " cached: " << zeros[1] <<  '\n';
	}
	COUT << "\n" << STATMRK <<  " Magic-time: " << accPrice << std::endl;
}

//----------------------------------------------------------------------------
std::pair<BufferStats,BufferStats> Framework::run(unsigned cycles, const size_t totalExperimentPostings, std::pair<unsigned, unsigned> updatesToQueriesRatio, unsigned printStatsFrequency,size_t capacity,
			bool IS_RANDOM_DEL, bool IS_SKI, size_t maxPostings) {

	using namespace Env;
	Dispenser dispenser(updatesToQueriesRatio.first, updatesToQueriesRatio.second);
	BufferStats before(*updatesBuffer);
	const size_t STABLE_CAPACITY = capacity;
	const unsigned ENV = (STABLE_CAPACITY > 0) ? (IS_RANDOM_DEL ? RNDDEL : CHRONO) : EVERGROW;
	updatesBuffer->setEnv(ENV);
	updatesBuffer->setTotalExpPostings(totalExperimentPostings);
	const bool isCastes = (qp->getCname() == QpWithCastes().getCname());
	qp->setIsSkiAndRandom(ENV,IS_SKI,IS_RANDOM_DEL,ENV == 0 ? maxPostings : 0);
	qp->setUB(updatesBuffer);
	qp->setDeletesDistr(deleteDistr);
	size_t prevPostings = 0;
	bool epcohRecorded = false;
	COUT2 << "ENV is " << envirnoment_names[ENV] << "\n";
	if(ENV != EVERGROW)
		COUT2 << "Will switch to stable at " << STABLE_CAPACITY << " postings\n";
	if(!IS_SKI) COUT2 << "Will record epoch upon reaching: " << totalExperimentPostings/NUMERIC::recordEpochDivider  << " postings\n";

	const std::vector<caste_t>* tid2cid = isCastes ?  &castes.getTidToCidMap() : nullptr;
	if(ENV == CHRONO)
		qp->populateChrono(std::max(updatesBuffer->innerLexSize(),lex.termsCount()),0); //the trick is: if not stable, delcount is 0, then the hist is reSized but no filled.
	else if(ENV == RNDDEL)
		qp->populateRnd(std::max(updatesBuffer->innerLexSize(),lex.termsCount()),0, tid2cid); //the trick is: if not stable, delcount is 0, then the hist is reSized but no filled.

	//Main cycle of basic cycles ==========================================================================
	for(unsigned i=0; i<cycles && updatesBuffer->seenPostings()<totalExperimentPostings; ++i) {
		size_t inBAcc = 0;
		while(! ( updatesBuffer->isFull(isCastes) || updatesBuffer->seenPostings()+inBAcc>=totalExperimentPostings ) ) { 		//every cycle executes # updates as given in cli
			auto newPostings = (isCastes) ?
					basicCycleQueryUpdate<QpWithCastes>(qp, updatesBuffer, queriesDistr, dispenser) : //then
					basicCycleQueryUpdate<QpNoWriteBack>(qp, updatesBuffer, queriesDistr, dispenser); //else
			inBAcc += newPostings;

			if(!IS_SKI && updatesBuffer->seenPostings()+inBAcc >= totalExperimentPostings/NUMERIC::recordEpochDivider && !epcohRecorded) { //considerRecording
				epcohRecorded = true;
				recordEpochData(castes,totalExperimentPostings);
			}

			bool stableStateReachedFlag = ENV != EVERGROW && //if we are in either of the stable cases (rnd,chrono)
					updatesBuffer->seenPostings()+inBAcc >= STABLE_CAPACITY;  //and we are beyond stable
			size_t delCount = (stableStateReachedFlag) ? newPostings : 0;
			if(ENV == CHRONO) {
				qp->populateChrono(std::max(updatesBuffer->innerLexSize(),lex.termsCount()),delCount, tid2cid); //the trick is: if not stable, delcount is 0, then the hist is reSized but not filled.
				if(isCastes)
					updatesBuffer->tryShiftStackForCastes(*qp->getInnerDelBuffer());
				else
					updatesBuffer->tryShiftStack(*qp->getInnerDelBuffer(), updatesBuffer->unsafeGetReMergeStack());
			}
			else if(ENV == RNDDEL)
				qp->populateRnd(std::max(updatesBuffer->innerLexSize(),lex.termsCount()),delCount, tid2cid); //the trick is: if not stable, delcount is 0, then the hist is reSized but not filled.

		} //end while buffer not full

		//eviction stage:
#ifdef URF
		size_t evicted = updatesBuffer->evictWithURange();
#else
		//the remerge will reduce totalLivePostingsInSystem if deletion occurs
		//size_t evicted = 0;
		if(updatesBuffer->seenPostings()+inBAcc<totalExperimentPostings) //if we stopped the loop because the buffer is full and not in the end of run
			/*evicted = */(isCastes) ? updatesBuffer->evictWithCastes(freeUpTo,*qp->getInnerDelBuffer())
									 : updatesBuffer->evictWithRemerge(*qp->getInnerDelBuffer());
		else {
			printAllStats(isCastes);
			break;
		}

#endif
		CHEAP_ASSERT( this->updatesWriter.getTotalBytes() <= updatesBuffer->seenPostings() * PostingSize::szOfPostingBytes, "it seems we've written more posting-bytes than we had!")

		//totalLivePostingsInSystem+=evicted;
		queriesDistr->setMax(lex.termsCount()-1); //set largest seen for fixing unseens in distr (optional)

		//reinterpret_cast<QPBase*>(qp)->getCache().report(std::cout);

		//print stats
		if(printStatsFrequency<updatesBuffer->seenPostings()-prevPostings) {
			printAllStats(isCastes);
			prevPostings = updatesBuffer->seenPostings();
		} //end if
	} //end for
//#define STOREQUERIES
#ifdef STOREQUERIES
{ //store queries
	std::stringstream prefix;
	prefix << envirnoment_names[ENV] << '_' << qp->getCname() << '_' << dispenser.getQs() << '_' << updatesBuffer->getMaxAllowedPostings();
	std::string fname = prefix.str();
	QPBase* qpbs = reinterpret_cast<QPBase*>(qp);
	{

	std::ofstream out(fname+"_lens.log");
	for(auto len : qpbs->recordQLens)
		out << len << ENDL;
	out.flush();
	}
	{
	std::ofstream out(fname+"_terms.log");
	for(auto tid : qpbs->recordTerms)
		out << tid << ENDL;
	out.flush();
	}
}
#endif
	//COUT << " hits_of_cache: " <<  qpbs->hitsCount() <<  " curr.size: " << qpbs->cachedPostings() << " cachedMCount: " << qpbs->cachedMCount()<<"\n";
	printAllStats(isCastes);
	castes.printHists();
	//std::ofstream sOut("/tmp/dumpIndex.state",std::ios::out); //serializeStateToStream(sOut,updatesToQueriesRatio); //no sense to do it when many exps run concurrently
	return std::make_pair(before,BufferStats(*updatesBuffer));
}
//----------------------------------------------------------------------------
std::string Framework::getQpStats() const {qp->printHist(); /*CasteStats(castes).printHist(std::cout);*/
return STATMRK + " Queries-asked: "  + toString(qp->totalAskedQs())
				+ " served-from-disk: " + toString(qp->totalServedFromDiskQs())
				+ " postings-missed: " + toString(qp->totalPostingsMissed())
				+ " postings-cached: " + toString(qp->totalPostingsServed());
}
//----------------------------------------------------------------------------
FrameworkStats Framework::getStats() const { return std::make_tuple(getUBStats(),getLexStats(),
		getIO(updatesWriter,"updwriter"),getIO(queryReader,"qreader"),getIO(consolidationReader,"creader"),getIO(consolidationWriter,"cwriter"),
		getQpStats()); }
//----------------------------------------------------------------------------
std::ostream& operator <<(std::ostream& stream, const FrameworkStats& bs) {
	writeTupleToStream(bs,stream);
	return stream;
}

//		//(i%CLEANUP == CLEANUP-1) &&  //and it is time to perform cleanup
//		if(stableStateReachedFlag && false == isCastes && ENV == RNDDEL)
//			totalLivePostingsInSystem -= removeOldDocsStaticRandom(totalLivePostingsInSystem,STABLE_CAPACITY);
//			//if(isCastes)	totalLivePostingsInSystem -= removeOldDocsCastesRandom(totalLivePostingsInSystem,STABLE_CAPACITY);
//		if(stableStateReachedFlag && false == isCastes && ENV == CHRONO)
//			totalLivePostingsInSystem -= removeOldDocsStaticChrono(totalLivePostingsInSystem,STABLE_CAPACITY);
//			//if(isCastes)	totalLivePostingsInSystem -= removeOldDocsCastesChrono(totalLivePostingsInSystem,STABLE_CAPACITY);

//----------------------------------------------------------------------------
/*
template<typename C, typename D>
void fillDeletesHist(C& buffer, D& distribution, size_t tCount, size_t count, bool shouldClear=true) {
	if(shouldClear)
		buffer.clear();
	if(tCount>buffer.size())
		buffer.resize(tCount,0);

	for(size_t i=0; i<count; ++i)
		buffer[distribution->next()]+=1; //add +1 deletion to a term sampled from deleteDistr
}
//----------------------------------------------------------------------------
size_t Framework::removeOldDocsAux(size_t currentSz, size_t maxCapacity) {
	if(currentSz<maxCapacity) return 0;
	size_t toRemove = (currentSz-maxCapacity);
	fillDeletesHist(auxVectorForDeletes,deleteDistr,lex.termsCount(),toRemove);
	return toRemove;
}
*/
//----------------------------------------------------------------------------
/*
unsigned chronoStackShifterHelper(std::vector<size_t>& sizeStack, size_t removed) {
	unsigned shiftStack = 0;
	for(unsigned i=0; i<sizeStack.size() && removed>0; ++i) {
		if(removed>=sizeStack[i]) {
			removed -= sizeStack[i];
			sizeStack[i] = 0;
			++shiftStack;
		}
		else {
			sizeStack[i] -= removed;
			removed = 0;
		}
	}
	if(shiftStack)
			sizeStack.erase(sizeStack.begin(),sizeStack.begin()+shiftStack);

	return shiftStack;
}
//----------------------------------------------------------------------------
std::vector<unsigned> rndStackShifterHelper(std::vector<size_t>& segmentStack, size_t removed) {
	std::vector<unsigned> shifted;
	double totalPostings = std::accumulate(segmentStack.begin(),segmentStack.end(),0.0);
	//spread the removed across segments
	for(unsigned i=0; i<segmentStack.size(); ++i) {
		double ratio = double(segmentStack[i]) / totalPostings;
		double toRemove = floor(removed*ratio);
		TASSERT(toRemove>0,"something wrong with remove invariant");
		segmentStack[i] -= toRemove;

		if( segmentStack[i] < 16 ) {
			segmentStack[i] = 0;
			shifted.push_back(i);
		}
	}

	if(shifted.size()) { //if we actually performed shifts:
		auto pend = std::remove(segmentStack.begin(),segmentStack.end(),0);
		segmentStack.resize(pend-segmentStack.begin());
	}
	return shifted;
}*/
//----------------------------------------------------------------------------
/*
size_t Framework::removeOldDocsCastesChrono(size_t currentSz, size_t maxCapacity) {
	size_t removedTotal = removeOldDocsAux(currentSz,maxCapacity);
	//go caste by caste
	for(CasteManager::mHeap::iterator it = castes.begin(); it!=castes.end(); ++it) {
		caste* cst = *it;
		if(nullptr == cst) continue;
		size_t removed = std::accumulate(cst->members.begin(),cst->members.end(),0,[this](size_t partialresult, tid_t index){return partialresult+auxVectorForDeletes[index]; });
		cst->totalPostings -= removed;
		auto shiftStack = chronoStackShifterHelper(cst->segmentStack,removed);
		lex.deleteOldInBatch(auxVectorForDeletes,cst->members,shiftStack);
	}

	return removedTotal;
}
//----------------------------------------------------------------------------
size_t Framework::removeOldDocsCastesRandom(size_t currentSz, size_t maxCapacity) {
	size_t removedTotal = removeOldDocsAux(currentSz,maxCapacity);

	//go caste by caste
	for(CasteManager::mHeap::iterator it = castes.begin(); it!=castes.end(); ++it) {
		caste* cst = *it;
		if(nullptr == cst) continue;
		size_t removed = std::accumulate(cst->members.begin(),cst->members.end(),0,[this](size_t partialresult, tid_t index){return partialresult+auxVectorForDeletes[index]; });
		cst->totalPostings -= removed;
		auto shiftStack = rndStackShifterHelper(cst->segmentStack,removed);
		lex.deleteOldInBatch(auxVectorForDeletes,cst->members,shiftStack); //update the global lex:
	}

	return removedTotal;
}
//----------------------------------------------------------------------------
size_t Framework::removeOldDocsStaticRandom(size_t currentSz, size_t maxCapacity) {
	size_t removedTotal = removeOldDocsAux(currentSz,maxCapacity);
	size_t removed = removedTotal;

	//fix stacks
	auto& sizeStack = updatesBuffer->unsafeGetReMergeStack();
	std::vector<size_t> sizeStackCopy(updatesBuffer->unsafeGetReMergeStack());


	auto shiftStack = rndStackShifterHelper(sizeStack,removed);
	lex.deleteOldInBatch(auxVectorForDeletes,shiftStack);//update the global lex:

	return removedTotal;
}
//----------------------------------------------------------------------------
size_t Framework::removeOldDocsStaticChrono(size_t currentSz, size_t maxCapacity) {
	size_t removedTotal = removeOldDocsAux(currentSz,maxCapacity);
	size_t removed = removedTotal;

	//fix stacks
	auto& sizeStack = updatesBuffer->unsafeGetReMergeStack();
	//std::vector<size_t> sizeStackCopy(updatesBuffer->unsafeGetReMergeStack());

	auto shiftStack = chronoStackShifterHelper(sizeStack,removed);
//	if(0) {
//		COUTL << "shifting stack after " << removedTotal << ENDL;
//		for(auto it : sizeStackCopy)
//			COUT << it << " ";
//		COUT << ENDL;
//		for(auto it : sizeStack)
//			COUT << it << " ";
//		COUT << ENDL;
//	}

	//update the global lex:
	lex.deleteOldInBatch(auxVectorForDeletes,shiftStack);

	return removedTotal;
}
*/
