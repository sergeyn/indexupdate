#ifndef NUMERIC_H_
#define NUMERIC_H_

// local and globals conts, default values, etc.:
namespace NUMERIC {
	const size_t totalPostingsClueweb(17075485964);
	const unsigned MAXT(86532822); //clueweb initial maxT

	//updates/queries distribution
	const unsigned avgPostingInDoc(340);
	const unsigned newTermFrequency(569*avgPostingInDoc);
	const unsigned newQueryTermFreq(2000);

	//======= IO const =============

	//disk
	constexpr unsigned HDisk_ioMBS(150);
	constexpr unsigned HDisk_ioSeek(7);
	//ssd (samsung)
	constexpr double SSDisk_ioSeek(0.0625);
	constexpr unsigned SSDisk_ioMBS(500);

#ifdef SSD
	constexpr unsigned ioMBS(SSDisk_ioMBS);
	constexpr double ioSeek(SSDisk_ioSeek);
#else
	constexpr unsigned ioMBS(HDisk_ioMBS);
	constexpr unsigned ioSeek(HDisk_ioSeek);
#endif
	
	constexpr unsigned timeoutTolerance(200);
	constexpr size_t bInMB(1<<20);
	constexpr size_t bytesReadInTimeOfSeek(ioSeek * ioMBS * bInMB / 1000);
	constexpr size_t bytesReadBeforeTimeout((ioMBS * bInMB / 1000)*timeoutTolerance);

	const unsigned szOfPostingBytes(4);

	//===== rates of updates and queries ============
	const unsigned qTermsInTraining(37171);

	//to make 0 queries: set updatesCountInStream to >0 and queriesCountInStream=0
	//to make 0 updates: set updatesCountInStream to =0 and queriesCountInStream>0
	const unsigned updatesCountInStream(2);
	const unsigned queriesCountInStream(3);

	//if nothing compares equal -- never merge: std::make_pair(1000,1) //too large num will cause an overflow
	//if all compares equal -- always merge: std::make_pair(0,1)
	//if the margin is around 1, it is the logarithmic scheme -- std::make_pair(15,16)
	const std::pair<unsigned,unsigned> compareMargin(std::make_pair(15,16)); //will be set in main
	const std::pair<unsigned,unsigned> compareMarginForNever(std::make_pair(256*1024,1)); //will be set in main

	const unsigned recordEpochDivider(8);

	const size_t mergingMemBuffer(1ULL<<26);
}


#endif /* NUMERIC_H_ */
