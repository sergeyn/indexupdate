#include "Caste.h"

//will be overwritten in main
size_t caste::minWBSize(1<<14);
size_t caste::maxWBSize(1<<21);

caste::caste() :
			totalPostings(0), //total postings in the system
			totalQueries(0), //all the queries that hit the caste (including cached)
			totalSeeks(0), //total query seeks for this caste (-1ed)
			ubPostings(0), //how many postings are in the UB
			casteId(0), //the id of this caste
			accSeeks(0), //seeks we accumulate doing queries
			evictionRounds(0), //in how many evicitions participated
			tokens(0), //tokens we accumulate through getting seeks
			predictedUpdates(0), //load from training data when creating caste
			predictedQueries(0), //load from training data when creating caste
			epochUpdates(0),//store during record-epoch for think
			epochQueries(0), //store during record-epoch for think
			overflowConsolidations(0),
			// ----- for assumption test
			consolidationData(0),
			consolidationPaid(0),
			consolidationSeeks(0), //temp
			stats {0}
{

	actualEvictions.push_back(0);
	actualQuerySeeks.push_back(0);

}

void caste::evicted(size_t postings) {
	++evictionRounds;
	actualEvictions.push_back(postings);
	actualQuerySeeks.push_back( totalSs() -
			std::accumulate(actualQuerySeeks.begin(),actualQuerySeeks.end(), 0ull)
			);
}

void caste::sortMembers() {
	std::sort(members.begin(),members.end()); //might make further indirect iteration more efficient
}

std::ostream& operator <<(std::ostream& stream, const caste& cst) {
	stream << cst.casteId << " " << cst.members.size() << " " << cst.ubPostings<< "(" << cst.totalPostings << ") " << cst.predictedQueries << ENDL;
	return stream;
}

bool caste::predicateSorterPrint(const caste& lhs, const caste& rhs) {
	if(lhs.totalPostings<rhs.totalPostings) return true;
	else if(lhs.totalPostings==rhs.totalPostings) return lhs.ubPostings < rhs.ubPostings;
	return false;
}

void caste::recordEpoch() {
	epochUpdates = totalPostings + ubPostings;
	epochQueries = totalQueries;
}

void caste::prepareTokens() {
	tokens += IOStats(0,accSeeks,-1).ioTimeInMinutes(); //update tokens with regged seeks
	totalSeeks += accSeeks;
	accSeeks = 0;
	rebuildCostsForEviction();
}

extern offset_t findCons(const std::vector<ConsolidationPacket>& consSchedule, offset_t szAfterEviction);

offset_t caste::whereAffordConsoidation(size_t /*totalSeenPostings*/, size_t /*endOfLifePostings*/) {
	auto it = consolidationCosts.rbegin(); //the first one is free

	if(epochQueries > 0) { //think case:
//		const double postingsLeft = double(endOfLifePostings - totalSeenPostings);
//		const double lifeRatio = postingsLeft / double(endOfLifePostings);
//		const double totalExpectedQueries = epochQueries * NUMERIC::recordEpochDivider;
//		const size_t expectedQueries = (totalExpectedQueries * lifeRatio) / 6;
//
//		for(size_t segs = 0; it!= consolidationCosts.rend(); ++it, ++segs) {
//			if(IOStats(0,expectedQueries*segs,-1).ioTimeInMinutes() < *it)
//				break;
//		}

		if(casteId == 3){
			for(auto s : segmentStack) COUT << " " << s;
			COUT<<ENDL;
			for(auto p : predictedEvictions) COUT << " " << p;
			COUT<<ENDL;
			for(auto c : consolidations) COUT << c << ENDL;
		}

		const offset_t sz = segmentStack.size();
		if(consolidations.empty() || sz<2)
			return sz;
		auto which = findCons(consolidations,sz);
		if(which == consolidations.size())
			return sz;
		return consolidations[which].position >> 1;
	}
	else { //ski-rental
		for(; it!= consolidationCosts.rend(); ++it) {
			if(tokens < *it)
				break;
		}
	}
	return std::distance(it,consolidationCosts.rend());
}


//namespace {
//	bool areCountsWithin1p(size_t a, size_t b) {
//		auto ratio = double(a)/double(b);
//		return ratio > 0.99 && ratio < 1.01;
//	}
//}

void caste::printHistData()const {
	COUT << ENDL << casteId <<  " with " << members.size() <<
			" members and " << actualEvictions.size()-1 <<  " evictions. " <<
			totalQueries << " queries required " << totalSeeks << " seeks (~" <<  std::accumulate(actualQuerySeeks.begin(), actualQuerySeeks.end(),0ull)
			<< "). AT_EVICTION_WRITE_BACK: " << stats[caste::AT_EVICTION_WRITE_BACK]
			<<" Forced consolidations: " << overflowConsolidations << ENDL;
//	COUT << casteId << " actualEvictions|";
//	for(auto e : actualEvictions) {
//		COUT << e << '|';
//	}
//	auto iP = actualEvictions.begin();
//	auto iQS = actualQuerySeeks.begin();
//	while(iP!=actualEvictions.end()) {
//		COUT << *iP << ' ' << *iQS << '\n';
//		++iP; ++iQS;
//	}
}

//=============================================================================================================
