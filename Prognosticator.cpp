#include "Asserts.h"
#include "Prognosticator.h"
#include "CasteSystem.h"
#include "Globals.h"
#include "IO.h"
#include "Numeric.h"
#include "ReMergePolicy.h"

//-------------------------------------------------------------------
//-1   0     1   2    3     4    5   6
static const char* names[] = { "NM","AM","LM","SF","SG", "SL","DL", "GG" };

unsigned findCons(const std::vector<ConsolidationPacket>& consSchedule, offset_t szAfterEviction) {
	if(szAfterEviction<2)
		return consSchedule.size();

	offset_t theLastGap = szAfterEviction*2-1;
	offset_t i = 0;
	//usually (in real simulator, not the simple model) the first ones
	for(; i < consSchedule.size(); ++i) {
		if(consSchedule[i].rightMost > theLastGap) {
			if(i>0)
				return i-1; //if we have consolidation to-the-left of new segment
			else
				break;
		}
		else if(consSchedule[i].rightMost == theLastGap)
			return i;
	}
	return consSchedule.size();
}

struct ConsolidationEmulator: public caste {
	std::vector<uint64_t> queryPerSegment;
	std::vector<uint64_t> evictions;
	std::vector<consolidation> schedule;
	unsigned current;

	size_t dataMult;
	size_t queryPerWindow;

	int lastRunWith;

	ConsolidationEmulator(uint64_t qpW, uint64_t dataM, uint64_t totEvictions) : current(0),
	//queryPerWindow(qpW), dataMult(dataM), /*dataToSeek(data2Seek),*/ totalEvictions(static_cast<offset_t>(evictions)),
			lastRunWith(-42)
	{
		evictions.resize(totEvictions,dataM);
		queryPerSegment.resize(totEvictions,qpW);
		dataMult = dataM;
		queryPerWindow = qpW;
	}
	ConsolidationEmulator(const std::vector<uint64_t> &_evictions, double updateToQRatio) : evictions(_evictions),current(0),lastRunWith(-42) {
		const offset_t totalEvictions = evictions.size();
		queryPerSegment.resize(totalEvictions,0);
		for(auto i : make_irange(totalEvictions))
			queryPerSegment[i] = oneWinQueries(updateToQRatio,evictions[i]);

		dataMult = evictions[0];
		queryPerWindow = queryPerSegment[0];
	}

	inline double totalSpent() const { return IOStats(consolidationData,consolidationSeeks,-1).ioTimeInMinutes() + queryCost(1,0,totalSs()+1);	}
	inline void printSpent(int Strategy) const {
		COUT << "|| " << names[1+Strategy] << "\t" << queryPerWindow << "\t" << dataMult << "\t"  <<
				std::accumulate(queryPerSegment.begin(),queryPerSegment.end(),0ull) << "\t"  << totalSs() << "\t"
			<< consolidationData << "\t" << consolidationSeeks << "\t" << rint(consolidationPaid) << "\t" << /*rint*/(totalSpent()) << ENDL;

	}
//-------------------------------------------------------------------
	ConsolidationStats consolidateTail(offset_t offset,size_t _isPB /* 0 or 1*/, size_t lastInMem /*0 or 1*/) {
		//seeks,sizesWrite,sizesRead
		auto consdata = auxConsolidateTail(segmentStack,/*consolidationCosts,*/ offset, _isPB, lastInMem);
		auxRebuildCPrice(consolidationCosts,segmentStack,1,_isPB); //last on disk now for sure :)
		auto cost = ConsTime::consolidationDataToMinutes(consdata);
		consolidationPaid += cost;
		tokens -= cost; //bad idea to zero it
		//tokens = std::max(tokens,0.0 - queryCost(queryPerWindow,0,segmentStack.size())); //don't let it go too negative -- more explains later
		consolidationSeeks += consdata.rseeks + consdata.wseeks;
		consolidationData += consdata.rdata+consdata.wdata;
		TASSERT(fabs(consolidationPaid - IOStats(consolidationData,consolidationSeeks,-1).ioTimeInMinutes())<0.1, "wrong costs")
		return consdata;
	}
//-------------------------------------------------------------------
	inline void evict() {
		segmentStack.push_back(dataMult);
	}
//-------------------------------------------------------------------
	offset_t skiGreedy()  {
		prepareTokens();
		auto tokensPlus = tokens; //+  queryCost(queryPerWindow,0,segmentStack.size()); //see how many tokens we are to have by the end of this round if not consolidating
		auto i = int(consolidationCosts.size()-1);
		while(i>=0 && tokensPlus>=consolidationCosts[static_cast<size_t>(i)])
			--i;
		return offset_t(i+1);
	}
//-------------------------------------------------------------------
	//0 - AM; 1 - LM; 2 - SA; 3 - SG; 4 - SL; -1 - NM;
	template<int Strategy>
	inline offset_t shouldConsolidate() {
		switch(Strategy) {
		case 0: //Always
			return (segmentStack.size()>1L) ? offset_t(0) : offset_t(segmentStack.size()); break;
		case 1: //Log
			return ReMergePolicy::whereTelescopic(segmentStack); break;
		case 2:
			if(consolidationCosts.size()<2L) return offset_t(segmentStack.size());
			return consolidationCosts[0]<=tokens ? 0 : offset_t(segmentStack.size()); break;
		case 3: //SG
			if(consolidationCosts.size()<2L) return offset_t(segmentStack.size());
			return skiGreedy(); break;
		case -1:
			return offset_t(segmentStack.size()); break;
		}
		FATAL("off")
		return offset_t(segmentStack.size());
	}
//-------------------------------------------------------------------
	//0 - AM; 1 - LM; 2 - SA; 3 - SG; 4 - SL; -1 - NM;
	template<int Strategy>
	inline offset_t shouldConsolidateAtEviction() {
		switch(Strategy) {
		case 3:
			if(consolidationCosts.size()<2) return offset_t(segmentStack.size());
			auxRebuildCPrice(consolidationCosts,segmentStack,0,1); //rebuild for eviction, no pb, data in mem
			return skiGreedy(); break;
		default: break;
		}
		return  shouldConsolidate<Strategy>();
	}
//-------------------------------------------------------------------
	template<int Strategy>
	double run(bool shouldPrint = false) {
		lastRunWith = Strategy;
		schedule.clear();
		for(offset_t i=0; i<evictions.size(); ++i) {

			//query reg
			dataMult = evictions[current];
			queryPerWindow = queryPerSegment[current];
			++current;
			if(segmentStack.size()>1)
				accSeeks += (segmentStack.size()-1) * queryPerWindow;
			totalQueries+=queryPerWindow;

			if(i == evictions.size()-1)
				break; //no eviction, as this is a UB dummy!

			evict();
			auxRebuildCPrice(consolidationCosts,segmentStack,0,0); //last in mem
			auto where = shouldConsolidateAtEviction<Strategy>();
			if(where < segmentStack.size()-1) {
				consolidateTail(where,0,1); //here we have the last one in memory!
				schedule.push_back({where,1});
			}
			//			else//				auxRebuildCPrice(consolidationCosts,segmentStack,1,Strategy>1 ? 1 : 0); //last on disk

			if(schedule.size()<i+1)
				schedule.push_back({0,0});
			TASSERT(schedule.size()==i+1, "two consolidations?")
		}


		if(shouldPrint)
			printSpent(Strategy);
		return totalSpent();
	}
//-------------------------------------------------------------------
//	double runSG(const std::vector<consolidation>& _schedule, bool isPrint=false) {
//		lastRunWith = 6;
//
//		offset_t whereAt = 0;
//		for(offset_t i=0; i<evictions.size(); ++i) {
//			evict();
//			auxRebuildCPrice(consolidationCosts,segmentStack,0,0); //last in mem
//
//			auto consPlan = _schedule[whereAt];
//			if(consPlan.second)
//				consolidateTail(consPlan.first,0,1);
//			++whereAt;
//
//			//query reg
//			accSeeks += (segmentStack.size()-1) * queryPerWindow;
//			totalQueries+=queryPerWindow;
//
//		}
//		if(isPrint)
//			printSpent(lastRunWith);
//		return totalSpent();
//	}
//-------------------------------------------------------------------
//		double runGG(const std::vector<consolidation>& consSchedule) {
//			lastRunWith = 6;
//			std::vector<uint64_t> segs(2*evictions.size(),-1);
//			offset_t whereAt = 0;
//			for(offset_t i=0; i<evictions.size(); ++i) {
//				evict();
//				segs[i*2]=dataMult; //use this repr. along with the stack
//				if(whereAt < consSchedule.size()) {
//					auto consPlan = consSchedule[whereAt];
//					auto left = consPlan.second;
//					auto right = consPlan.first;
//					if(right/2 < i ) { //if consolidate
//						auto offset = std::count(segs.begin(),segs.begin()+left,_border);
//						consolidateTail(offset,0,1);
//						segs[left] = 0;
//					 	//now skip the other ones
//						while(whereAt < consSchedule.size() && right == consSchedule[whereAt].first ) {
//							segs[consSchedule[whereAt].second] = 0;
//							++whereAt;
//						}
//					} //end if consolidate
//				}
//				for(uint64_t q = 0; q < queryPerWindow; ++q)
//					queryReg(unsigned(segmentStack.size()));
//			}
//			printSpent(lastRunWith);
//			return totalSpent();
//		}

	inline offset_t makeOffsetFromCons(const std::vector<size_t>& segs, ConsolidationPacket cp) {
		return std::count(segs.begin(),segs.begin()+cp.leftMost,_border);
	}
	//-------------------------------------------------------------------
	double run(const std::vector<ConsolidationPacket>& consSchedule, bool isPrint=false) {
		lastRunWith = 6;
		std::vector<uint64_t> segs(2*evictions.size(),_border);
		for(offset_t i=0; i<evictions.size(); ++i) {

			//query reg
			dataMult = evictions[current];
			queryPerWindow = queryPerSegment[current];
			++current;
			if(segmentStack.size()>1)
				accSeeks += (segmentStack.size()-1) * queryPerWindow;
			totalQueries+=queryPerWindow;

			if(i == evictions.size()-1)
				break; //no eviction, as this is a UB dummy!

			evict();
			auxRebuildCPrice(consolidationCosts,segmentStack,0,0); //last in mem -- should?
			
			segs[i*2]=dataMult;
			auto which = findCons(consSchedule,i+1);
			if(segmentStack.size() > 1 && which != consSchedule.size()) {
				auto offset = makeOffsetFromCons(segs,consSchedule[which]);
//				COUTL << consSchedule[which] << " " << offset << ENDL;
				consolidateTail(offset,0,1);
				for(offset_t w = consSchedule[which].leftMost+2; w < consSchedule[which].rightMost-2; w+=2 )
					segs[w] = 0;
				segs[consSchedule[which].position] = 0; //for a case of e.g. 1,1,5
			}

		}
		if(isPrint)
			printSpent(lastRunWith);
		return totalSpent();
	}
	//-------------------------------------------------------------------
	static void runPrognosticator(uint64_t qpW, uint64_t dataM, offset_t evictions) {
		auto trainOn = evictions;
		const int s0 = -1; const int s1 = 1; const int s2 = 0; const int s3 = 3;
		std::vector<ConsolidationEmulator*> ems = { new ConsolidationEmulator(qpW,dataM,trainOn),new ConsolidationEmulator(qpW,dataM,trainOn),new ConsolidationEmulator(qpW,dataM,trainOn),new ConsolidationEmulator(qpW,dataM,trainOn) };
		std::vector<double> costs = { ems[0]->run<s0>(), ems[1]->run<s1>(), ems[2]->run<s2>(), ems[3]->run<s3>()};
		auto bestOne = std::min_element(costs.begin(),costs.end()) - costs.begin();
		ems[size_t(bestOne)]->printSpent(ems[size_t(bestOne)]->lastRunWith);
	}
	};
//-------------------------------------------------------------------
bool testAssumption(const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions, bool isPrint) {
	// if(!DP)
	// 	DP = allocAndZero3d<double>(totalEvictions);
	// if(!B)
	// 	B = allocAndZero3d<std::pair<uint16_t,uint16_t> >(totalEvictions);

//	if(flagFirst && DP) {
//		CHEAP_ASSERT(totalEvictions<=512,"increase cache size")
//		flagFirst = false;
//		for(offset_t i=0; i<512; ++i)
//			for(offset_t j=0; j<512; ++j)
//				cache[i][j] = std::make_pair(-1,0);
//	}

//	COUTL << ConsolidationEmulator(queryPerWindow,dataMult,dataToSeek,totalEvictions).dataToSeek << ENDL;
//	COUTL << ReMergePolicy::getCmpMargin().first  << "/" << ReMergePolicy::getCmpMargin().second << ENDL;
//	COUTL << ReMergePolicy::isComparableSize(8,4) << " " << ReMergePolicy::isComparableSize(8,7) << " " << ReMergePolicy::isComparableSize(8,8) << ENDL;
	if(isPrint)
		COUT << "|| St\tqueryPerWindow\tdataMult\ttotalQs\tquerySeeks\tconsolidationData\tconsolidationSeeks\tconsolidationPaid\ttotalSpent\n";
//	ConsolidationEmulator(queryPerWindow,dataMult,totalEvictions).run<-1>(true);

	double spent[4];
	spent[0] = ConsolidationEmulator(queryPerWindow,dataMult,totalEvictions).run<1>(isPrint);
	{
	ConsolidationEmulator ce(queryPerWindow,dataMult,totalEvictions);
	spent[1] = ce.run<0>(isPrint);
	//for(auto p : ce.schedule) COUTL << '<' << p.first << ',' << p.second << '>' << ENDL;
	}


	spent[2] = ConsolidationEmulator(queryPerWindow,dataMult,totalEvictions).run<3>(isPrint);

	std::vector<uint64_t> evictions;
	evictions.resize(totalEvictions,dataMult);
	BaseScheduler bs(evictions, double(dataMult)/double(queryPerWindow));
	auto schedule = bs();
	for(auto c : schedule) COUTL << c << ENDL;
	ConsolidationEmulator ce(queryPerWindow,dataMult,totalEvictions);
	spent[3] = ce.run(schedule,isPrint); //globalGadi( queryPerWindow, dataMult, totalEvictions)
	//for(auto p : ce.schedule) COUTL << '<' << p.first << ',' << p.second << '>' << ENDL;
	//ConsolidationEmulator::runPrognosticator(queryPerWindow,dataMult,totalEvictions);
	//totalCostDPFull(q,s,evictions);
	//totalCostDPFullAndLog(DP,B,queryPerWindow,dataMult,totalEvictions);

//	if(DP)
//		release3d<double>(DP,totalEvictions);
//	if(B)
//		release3d<std::pair<uint16_t,uint16_t> >(B,totalEvictions);
	return std::abs(spent[3] - *std::min_element(spent,spent+4)) < 0.01;
}

bool testAssumption(const std::vector<size_t>& evictions, double updatesToQRatio, bool isPrint) {
	if(isPrint)
		COUT << "|| St\tqueryPerWindow\tdataMult\ttotalQs\tquerySeeks\tconsolidationData\tconsolidationSeeks\tconsolidationPaid\ttotalSpent\n";

	double spent[4];
	spent[0] = ConsolidationEmulator(evictions,updatesToQRatio).run<1>(isPrint);
	{
	ConsolidationEmulator ce(evictions,updatesToQRatio);
	spent[1] = ce.run<0>(isPrint);
	//for(auto p : ce.schedule) COUTL << '<' << p.first << ',' << p.second << '>' << ENDL;
	}
	spent[2] = ConsolidationEmulator(evictions,updatesToQRatio).run<3>(isPrint);

	BaseScheduler bs(evictions,updatesToQRatio);
	auto schedule = bs();
	if(isPrint) {
		for(auto s : evictions)
			COUT << s << ',';
		COUT << ENDL << "Total times: " << bs.totalCostForSeeks << ' ' << bs.getTotalCostIfNoConsolidation() << ENDL;
		for(auto c : schedule) COUTL << c << ENDL;
	}
	ConsolidationEmulator ce(evictions,updatesToQRatio);
	spent[3] = ce.run(schedule,isPrint); //globalGadi( queryPerWindow, dataMult, totalEvictions)
	//for(auto p : ce.schedule) COUTL << '<' << p.first << ',' << p.second << '>' << ENDL;

	return std::abs(spent[3] - *std::min_element(spent,spent+4)) < 0.1;
}

//-------------------------------------------------------------------
//return 1 if in mem (single segment, never merged before)
inline size_t isInMem(offset_t where, offset_t stopat) {
	return stopat == where+2 ? 1 : 0;
}


inline size_t consolidationSumsFor(const std::vector<int64_t>& ss, ConsolidationPacket& packet) {
	TASSERT(ss.at(packet.position) == _border, "wrong where")
	return
			(packet.leftSum = dirSum(ss,-1,packet.position,packet.leftMost)) //left wing
			+
			(packet.rightSum = dirSum(ss,1,packet.position,packet.rightMost));
}

inline double consolidateThatCost(offset_t where, const std::vector<int64_t>& ss, offset_t& stopAt) {
	ConsolidationPacket packet;
	packet.position = where;
	packet.rightMost = stopAt;
	consolidationSumsFor(ss,packet);
	size_t segments[2] = {packet.leftSum,packet.rightSum};
	return ConsTime::consolidationDataToMinutes(kWayConsolidate(segments,segments+2,isInMem(where,stopAt)));
}


//find the best cost-2-benefit consolidation
ConsolidationPacket BaseScheduler::getBestGain() {
	CHEAP_ASSERT(totalEvictions>=2,"not enough evictions")

	ConsolidationPacket best;
	for(auto it : make_irange(totalEvictions-2)) { // -2 because we the last segment is never evicted
		auto i = 2*it+1;  // s0,-1,s1,-1,...  <- target the borders
		if(evictedSizes[i] == 0) //already consolidated
			continue;
		else {
			TASSERT(evictedSizes[i]==_border,"should be border")
		}

		ConsolidationPacket candidate(i);
		auto consolidationCost = consolidationCostFor(candidate);
		candidate.cGain = queryCost(windowsQueriesFrom((candidate.rightMost+1)>>1), //since we start paying seeks after evicting the rightmost!
						      0 /*just seeks*/,2 /* dummy */) - consolidationCost;
		if(candidate.cGain > best.cGain)
			best = candidate; // std::make_tuple(gain,i,stopAt-1); //place of consolidation, right border
	}
	return best;
}
//-------------------------------------------------------------------
std::ostream& operator<<(std::ostream& out, const ConsolidationPacket &p) {
	return out << p.leftMost << " - " << p.position << " - " << p.rightMost << " gain: " << p.cGain;
}
//-------------------------------------------------------------------
double BaseScheduler::consolidationCostFor(ConsolidationPacket& packet) const {
	consolidationSumsFor(evictedSizes, packet);
	ConsolidationStats consData = consolidateTwoSegments(packet.leftSum,packet.rightSum,packet.isInMem());
	return ConsTime::consolidationDataToMinutes(consData);
}

double RndScheduler::consolidationCostFor(ConsolidationPacket& packet) const {
	consolidationSumsFor(evictedSizes, packet);
	size_t estimateReduction = 0;
	auto consData = consolidateTwoSegments(packet.leftSum,packet.rightSum,packet.isInMem(),estimateReduction);
	return ConsTime::consolidationDataToMinutes(consData);
}

double BaseScheduler::getTotalCostIfNoConsolidation() const {
	size_t expectedSeeks = 0;
	//before first eviction we pay 0 seeks (in mem.)
	//then we pay 1-1 seeks! so, first two evictions are free...
	for(auto i : make_irange(2,totalEvictions)) //calc the cost of having no consolidations:
		expectedSeeks += queryPerWindow[i] * (i-1);
	return IOStats(0,expectedSeeks,-1).ioTimeInMinutes();
}

//remember! the last segment in evictions is **never** evicted -- it is a helper for queries
const std::vector<ConsolidationPacket>&  BaseScheduler::operator()() {
	if(totalEvictions<=2) //needs to have 0,s0,s1 at least
		return resVector;

	auto totalSeekCost = getTotalCostIfNoConsolidation();
	for(auto round : make_irange(1u,totalEvictions)) { 	(void)round; //surpress warning
		auto cand = getBestGain();
		if(cand.cGain <= 0.0) //no profitable cons. left
			break;

		totalSeekCost -= cand.cGain; //get the gain
		TASSERT(evictedSizes[cand.position]==_border,"should be border")
		evictedSizes[cand.position] = 0; //consolidate
		resVector.push_back(cand);
	}
	std::sort(resVector.begin(),resVector.end());
	totalCostForSeeks = totalSeekCost;
	return resVector;
}

//-------------------------------------------------------------------
//std::tuple<double,size_t,size_t> getBestCost(const offset_t totalEvictions, std::vector<int64_t>& ss,const uint64_t queryPerWindow) {
//	auto cand = std::make_tuple(0.0,0ULL,0ULL);
//	//find the best cost-2-benefit consolidation
//	for(auto it : make_irange(totalEvictions-1)) {
//		auto i = 2*it+1;
//		if(ss[i] == 0) //already consolidated
//			continue;
//		else {
//			TASSERT(ss[i]==_border,"should be border")
//		}
//
//		offset_t stopAt;
//		auto cost = consolidateThatCost(i,ss,stopAt);
//		auto gain = queryCost( (totalEvictions-((stopAt-2)/2)) *queryPerWindow,0/*just the seeks*/,2 /* dummy 2 */) - cost;
//		if(gain>std::get<0>(cand))
//			cand = std::make_tuple(gain,i,stopAt-1); //place of consolidation, right border
//	}
//
//	return cand;
//}
/*
	DP[s][e]  = min {
     	 	 	 	 	 Q + cons_cost(s segments, size of N-e) + DP[1,e-1]  //consolidate to one, then query
                         (i-1)Q + DP[s+1,e-1]  //skip consolidation, query, and start next round with s+1 segments
                    }
	DP[i][1] = min { //the last window with 1 ≤ i ≤ N segments
					  cons_cost(i segments, size of N)  //consolidate to one, then query with 0 overhead
					  i*Q //skip consolidation, query with overhead
	     	 	    } */


//inline double consolidateSegmentCost(size_t rPostings,size_t wPostings, size_t rSeeks) { return IOStats(rPostings+wPostings,rSeeks+1,-1).ioTimeInMinutes(); }
//#define COST consolidateSegmentCos
/*
double totalCostDPFull(const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions) {
	auto DP = allocAndZero2d<double>(totalEvictions);
	//auto BT = allocAndZero2d<bool>(totalEvictions);
	for(offset_t s=1; s<=totalEvictions; ++s)
		DP[s][1] = std::min(queryCost(queryPerWindow,0,s+1),  //skip consolidation, query
							 COST(totalEvictions*dataMult,totalEvictions*dataMult,s)); //consolidate to one, then get a free query

	for(offset_t e=2; e<=totalEvictions; ++e)
		for(offset_t s=1; s<=totalEvictions-e+1; ++s) {
			auto skipCons =    DP[s+1][e-1] + 0                                       + queryCost(queryPerWindow,0,s+1); //skip consolidation, query, and start next round with s+1 segments
			auto consolidate = DP[1][e-1]   + COST((totalEvictions-e+1)*dataMult,(totalEvictions-e+1)*dataMult,s+1) + queryCost(queryPerWindow,0,1); //full consolidation, the query cost is actually 0
			DP[s][e] = std::min(skipCons,consolidate);
			//BT[s][e] = DP[s][e] == consolidate;
		}

	//print(DP,totalEvictions);
	//print(BT,totalEvictions);
	auto res = DP[1][totalEvictions];
	COUT << "|| DF\t"<<queryPerWindow<<"\t"<<dataMult<<"\t"<<totalEvictions*queryPerWindow<<"\tx\tx\tx\tx\t" << res << ENDL;
	release2d<double>(DP,totalEvictions);
	return res;
}
*/


//template<typename T>
//void print2d(T** ptr, offset_t totalEvictions) {
//	for(offset_t s=0; s<=totalEvictions+1; ++s) {
//		COUT << s << (s<10?" ":"") << "| ";
//		for(offset_t e=0; e<=totalEvictions+1; ++e)
//			if(ptr[s][e]>0)COUT << size_t(ptr[s][e]) << " ";
//			else COUT << "  ";
//		COUT << "\n";
//	}
//}
//-------------------------------------------------------------------
//	//has no PB
//	double run(const std::vector<consolidation>& _schedule) {
//		lastRunWith = 5;
//
//		offset_t whereAt = 0;
//		TASSERT(_schedule[0].first == 0, "strange")
//		for(offset_t i=0; i<evictions.size(); ++i) {
//			evict();
//			auxRebuildCPrice(consolidationCosts,segmentStack,0,0); //last in mem
//
//			auto consPlan = _schedule[whereAt];
//			//COUTL << consPlan.first  << " " << consPlan.second << "\n";
//			if(consPlan.first == 0 && consPlan.second == 1) //full
//				consolidateTail(0,0,1);
//			else if(whereAt && (consPlan.second == _schedule[whereAt-1].second+1 || (consPlan.second == consPlan.first && consPlan.second == 0))) //skip
//			{}
//			else  //log
//				consolidateTail(ReMergePolicy::whereTelescopic(segmentStack),0,1);
//			++whereAt;
//			for(offset_t q = 0; q < queryPerWindow; ++q)
//				queryReg(unsigned(segmentStack.size()));
//		}
//		printSpent(lastRunWith);
//		return totalSpent();
//	}
//-------------------------------------------------------------------
std::vector<consolidation> findOptimalConsolidation(const std::vector<size_t>& evictedSizes, double updatesToQueriesRatio) {
	BaseScheduler bs(evictedSizes,updatesToQueriesRatio);
	FATAL("type!")
	return {};  //bs();
/*
	const auto totalEvictions =  evictedSizes.size();
	if(totalEvictions<2)
		return {};

	unsigned expectedSeeks = 0;
	for(auto i : make_irange(totalEvictions)) //calc the cost of having no consolidations:
		{ expectedSeeks += oneWinQueries(updatesToQueriesRatio,evictedSizes[i]) * i; }
	auto totalSeekCost = IOStats(0,expectedSeeks,-1).ioTimeInMinutes();

	std::vector<int64_t> queryPerWindow(totalEvictions,0);
	for(auto i : make_irange(totalEvictions))
		queryPerWindow[i] = oneWinQueries(updatesToQueriesRatio,evictedSizes[i]);

	std::vector<consolidation> rememberRBorders;
	auto ss = make_hangers(evictedSizes);

	for(auto round : make_irange(1u,totalEvictions)) {
		(void)round; //surpress warning
		auto cand = getBestCost(totalEvictions,ss,queryPerWindow);
		if(std::get<0>(cand)<=0.0) //no profitable cons. left
			break;

		totalSeekCost -= std::get<0>(cand); //get the gain
		TASSERT(ss[std::get<1>(cand)]==_border,"should be border")
		ss[std::get<1>(cand)] = 0; //consolidate
		rememberRBorders.push_back( std::make_pair(std::get<2>(cand),std::get<1>(cand)) );
	}
	std::sort(rememberRBorders.begin(),rememberRBorders.end());

	return rememberRBorders;
	*/
}
//-------------------------------------------------------------------


//
//std::vector<consolidation> globalGadi(const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions) {
//	auto totalCost = 0.0;
//	for(auto i : make_irange(totalEvictions)) //calc the cost of having no consolidations:
//		totalCost += queryCost(queryPerWindow,0,i+1); //increasing (by 1) segment count
//
//	auto ss = make_hangers(std::vector<size_t>(totalEvictions,dataMult));
//	std::vector<consolidation> rememberRBorders;
//
//	for(auto round : make_irange(1u,totalEvictions)) {
//		(void)round;
//		auto cand = getBestCost(totalEvictions,ss,queryPerWindow);
//		if(std::get<0>(cand)<=0.0) //no profitable cons. left
//			break;
//
//		totalCost -= std::get<0>(cand); //get the gain
//		TASSERT(ss[std::get<1>(cand)]==_border,"should be border")
//		ss[std::get<1>(cand)] = 0; //consolidate
//		rememberRBorders.push_back( std::make_pair(std::get<2>(cand),std::get<1>(cand)) );
//	}
//	std::sort(rememberRBorders.begin(),rememberRBorders.end());
//	//for(auto c : ss) COUT << c << " "; COUT<<ENDL;
//	//for(auto b : rememberRBorders) COUTL << b.second << " " << b.first << "\n";
//	return rememberRBorders;
//}
//-------------------------------------------------------------------
//std::vector<consolidation> backtrack(consolidation*** B, const offset_t totalEvictions);
//double totalCostDPFullAndLog(double*** DP, consolidation*** B, const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions) ;
//
//template<typename T>
//void init2d(T** DP,unsigned totalEvictions) {
//	for(unsigned i=0; i<totalEvictions; ++i)
//		memset(DP[i],0,sizeof(T)*totalEvictions);
//}
//
//template<typename T>
//T** allocAndZero2d(unsigned totalEvictions) {
//	totalEvictions+=2; //two more
//	T **DP = new T*[totalEvictions];
//	for(unsigned i=0; i<totalEvictions; ++i) {
//		DP[i] = new T[totalEvictions];
//		memset(DP[i],0,sizeof(T)*totalEvictions);
//	}
//	return DP;
//}
//
//template<typename T>
//void release2d(T** ptr,unsigned totalEvictions) {
//	for(unsigned i=0; i<totalEvictions+2; ++i)
//		delete[] ptr[i];
//}
//
//template<typename T>
//void init3d(T*** DP,unsigned totalEvictions) {
//	for(unsigned i=0; i<totalEvictions+16; ++i)
//		init2d<T>(DP[i],totalEvictions);
//}
//template<typename T>
//void release3d(T*** ptr,unsigned totalEvictions) {
//	for(unsigned i=0; i<totalEvictions+16; ++i)
//		release2d<T>(ptr[i],totalEvictions);
//	delete[] ptr;
//}
//
//template<typename T>
//T*** allocAndZero3d(unsigned totalEvictions) {
//	T ***DP = new T**[totalEvictions+16];
//	for(unsigned i=0; i<totalEvictions+16; ++i)
//		DP[i] = allocAndZero2d<T>(totalEvictions);
//	return DP;
//}
/*
std::tuple<double,size_t,size_t> getBestCost(const offset_t totalEvictions, std::vector<int64_t>& ss,const std::vector<int64_t>& queryPerWindow) {
    CHEAP_ASSERT(totalEvictions>1,"too few evictions")
    auto cand = std::make_tuple(0.0,0ULL,0ULL);
    //find the best cost-2-benefit consolidation
    for(auto it : make_irange(totalEvictions-1)) {
            auto i = 2*it+1;
            if(ss[i] == 0) //already consolidated
                    continue;
            TASSERT(ss[i]==_border,"should be border")
            offset_t stopAt;
            auto consolidationCost = consolidateThatCost(i,ss,stopAt);

            auto gain = queryCost(windowsQueries(queryPerWindow,(stopAt-2)/2),
                            //(totalEvictions-((stopAt-2)/2)) *queryPerWindow,
                                            0,2) - consolidationCost;
            if(gain>std::get<0>(cand))
                    cand = std::make_tuple(gain,i,stopAt-1); //place of consolidation, right border
    }
    return cand;
}*/
//-------------------------------------------------------------------
//segments[0] is the largest
//ret: rwData,seeks,totalSz
//std::pair<uint64_t,offset_t> telescopic(const std::vector<size_t>& /*segments*/) {
	//FATAL("?!")
//	auto ss = make_hangers(segments);
//
//	auto start = ss[0];
//	auto mx = start/start;
//	while(mx<=start) {
//		offset_t ls=0;
//		offset_t rs=0;
//		offset_t nls = 0;
//		int64_t accLeft = 0;
//		while(ls<ss.size() && rs<ss.size()) { //could be replaced with binary search, but who cares...
//			if(accLeft == 0)
//				accLeft = sumUntilDelim(ss,ls,rs);
//			if(rs>=ss.size())
//				break;
//			auto accRight = sumUntilDelim(ss,rs,nls);
//			if(accLeft!=mx || accRight!=accLeft) { //#skip
//				accLeft = accRight;
//				rs = nls;
//			}
//			else{ //merge case -- just replace -1 with 0 and switch accs
//				TASSERT(ss[rs-1]==-1,"not a border")
//				ss[rs-1] = 0;
//				accLeft=0;
//				ls=nls;
//			}
//		} //end linear while
//		mx *= 2;
//	} //end log-while
//
//	offset_t readSeeks = 0;
//	offset_t writeSeeks = 0;
//	uint64_t rwData = 0;
//
//	offset_t count = 0;
//	uint64_t szs = 0;
//	for(auto v : ss)
//		if(v<0) { //border
//			if(count>1) { //was merging
//				readSeeks += count;
//				++writeSeeks;
//				rwData += szs;
//				TASSERT(typedBitSetCount(szs)==1,"bad sz")
//			}
//			szs=count=0;
//		}
//		else if(v>0) {
//			++count;
//			szs += uint64_t(v);
//		}
//	return std::make_pair(rwData,readSeeks+writeSeeks);
//}
//-------------------------------------------------------------------
//static std::pair<double,offset_t> cache[512][512];
//static bool flagFirst = true;


//std::pair<uint64_t,offset_t> telescopic(const offset_t rp2p, const offset_t singletones) {
//	TASSERT(rp2p > 1 && rp2p<512 && singletones<512, "bad sz");
//	auto data = cache[rp2p][singletones];
//	if(data.first>-1)
//		return data;
//
//	auto p2p = rp2p;
//	std::vector<size_t> segments;
//	segments.reserve(32);
//	for(offset_t i=0; i<singletones; ++i)
//		segments.push_back(1);
//	offset_t p = 1;
//	while(p2p) {
//		if(p2p%2)
//			segments.push_back(p);
//		p <<= 1;
//		p2p >>= 1;
//	}
//	std::reverse(segments.begin(),segments.end());
//	cache[rp2p][singletones] = telescopic(segments);
//	return cache[rp2p][singletones];
//}
//-------------------------------------------------------------------
//double totalCostDPFull(const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions);
//std::pair<uint64_t,offset_t> telescopic(const std::vector<size_t>& segments);
//std::pair<uint64_t,offset_t> telescopic(const offset_t rp2p, const offset_t singletones);
//std::pair<uint64_t,offset_t> telescopic0(offset_t singletones);


////rwData,seeks,totalSz
////a version that assumes that p2p is 0
//std::pair<uint64_t,offset_t> telescopic0(offset_t singletones) {
//	auto maskLower = (singletones>>1)<<1;
//	auto bitset = typedBitSetCount(maskLower);
//	return std::make_pair(maskLower,singletones+bitset);
//}
////-------------------------------------------------------------------
//inline std::pair<uint64_t,offset_t> getNewP2PAndS(const offset_t p2p, const offset_t s) {
//	auto bitset = typedBitSetCount(p2p);
//	auto singletones = s+1-bitset;
//	return std::make_pair( ((p2p+singletones)>>1)<<1 , typedBitSetCount(p2p+singletones) );
//}
//-------------------------------------------------------------------
//at stage e when we start with s segments
//only those pairs of p2p and singletones are valid where:
// s+1 = bitset(p2p) + singletones
// p2p+singletones == totalEvictions-e+1
/*
inline double getBestTelescopic(double*** DP, const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions,
									const offset_t p2p, const offset_t e, const offset_t s) {
	TASSERT(p2p%2==0,"wrong flags")
	auto bitset = typedBitSetCount(p2p);
	offset_t singletones = s+1-bitset;
	if(s+1<typedBitSetCount(p2p) || //can't happen in this round
			singletones + p2p != totalEvictions-e+1	|| //more data than evicted
				singletones<2) //silly corner case
		return std::numeric_limits<double>::max();

	double minCost = std::numeric_limits<double>::max();
	auto iSingletones = singletones;
	{
		auto newp2p = ((p2p+iSingletones)>>1)<<1;
		auto newS = typedBitSetCount(p2p+iSingletones);
		//TASSERT(getNewP2PAndS(p2p,s)=={newp2p,newS},"diff code?")
		auto mergeData = (p2p == 0) ? telescopic0(iSingletones) : telescopic(p2p,iSingletones);
		TASSERT(e<=1 || DP[newp2p][newS][e-1]>0,"why 0?")
		auto cost = DP[newp2p][newS][e-1] + COST(mergeData.first*dataMult,mergeData.first*dataMult,mergeData.second) + queryCost(queryPerWindow,0,newS);
		minCost = std::min(cost,minCost);
	}
	return minCost;
}

std::vector<consolidation> backtrack(consolidation*** B, const offset_t totalEvictions) {
	std::vector<consolidation> ret;
	ret.reserve(totalEvictions+16);
	offset_t p2p = 0;
	offset_t s = 1;
	offset_t e = totalEvictions;
	while(e!=0) {
		auto pairB=B[p2p][s][e];
		ret.push_back(pairB);
		p2p = pairB.first;
		s = pairB.second;
		--e;
	}
	if(ret.back().second == 0 && ret.back().first == 0) {
		ret.pop_back();
		ret.push_back(ret.back()); //override last with the one before if is invalid
	}
	return ret;
}
*/
/*
double totalCostDPFullAndLog(double*** DP, consolidation*** B, const uint64_t queryPerWindow, const uint64_t dataMult, const offset_t totalEvictions) {
	init3d(DP,totalEvictions);
	init3d(B,totalEvictions);
	for(offset_t p2p=0; p2p<=totalEvictions; p2p+=2)
		for(offset_t s=1; s<=totalEvictions; ++s) {
			auto tele = getBestTelescopic(DP,queryPerWindow,dataMult,totalEvictions,p2p,1,s);
			DP[p2p][s][1] = std::min( {queryCost(queryPerWindow,0,s+1),  //skip consolidation, query
									  COST(totalEvictions*dataMult,totalEvictions*dataMult,s), //consolidate to one, then query
									  tele});
		}

	for(offset_t e=2; e<=totalEvictions; ++e)
		for(offset_t s=1; s<=totalEvictions-e+1; ++s)
			for(offset_t p2p=0; p2p<totalEvictions-e+1; p2p+=2) {
				TASSERT(DP[p2p][s+1][e-1]>0 && DP[0][1][e-1]>0 ,"?")
				auto skipCons =        DP[p2p][s+1][e-1] + 0                                       + queryCost(queryPerWindow,0,s+1); //skip consolidation, query, and start next round with s+1 segments
				auto consolidateFull = DP[0][1][e-1]     + COST((totalEvictions-e+1)*dataMult,(totalEvictions-e+1)*dataMult,s+1) + 0; //full consolidation, the query cost is actually 0
				auto logCons = getBestTelescopic(DP,queryPerWindow,dataMult,totalEvictions,p2p,e,s);
				auto sm = DP[p2p][s][e] = std::min({skipCons,consolidateFull,logCons});
				if(double_equals(sm,skipCons))
					B[p2p][s][e] = std::make_pair(p2p,s+1);
				else if(double_equals(sm,consolidateFull))
					B[p2p][s][e] = std::make_pair(0,1);
				else if(double_equals(sm,logCons))
					B[p2p][s][e] = getNewP2PAndS(p2p,s);
				else { FATAL("why here?") }
			}

	auto res = DP[0][1][totalEvictions];
	ConsolidationEmulator(queryPerWindow,dataMult,totalEvictions).runDP(backtrack(B,totalEvictions));
	return res;
}
*/
